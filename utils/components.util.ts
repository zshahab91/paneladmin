import { message } from 'antd';
import { RcFile } from 'antd/lib/upload/interface';

/**
 * Validate photo ratio before upload
 * @param file
 */
export function ratioCheck(file: RcFile): Promise<void> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const image = new Image();
      image.src = reader.result as string;
      image.onload = function () {
        if (image.width === image.height) {
          resolve();
        } else {
          message.error('Invalid photo ratio. The valid ratio is 1:1');
          reject();
        }
      };
    };
  });
}

export function resizeDataURL(data: string, size: number) {
  return new Promise(function(resolve, reject) {

    // We create an image to receive the Data URI
    const img = document.createElement('img') as HTMLImageElement;

    // When the event "onload" is triggered we can resize the image.
    img.onload = function() {
      // We create a canvas and get its context.
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');

      // We set the dimensions at the wanted size.
      canvas.width = size;
      canvas.height = size;

      // We resize the image with the canvas method drawImage();
      //@ts-ignore
      ctx?.drawImage(this, 0, 0, size, size);

      const dataURI = canvas.toDataURL();

      // This is the return of the Promise
      resolve(dataURI);
    };

    img.src = data;
  })
}