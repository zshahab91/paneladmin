import {request} from '@services/http/adapter';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import ShippingPeriod from '@entities/shippingPeriod';

export default class ShippingPeriodsService {
    public static getList(): Promise<AxiosResponse<IPaginatedResponse<ShippingPeriod[]>>> {
        return request.get(`/admin/shipping-periods`);
    }

    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<ShippingPeriod>>> {
        return request.get(`/admin/shipping-periods/${id}`);
    }

    public static add(
        body: Partial<ShippingPeriod>,
    ): Promise<AxiosResponse<IPaginatedResponse<ShippingPeriod>>> {
        return request.post(`/admin/shipping-periods`, body);
    }

    public static edit(
        body: Partial<ShippingPeriod>,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<ShippingPeriod>>> {
        return request.patch(`/admin/shipping-periods/${id}`, body);
    }
}
