import {request} from '@services/http/adapter';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import ShippingMethod, {AddShippingMethod, EditShippingMethod} from '@entities/shippingMethod';

export default class ShippingMethodsService {
  public static getList(): Promise<
    AxiosResponse<IPaginatedResponse<ShippingMethod[]>>
    > {
    return request.get(`/admin/shipping-methods`);
  }

  public static getById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingMethod>>> {
    return request.get(`/admin/shipping-methods/${id}`);
  }

  public static add(
    body: AddShippingMethod,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingMethod>>> {
    return request.post(`/admin/shipping-methods`, body);
  }

  public static edit(
    body: EditShippingMethod,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingMethod>>> {
    return request.patch(`/admin/shipping-methods/${id}`, body);
  }

  public static delete(
    id: number,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingMethod>>> {
    return request.delete(`/admin/shipping-methods/${id}`);
  }
}
