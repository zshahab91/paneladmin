import { request } from '@services/http/adapter';
import { ApiService } from '@services/api/api.service';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';
import ShippingCategory, { AddShippingCategory, EditShippingCategory, } from '@entities/shippingCategory';

class ShippingCategoriesApiService extends ApiService {



  public static grid(pageIndex?: number, query?: string): Promise<
    AxiosResponse<IPaginatedResponse<ShippingCategory[]>>
  > {
    return request.get(`/admin/shipping-categories?page=${pageIndex ? pageIndex : 1}${query ? query : ''}`);
  }

  public static getById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingCategory>>> {
    return request.get(`/admin/shipping-categories/${id}`);
  }

  public static add(
    body: AddShippingCategory,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingCategory>>> {
    return request.post(`/admin/shipping-categories/`, body);
  }

  public static edit(
    body: EditShippingCategory,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingCategory>>> {
    return request.patch(`/admin/shipping-categories/${id}`, body);
  }

  public static delete(
    id: number,
  ): Promise<AxiosResponse<IPaginatedResponse<ShippingCategory>>> {
    return request.delete(`/admin/shipping-categories/${id}`);
  }
}

export default ShippingCategoriesApiService;
