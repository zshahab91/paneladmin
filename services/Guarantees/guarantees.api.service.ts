import ProductOptionValue from "@entities/productOptionValue";
import { IResponse } from "@entities/response";
import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';

import ProductOption, {Guarantee,} from '@entities/productOption';

class GuaranteeService extends ApiService {

    public static async  show(id: number): Promise<AxiosResponse<IResponse<ProductOptionValue>>> {
        return request.get(`/admin/product/options/values/${id}`);
    }

    private static async _getGuaranteeValues() {
        const {status, data} = await request.get("/admin/product/options", {params: {'filter[code]': 'guarantee'}});
        if (Math.floor(status / 100) !== 2)
            throw new Error('Oppps ! status code is ' + status);

        const productOption: ProductOption = data?.results[0];
        if (!productOption)
            throw new Error('Could not find guarantees in product options !');

        const guaranteeValues: Guarantee[] = productOption.values || [];

        return {productOption, guaranteeValues};
    }


    public static async add(guarantee: Partial<ProductOption>): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
        return request.post(`/admin/product/options/guarantee/values`, guarantee);
    }

    public static async edit(
        id: number,
        guarantee: Partial<ProductOption>
    ): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
        return request.patch(`/admin/product/options/values/${id}`, guarantee);
    }
}


export default GuaranteeService;
