/* eslint no-underscore-dangle: 0 */

export enum Resizing {
  FIT = 'fit',

  FILL = 'fill',

  AUTO = 'auto',
}

export enum Gravity {
  NORTH = 'no',

  SOUTH = 'so',

  EAST = 'ea',

  WEST = 'we',

  NORTH_EAST = 'noea',

  NORTH_WEST = 'nowe',

  SOUTH_EAST = 'soea',

  SOUTH_WEST = 'sowe',

  CENTER = 'ce',

  SMART = 'sm',
}

export class Proxy {
  private readonly _uri: string;

  private readonly _signature: string;

  constructor(uri?: string, signature?: string) {
    this._signature = signature || '1';
    this._uri = uri || (process.env.NEXT_PUBLIC_IMAGE_PROXY as string);
  }

  public generate(
    source: string,
    width: number,
    height: number,
    extension: string = 'png',
    resizingType: Resizing = Resizing.FILL,
    gravity: Gravity = Gravity.SMART,
    enlarge: boolean = true,
  ): string {
    return `${this._uri}/${this._signature}/${resizingType}/${width}/${height}/${gravity}/${enlarge}/plain/${source}@${extension}`;
  }
}
