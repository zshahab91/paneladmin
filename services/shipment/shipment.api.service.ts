import Order from "@entities/order";
import { IPaginatedResponse } from "@entities/paginated.response";
import { IShipment } from "@entities/shipment";
import { ApiService } from "@services/api/api.service";
import { request } from "@services/http/adapter";
import { AxiosResponse } from "axios";

export class ShipmentApiService extends ApiService{
    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<IShipment>>> {
        return request.get(`/admin/order-shipments/${id}`);
    }

}
