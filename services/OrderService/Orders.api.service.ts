import { IBalance } from "@entities/balance";
import { IResponse } from "@entities/response";
import { request } from '@services/http/adapter';
import { ApiService } from '@services/api/api.service';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';
import Order, {
    AddOrderOption,
    ChangeDeliveryAddress,
    ChangeOrderStatus,
    EditOrderOption,
    EditOrderItemPrice,
    OrderShipment,
    DatesShipment,
    Note,
    IExtra,
} from '@entities/order';

class OrdersApiService extends ApiService {
    public static getList(): Promise<AxiosResponse<IPaginatedResponse<Order[]>>> {
        return request.get(`/admin/orders`);
    }

    public static findByCustomerId(
        customer: number,
    ): Promise<AxiosResponse<IPaginatedResponse<Order[]>>> {
        return request.get(`/admin/orders?filter[customer.id]=${customer}`);
    }

    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<Order, IExtra>>> {
        return request.get(`/admin/orders/${id}`);
    }

    public static add(
        body: AddOrderOption,
    ): Promise<AxiosResponse<IPaginatedResponse<Order>>> {
        return request.post(`/admin/orders`, body);
    }

    public static edit(
        body: EditOrderOption,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<Order>>> {
        return request.patch(`/admin/orders/${id}`, body);
    }

    public static editPriceItem(
        body: EditOrderItemPrice,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<Order>>> {
        return request.patch(`/admin/orders/${id}/items`, body);
    }

    public static changeStatus(
        id: string,
        body: ChangeOrderStatus,
    ): Promise<AxiosResponse<IPaginatedResponse<Order>>> {
        return request.patch(`/admin/orders/${id}/status`, body);
    }

    public static changeDeliveryAddress(
        id: number,
        body: ChangeDeliveryAddress,
    ): Promise<AxiosResponse<IPaginatedResponse<Order>>> {
        return request.patch(`/admin/orders/${id}/delivery-address`, body);
    }

    public static getShipments(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<OrderShipment[]>>> {
        return request.get(`/admin/orders/${id}/shipments`);
    }

    public static updateShipmentStatus(
        id: string | number,
        status: string,
    ): Promise<AxiosResponse<IPaginatedResponse<OrderShipment[]>>> {
        return request.patch(`/admin/order-shipments/${id}/status`, { status });
    }

    public static updateShipmentGrandTotal(
        id: string | number,
        grand_total: string | number,
    ): Promise<AxiosResponse<IPaginatedResponse<OrderShipment[]>>> {
        return request.patch(`/admin/order-shipments/${id}/grand-total`, {
            grand_total,
        });
    }

    public static getDateShipments(
        id: number,
    ): Promise<AxiosResponse<IPaginatedResponse<DatesShipment>>> {
        return request.get(`/admin/order-shipments/${id}/delivery-dates`);
    }

    public static updateShipmentDeliveryDate(
        id: string | number,
        delivery_date: string,
    ): Promise<AxiosResponse<IPaginatedResponse<OrderShipment[]>>> {
        return request.patch(`/admin/order-shipments/${id}/delivery-date`, {
            delivery_date,
        });
    }

    public static getBalance(id: string): Promise<AxiosResponse<IResponse<IBalance>>> {
        return request.get(`/admin/orders/${id}/balance-status`)
    }

    public static refund(id: string, refund: { tracking_number: number, paid_at: string, description: string }): Promise<AxiosResponse<IResponse<IBalance>>> {
        return request.post(`/admin/orders/${id}/balance-refund`, refund)
    }

    public static getNotes(
        id: string | number,
    ): Promise<AxiosResponse<IPaginatedResponse<Note[]>>> {
        return request.get(`/admin/orders/${id}/notes`);
    }

    public static addNote(
        id: string | number,
        description: string,
    ): Promise<AxiosResponse<IPaginatedResponse<Note[]>>> {
        return request.post(`/admin/orders/${id}/notes`, {
            description,
        });
    }

    public static deleteItem(id: string | number): Promise<AxiosResponse<IPaginatedResponse<Note[]>>> {
        return request.delete(`/admin/order-items/${id}`);
    }

    public static updateItems(
        identifier: string | number,
        items: { id: number, price?: number, quantity?: number }[]
    ): Promise<AxiosResponse<IPaginatedResponse<Note[]>>> {
        return request.patch(`/admin/orders/${identifier}/items`, { items });
    }
}

export default OrdersApiService;
