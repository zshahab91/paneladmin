import { IHttpAdapter, request } from "@services/http/adapter";
import { ICustomer } from "@entities/customer";
import { AxiosResponse } from "axios";
import { IResponse } from "@entities/response";

export class CustomerApiService {
    private _adapter: IHttpAdapter;

    public constructor() {
        this._adapter = request;
    }

    public create(customer: ICustomer): Promise<AxiosResponse<IResponse<ICustomer>>> {
        return this._adapter.post(`/admin/customers`, customer);
    }

    public update(id: number, customer: ICustomer): Promise<AxiosResponse<IResponse<ICustomer>>> {
        return this._adapter.patch(`/admin/customers/${id}`, customer);
    }

    public show(id: number): Promise<AxiosResponse<IResponse<ICustomer>>> {
        return this._adapter.get(`/admin/customers/${id}`);
    }
}