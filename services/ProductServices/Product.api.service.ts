import { request } from '@services/http/adapter';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';
import Product, { EditProductProps, AddProductProps } from '@entities/product';
import Brand from '@entities/brand';
import ShippingCategory from '@entities/shippingCategory';
import ProductVariant from '@entities/productVariant';
import ProductOption from '@entities/productOption';
import Category from '@entities/category';
import { InventoryDTO } from "@entities/inventories";

interface ImageResponse {
  path: string;
  alt: string;
}

interface ProductProps {
  images: ImageResponse[];
  featuredImage: ImageResponse;
  id: number;
  title: string;
  subtitle: string;
  code: string;
  description: string;
  isActive: true;
  weight: number;
  height: number;
  width: number;
  length: number;
  brand: Brand;
  metaDescription: string;
  shippingCategory: ShippingCategory;
  status: 'SOON';
  productVariants: ProductVariant[];
  options: ProductOption[];
  category: Partial<Category>;
}

interface media {
  imageFile: string;
}

interface IServiceDataHook<TData> {
  data?: TData;
  isLoading: boolean;
}

export default class ProductsService {
  public static getList(): Promise<
    AxiosResponse<IPaginatedResponse<Product[]>>
  > {
    return request.get(`/admin/products`);
  }

  public static getById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Product>>> {
    return request.get(`/admin/products/${id}`);
  }

  public static add(
    body: AddProductProps,
  ): Promise<AxiosResponse<IPaginatedResponse<ProductProps>>> {
    return request.post(`/admin/products`, body);
  }

  public static edit(
    body: EditProductProps,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ProductProps>>> {
    return request.patch(`/admin/products/${id}`, body);
  }

  public static createInventory(
      body: InventoryDTO
  ): Promise<AxiosResponse<ProductProps>> {
    return request.post(`/admin/product/variants/create-inventory`, body);
  }
}
