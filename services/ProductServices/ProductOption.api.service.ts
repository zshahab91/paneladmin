import { request } from '@services/http/adapter';
import { ApiService } from '@services/api/api.service';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';

import ProductOption, {
  AddProductOption,
  EditProductOption,
} from '@entities/productOption';

class ProductOptionApiService extends ApiService {

  public static getList(pageIndex?: number, query?: string): Promise<
    AxiosResponse<IPaginatedResponse<ProductOption[]>>
  > {
    return request.get(`/admin/product/options?page=${pageIndex ? pageIndex : 1}${query ? query : ''}`);
  }


  public static getById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
    return request.get(`/admin/product/options/${id}`);
  }

  public static add(
    body: AddProductOption,
  ): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
    return request.post(`/admin/product/options`, body);
  }

  public static edit(
    body: EditProductOption,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
    return request.patch(`/admin/product/options/${id}`, body);
  }
}

export default ProductOptionApiService;
