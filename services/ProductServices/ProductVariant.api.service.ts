import { request } from "@services/http/adapter";
import { AxiosResponse } from "axios";
import { IPaginatedResponse } from "@entities/paginated.response";
import ProductVariant, { EditProductVariant, AddProductVariant } from "@entities/productVariant";





export default class ProductVariantService {



    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<ProductVariant>>>{
        return request.get(`/admin/product/variants/${id}`);
    }

    public static add(
      body: AddProductVariant,
    ): Promise<AxiosResponse<IPaginatedResponse<ProductVariant>>> {
        return request.post(`/admin/product/variants`, body);
    }


    public static edit(
        body: EditProductVariant,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<ProductVariant>>> {
        return request.patch(`/admin/product/variants/${id}`, body);
    }
    
    
}
