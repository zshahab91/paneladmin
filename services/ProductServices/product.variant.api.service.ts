import { request } from "@services/http/adapter";
import { AxiosResponse } from "axios";
import { IPaginatedResponse } from "@entities/paginated.response";
import ProductVariant from "@entities/productVariant";

export class ProductVariantApiService {

    public static index(): Promise<AxiosResponse<IPaginatedResponse<ProductVariant[]>>> {
        return request.get(`/admin/product/variants?limit=9999999`)
    }
}