import { IHttpAdapter, request } from "@services/http/adapter";
import { AxiosResponse } from "axios";
import { ISeller } from "@entities/seller";
import { IResponse } from "@entities/response";


export class SellerApiService {
    private _adapter: IHttpAdapter;

    public constructor() {
        this._adapter = request;
    }

    public show(id: number): Promise<AxiosResponse<IResponse<ISeller>>> {
        return this._adapter.get(`/admin/sellers/${id}`);
    }

    public create(seller: ISeller): Promise<AxiosResponse<IResponse<ISeller>>> {
        return this._adapter.post(`/admin/sellers`, seller);
    }

    public update(id: number, seller: Partial<ISeller>): Promise<AxiosResponse<IResponse<ISeller>>> {
        return this._adapter.patch(`/admin/sellers/${id}`, seller);
    }
}