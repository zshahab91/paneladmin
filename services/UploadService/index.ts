import { request as http } from '@services/http/adapter';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';

interface UploadProps {
    imageFileName: string;
    media: { path: string; url: string }
}

export default class UploadService {
    public static add(
        type: string,
        body: File,
    ): Promise<AxiosResponse<IPaginatedResponse<UploadProps>>> {
        const formData = new FormData();
        formData.append('imageFile', body);
        return http.post(`/admin/media/${type}/upload`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
    }
}
