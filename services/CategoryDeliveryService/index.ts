import CategoryLead from "@entities/categoryLead";
import { IPaginatedResponse } from "@entities/paginated.response";
import { request } from "@services/http/adapter";
import { AxiosResponse } from "axios";

interface CategoryDeliveryProps {
    id: number,
    category: {
        id: string,
        title: string
    },
    start: string,
    end: string,
}

interface IServiceDataHook<TData> {
    data?: TData;
    isLoading: boolean;
}

export default class CategoryDeliveryService {


    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryDeliveryProps>>> {
        return request.get(`/admin/category-deliveries/${id}`);
    }

    public static add(
        body: Partial<CategoryLead>,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryDeliveryProps>>> {
        return request.post(`/admin/category-deliveries`, body);
    }

    public static edit(
        body: Partial<CategoryLead>,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryDeliveryProps>>> {
        return request.patch(`/admin/category-deliveries/${id}`, body);
    }

    public static delete(
        id: number,
    ): Promise<AxiosResponse<IPaginatedResponse<{}>>> {
        return request.delete(`/admin/category-deliveries/${id}`);
    }


}
