import { IPaginatedResponse } from '@entities/paginated.response';

import ProductOption from '@entities/productOption';
import ProductOptionValue from "@entities/productOptionValue";
import { IResponse } from "@entities/response";
import { ApiService } from '@services/api/api.service';
import { request } from '@services/http/adapter';
import { AxiosResponse } from 'axios';

class DressSizesService extends ApiService {

    public static async  show(id: number): Promise<AxiosResponse<IResponse<ProductOptionValue>>> {
        return request.get(`/admin/product/options/values/${id}`);
    }

    public static async add(size: Partial<ProductOption>): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
        return request.post(`/admin/product/options/dress-size/values`, size);
    }

    public static async edit(
        id: number,
        size: Partial<ProductOption>
    ): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
        return request.patch(`/admin/product/options/values/${id}`, size);
    }
}


export default DressSizesService;
