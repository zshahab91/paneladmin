import { IHttpAdapter, Adapter as HttpAdapter } from "@services/http/adapter";
import { ApiService } from "@services/api/api.service";
import { AxiosResponse } from "axios";
import { IPaginatedResponse } from "../../entities/paginated.response";
import { IAdmin } from "../../entities/admin";
import { IResponse } from "../../entities/response";

export class AdminApiService extends ApiService {
    private readonly _adapter: IHttpAdapter;

    public constructor() {
        super();
        this._adapter = HttpAdapter({ baseURL: `http://timcheh.test/` });
    }

    public grid(): Promise<AxiosResponse<IPaginatedResponse<IAdmin[]>>> {
        return this._adapter.get(`/admin/admins`)
    }

    public create(admin: IAdmin): Promise<AxiosResponse<IResponse<IAdmin[]>>> {
        return this._adapter.post(`/admin/admins`, admin)
    }

    public update(id: number, admin: IAdmin): Promise<AxiosResponse<IResponse<IAdmin[]>>> {
        return this._adapter.patch(`/admin/admins/${id}`, admin);
    }


    public show(id: number): Promise<AxiosResponse<IResponse<IAdmin>>> {
        return this._adapter.get(`/admin/admins/${id}`);
    }

}