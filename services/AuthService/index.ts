import {TokenProps} from '@services/AuthService/models';
import {request} from '@services/http/adapter';
import GlobalEvents from '@services/GlobalEvents';
import {loadTokensMapFromLocalStorage, removeTokensInLocalStorage, saveTokensInLocalStorage,} from './localStorage';

let token = loadTokensMapFromLocalStorage();

class AuthServiceClass {
  // #region signIn/signOut

  public async signIn(username: string, password: string): Promise<boolean> {
    const { status, data } = await request.post('/admin/security/login', {
      username,
      password,
    });
    if (Math.floor(status / 100) === 2) {
      this.setTokenInfo(data);
      return true;
    }

    return false;
  }

  public async signOut() {
    return this.removeTokenInfo();
  }

  // #endregion

  // #region token

  public setTokenInfo(value: TokenProps): void {
    token = value;
    saveTokensInLocalStorage(value);
    GlobalEvents.dispatchEvent('token', token);
  }

  public getTokenInfo(): TokenProps | undefined {
    return token;
  }

  public removeTokenInfo(): void {
    token = undefined;
    removeTokensInLocalStorage();
    GlobalEvents.dispatchEvent('token', token);
  }

  // #endregion
}

const AuthService = new AuthServiceClass();
export default AuthService;
