import {TokenProps} from '@services/AuthService/models';
import {destroyCookie, parseCookies, setCookie} from "nookies";


const localStorageKey = 'token';

export function saveTokensInLocalStorage(token: TokenProps): void {
    const stringyToken = JSON.stringify(token);
    // eslint-disable-next-line @rushstack/no-null
    setCookie(null, localStorageKey, stringyToken, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
    });
}

export function removeTokensInLocalStorage(): void {
    // eslint-disable-next-line @rushstack/no-null
    destroyCookie(null, localStorageKey, {
        path:'/'
    });
}


export function loadTokensMapFromLocalStorage(): TokenProps | undefined {
    const cookies = parseCookies();

    const stringyToken = cookies[localStorageKey];
    if (stringyToken)
        try {
            return JSON.parse(stringyToken);
        } catch (e) {
            console.error(e);
        }

    return undefined;
}
