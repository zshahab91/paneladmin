export interface TokenProps {
    token: string;
    expireDate: number;
}
