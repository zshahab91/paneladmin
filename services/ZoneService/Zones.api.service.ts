import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import Zone from '@entities/zone';

class ZonesApiService extends ApiService {
  public static getList(): Promise<
    AxiosResponse<IPaginatedResponse<Zone[]>>
    > {
    return request.get(`/admin/zones`);
  }
}

export default ZonesApiService;
