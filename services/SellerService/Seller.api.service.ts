import { request } from '@services/http/adapter';
import { ApiService } from '@services/api/api.service';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';

import Seller, {AddSellerCategory, EditSellerCategory} from "@entities/seller";

class SellerApiService extends ApiService {
  public static getList(pageIndex?:number,query?:string): Promise<
    AxiosResponse<IPaginatedResponse<Seller[]>>
    > {
    return request.get(`/admin/sellers?page=${pageIndex? pageIndex : 1}${query ? query: ''}`);
  }

  public static getById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Seller>>> {
    return request.get(`/admin/sellers/${id}`);
  }

  public static add(
    body: AddSellerCategory,
  ): Promise<AxiosResponse<IPaginatedResponse<Seller>>> {
    return request.post(`/admin/sellers/`, body);
  }

  public static edit(
    body: EditSellerCategory,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Seller>>> {
    return request.patch(`/admin/sellers/${id}`, body);
  }
}

export default SellerApiService;
