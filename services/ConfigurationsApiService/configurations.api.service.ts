import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import Configuration from "@entities/configurations";

class ConfigurationsApiService extends ApiService {

    public edit(
        configs: Partial<Configuration>[]
    ): Promise<AxiosResponse<IPaginatedResponse<{}>>> {
        return request.post(`/admin/configurations/bulk`, {
            configs
        });
    }

}

const ConfigurationsService = new ConfigurationsApiService();
export default ConfigurationsService;
