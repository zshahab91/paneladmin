import { useEffect, useState } from "react";
import { request } from "@services/http/adapter";
import { AxiosResponse } from "axios";
import { IPaginatedResponse } from "@entities/paginated.response";
import BrandProps, { AddBrandProps, EditBrandProps } from "@entities/brand";

interface IServiceDataHook<TData> {
    data?: TData;
    isLoading: boolean;
}

export default class BrandsService {
    public static useData(): IServiceDataHook<BrandProps[]> {
        const [data, setData] = useState<BrandProps[] | undefined>(undefined);
        const [isLoading, setIsLoading] = useState(false);


        const fetchData = (): void => {
            setIsLoading(true);

            request.get("/admin/brands")
                .then(({ data: { results, succeed } }) => {
                    if (succeed)
                        setData(results);

                }).catch(console.error)
                .finally(() => setIsLoading(false))
        }

        useEffect(() => fetchData(), []);

        return { data, isLoading }
    }


    public static getList(pageIndex?: number, query?: string): Promise<
        AxiosResponse<IPaginatedResponse<BrandProps[]>>
    > {
        return request.get(`/admin/brands?page=${pageIndex ? pageIndex : 1}${query ? query : ''}`);
    }

    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<BrandProps>>> {
        return request.get(`/admin/brands/${id}`);
    }

    public static add(
        body: AddBrandProps,
    ): Promise<AxiosResponse<IPaginatedResponse<BrandProps>>> {
        return request.post(`/admin/brands`, body);
    }

    public static edit(
        body: EditBrandProps,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<BrandProps>>> {
        return request.patch(`/admin/brands/${id}`, body);
    }

    public static delete(
        id: number,
    ): Promise<AxiosResponse<IPaginatedResponse<{}>>> {
        return request.delete(`/admin/brands/${id}`);
    }
}
