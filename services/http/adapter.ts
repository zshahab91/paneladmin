import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import AuthService from '@services/AuthService';
import Router from 'next/router';
import * as Sentry from '@sentry/node';

export interface IHttpAdapter extends AxiosInstance {}

export const baseUrl = process.env.NEXT_PUBLIC_API_HOST;

const exclusions: string[] = [
  '/places/provinces',
  '/places/cities',
  '/places/districts',
];

function addAuthorizationHeaderToInstance(instance: AxiosInstance) {
  instance.interceptors.request.use(
    (request: AxiosRequestConfig) => {
      if (exclusions.some((e) => request.url?.includes(e))) return request;

      const token = AuthService.getTokenInfo()?.token;
      if (token) request.headers.Authorization = 'Bearer ' + token;
      return request;
    },
    (error) => {
      return Promise.reject(error);
    },
  );
}

function addRedirectionWhenGetting401ErrorToInstance(instance: AxiosInstance) {
  instance.interceptors.response.use(
    (response) => response,
    (error) => {
      if (
        error?.response?.status === 401 &&
        !Router.pathname.includes('/auth/sign-in')
      ) {
        Router.replace(`/auth/sign-in?returnUrl=${Router.pathname}`).catch(
          console.error,
        );
        AuthService.removeTokenInfo();
      }
      return Promise.reject(error);
    },
  );
}

function addSentryLogWhenGettingError(instance: AxiosInstance) {
  instance.interceptors.response.use(
    (response) => response,
    (error) => {
      /**
       * Catch Api Call Errors
       * todo, this is experimental, and needs some refactor.
       */
      Sentry.withScope((scope) => {
        scope.setExtra('Axios Response', error?.response);
        scope.setTag('statusCode', error?.response?.status);
        scope.setTag('env', 'production');
        Sentry.captureMessage(
          `Error ${error?.response?.status}: ${error?.response?.message}`,
          Sentry.Severity.Debug,
        );
      });
      return Promise.reject(error);
    },
  );
}

export function Adapter(config?: AxiosRequestConfig): AxiosInstance {
  const instance: AxiosInstance = axios.create(config);

  instance.defaults.baseURL = baseUrl;
  instance.interceptors.request.use(
    (config) => {
      if (config.params && !config.params.hasOwnProperty('sort')) {
        config.params.sort = ['-id'];
      }

      return Promise.resolve(config);
    },
    (error) => Promise.reject(error),
  );
  addRedirectionWhenGetting401ErrorToInstance(instance);
  addAuthorizationHeaderToInstance(instance);
  addSentryLogWhenGettingError(instance);

  return instance;
}

export const request: IHttpAdapter = Adapter();
