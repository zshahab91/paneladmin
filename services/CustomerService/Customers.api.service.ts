import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import Customer from '@entities/customer';

class CustomersApiService extends ApiService {
  public static getList(): Promise<
    AxiosResponse<IPaginatedResponse<Customer[]>>
    > {
    return request.get(`/admin/customers`);
  }

  public static getById(
    id: string | number,
  ): Promise<AxiosResponse<IPaginatedResponse<Customer>>> {
    return request.get(`/admin/customers/${id}`);
  }
}

export default CustomersApiService;
