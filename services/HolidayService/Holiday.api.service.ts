import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';

import Holiday, {AddHoliday, EditHoliday} from '@entities/holiday';

class HolidayApiService extends ApiService {
  public static getList(): Promise<AxiosResponse<IPaginatedResponse<Holiday[]>>> {
    return request.get(`/admin/holidays`);
  }

  public static getById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Holiday>>> {
    return request.get(`/admin/holidays/${id}`);
  }

  public static add(
    body: AddHoliday,
  ): Promise<AxiosResponse<IPaginatedResponse<Holiday>>> {
    return request.post(`/admin/holidays`, body);
  }

  public static edit(
    body: EditHoliday,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Holiday>>> {
    return request.patch(`/admin/holidays/${id}`, body);
  }

  public static delete(
    id: number,
  ): Promise<AxiosResponse<IPaginatedResponse<Holiday>>> {
    return request.delete(`/admin/holidays/${id}`);
  }
}

export default HolidayApiService;
