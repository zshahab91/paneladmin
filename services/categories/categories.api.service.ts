import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import Category, {AddCategoryProps, EditCategoryProps} from '@entities/category';
import _ from 'lodash';

class CategoriesApiService extends ApiService {

  public  grid(pageIndex?:number,query?:string): Promise<
  AxiosResponse<IPaginatedResponse<Category[]>>
  > {
  return request.get(`/admin/categories?page=${pageIndex? pageIndex : 1}&limit=30&${query ? query: ''}`);
}

  public categoryById(
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Category>>> {
    return request.get(`/admin/categories/${id}`);
  }

  public add(
    body: AddCategoryProps,
  ): Promise<AxiosResponse<IPaginatedResponse<Category>>> {
    return request.post(`/admin/categories`, body);
  }

  public edit(
    body: EditCategoryProps,
    id: string,
  ): Promise<AxiosResponse<IPaginatedResponse<Category>>> {
    return request.patch(`/admin/categories/${id}`, body);
  }
  public searchParentCategory(data:  IPaginatedResponse<Category[]>): Category[] {

    const oldData = data.results;
    const newData: object[] = [];
    _.forEach(oldData, function(value) {
      const id = value.id;
      //@ts-ignore
      const filterData = _.filter(oldData, function(item) { return item.parent !== null && item.parent.id === id });
      if(filterData.length === 0){
         newData.push(value);
      }

    });
    //@ts-ignore
     return newData;

  }

}

const categoriesService = new CategoriesApiService();
export default categoriesService;
