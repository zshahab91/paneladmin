import CategoryCommission from '@entities/categoryCommission';
import { IPaginatedResponse } from '@entities/paginated.response';
import { request } from '@services/http/adapter';
import { AxiosResponse } from 'axios';

export default class CategoryCommissionsService {
    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryCommission>>> {
        return request.get(`/admin/category-commissions/${id}`);
    }

    public static add(
        body: Partial<CategoryCommission>,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryCommission>>> {
        return request.post(`/admin/category-commissions`, body);
    }

    public static edit(
        body: Partial<CategoryCommission>,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryCommission>>> {
        return request.patch(`/admin/category-commissions/${id}`, body);
    }

    public static delete(
        id: number,
    ): Promise<AxiosResponse<IPaginatedResponse<{}>>> {
        return request.delete(`/admin/category-commissions/${id}`);
    }
}
