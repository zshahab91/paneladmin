import {request} from "@services/http/adapter";
import {AxiosResponse} from "axios";
import {IPaginatedResponse} from "@entities/paginated.response";
import CategoryLead from "@entities/categoryLead";


export default class CategoryDeliveryService {

    public static getById(
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryLead>>> {
        return request.get(`/admin/category-leads/${id}`);
    }

    public static add(
        body: Partial<CategoryLead>,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryLead>>> {
        return request.post(`/admin/category-leads`, body);
    }

    public static edit(
        body: Partial<CategoryLead>,
        id: string,
    ): Promise<AxiosResponse<IPaginatedResponse<CategoryLead>>> {
        return request.patch(`/admin/category-leads/${id}`, body);
    }

    public static delete(
        id: number,
    ): Promise<AxiosResponse<IPaginatedResponse<{}>>> {
        return request.delete(`/admin/category-leads/${id}`);
    }


}
