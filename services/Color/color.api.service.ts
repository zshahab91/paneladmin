import ProductOptionValue from "@entities/productOptionValue";
import { IResponse } from "@entities/response";
import { request } from '@services/http/adapter';
import { ApiService } from '@services/api/api.service';
import { AxiosResponse } from 'axios';
import { IPaginatedResponse } from '@entities/paginated.response';

import ProductOption, { Color } from '@entities/productOption';

class ColorService extends ApiService {

    public static async  show(id: number): Promise<AxiosResponse<IResponse<ProductOptionValue>>> {
        return request.get(`/admin/product/options/values/${id}`);
    }

    private static async _getColorValues() {
        const { status, data } = await request.get('/admin/product/options', {
            params: { 'filter[code]': 'color' },
        });
        if (Math.floor(status / 100) !== 2)
            throw new Error('Oppps ! status code is ' + status);

        const productOption: ProductOption = data?.results[0];
        if (!productOption)
            throw new Error('Could not find guarantees in product options !');

        const colorValues: Color[] = (productOption.values || []) as Color[];

        return { productOption, colorValues };
    }


    public static async add(color: Partial<ProductOption>): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
        return request.post(`/admin/product/options/color/values`, color);
    }

    public static async edit(
        id: number,
        color: Partial<ProductOption>
    ): Promise<AxiosResponse<IPaginatedResponse<ProductOption>>> {
        return request.patch(`/admin/product/options/values/${id}`, color);
    }
}

export default ColorService;
