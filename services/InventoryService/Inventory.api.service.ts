import {request} from '@services/http/adapter';
import {ApiService} from '@services/api/api.service';
import {AxiosResponse} from 'axios';
import {IPaginatedResponse} from '@entities/paginated.response';
import Inventory from "@entities/inventories";
import { IResponse } from "@entities/response";

class InventoryApiService extends ApiService {
    public static delete(
        id: number,
    ): Promise<AxiosResponse<IPaginatedResponse<{}>>> {
        return request.delete(`/admin/inventories/${id}`);
    }

    public static create(inventory: Inventory): Promise<AxiosResponse<IPaginatedResponse<Inventory>>> {
        return request.post(`/admin/inventories/`, inventory);
    }

    public static show(id: number): Promise<AxiosResponse<IResponse<Inventory>>> {
        return request.get(`/admin/inventories/${id}`);
    }

    public static update(id: number, inventory: Inventory): Promise<AxiosResponse<IResponse<Inventory>>> {
        return request.patch(`/admin/inventories/${id}`, inventory);
    }
}

export default InventoryApiService;
