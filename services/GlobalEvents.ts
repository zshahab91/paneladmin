const eventMap = new Map();

type HandlerType<TData> = (data: TData) => void;

class GlobalEventsClass {
  public addEventListener<TData>(eventName: string, handler: HandlerType<TData>): void {
    const eventList: HandlerType<TData>[] = eventMap.get(eventName) || [];
    eventList.push(handler);
    eventMap.set(eventName, eventList);
  }

  public removeEventListener<TData>(
    eventName: string,
    handler: (data: TData) => void,
  ): void {
    const eventList: HandlerType<TData>[] = eventMap.get(eventName) || [];
    const ix = eventList.indexOf(handler);
    if (ix === -1)
      return console.error('Handler not exists for eventName', {
        eventName,
        handler,
      });

    eventList.splice(ix, 1);
  }

  public dispatchEvent<TData>(eventName: string, data: TData): void {
    const eventList: HandlerType<TData>[] = eventMap.get(eventName) || [];
    eventList.forEach((handler) => handler(data));
  }
}

const GlobalEvents = new GlobalEventsClass();
export default GlobalEvents;
