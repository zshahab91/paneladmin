import { IAdmin } from "@entities/admin";
import Order from "@entities/order";
import OrderItem, { ICustomOrderItem } from "@entities/orderItems";
import ShippingPeriod from "@entities/shippingPeriod";

export interface IShipmentLog {
    createdAt: string

    statusFrom: string

    statusTo: string

    user: IAdmin
}

export interface IShipment {
    id: number

    grandTotal: number

    order: Partial<Order>

    orderItemsCount: number

    orderItems: OrderItem[];

    status: string

    subTotal: number

    sumGrandTotal: number

    orderShipmentStatusLogs: IShipmentLog[]
}


export interface ICustomShipment {
    id: number

    grandTotal: number

    order: Partial<Order>

    orderItemsCount: number

    status: string

    subTotal: number

    sumGrandTotal: number

    orderShipmentStatusLogs: IShipmentLog[]

    orderItems: ICustomOrderItem[];

    deliveryDate: string

    period: ShippingPeriod
}
