export default interface ShippingPeriod {
    id: number,
    isActive: boolean,
    start: string, // todo isoString date
    end: string // todo isoString date
}
