import { IAdmin } from '@entities/admin';
import { ICustomer } from "@entities/customer";
import { IOrderDocument, IRefundDocument } from "@entities/document";
import OrderItem from '@entities/orderItems';
import { City } from "@entities/zone";

export enum OrderStatusEnum {
  'CALL_FAILED' = 'Call Failed',
  'CANCELED' = 'Canceled',
  'CANCELED_SYSTEM' = 'Canceled System',
  'CONFIRMED' = 'Confirmed',
  'DELIVERED' = 'Delivered',
  'NEW' = 'New',
  'REFUND' = 'Refund',
  'WAITING_FOR_PAY' = 'Waiting For Pay',
  'WAIT_CUSTOMER' = 'Wait Customer'
}

export enum ShipmentStatusEnum {
  'NEW' = 'New',
  'WAITING_FOR_SUPPLY' = 'Waiting For Supply',
  'PREPARING' = 'Preparing',
  'WAITING_FOR_SEND' = 'Waiting For Send',
  'SENT' = 'Sent',
  'DELIVERED' = 'Delivered',
  'CANCELED' = 'Canceled',
  'AFTER_SALES' = 'After Sales',
  'RETURNING' = 'Returning',
  'RETURNED' = 'Returned',
  'THIRD_PARTY_LOGISTICS' = 'Third Party Logistics',
}

export interface OrderAddress {
  id: number;
  unit: number;
  number: number;
  name: string;
  family: string;
  nationalCode: number;
  phone: string;
  postalCode: number;
  district: { id: number; name: string };
  city: City;
  fullAddress: string;
}

export default interface Order {
  id: number;
  status: keyof typeof OrderStatusEnum;
  identifier: string;
  subtotal: number;
  grandTotal: number;
  paymentMethod: string;
  customer: Partial<ICustomer>;
  orderAddress: OrderAddress;
  orderItems: OrderItem[];
  createdAt: string;
  updatedAt: string;
  orderDocument: IOrderDocument;
  refundDocuments: IRefundDocument[];
  statusLogs: {
    createdAt: string;
    statusFrom: string;
    statusTo: string;
    user: IAdmin;
  }[];
}

export interface AddOrderOption extends Omit<Order, 'id' | 'code'> {}

export interface EditOrderOption extends Omit<Order, 'id' | 'code'> {}

export interface ChangeOrderStatus {
  status: string;
  force: number;
}

export interface ChangeDeliveryAddress {
  address: number;
}

export interface EditOrderItemPrice {
  items: [
    {
      id: number;
      price: number;
    },
  ];
}

export interface OrderShipment {
  id: number;
  subTotal: number;
  grandTotal: number;
  status: keyof typeof ShipmentStatusEnum;
  deliveryDate: Date; // 2020-08-13T01:46:23+00:00
  trackingCode: string;
  orderItems: OrderItem[];
  period: { start: string; end: string };
}

export interface Period {
  start: string;
  end: string;
}

export interface DatesShipment {
  dates: string[];
  periods: Period[];
}

export interface Note {
  id: number;
  admin: {
    id: number;
    name: string;
    family: string;
    createdAt: string;
    updatedAt: string;
  };
  description: string;
  createdAt: string;
  updatedAt: string;
}

export interface IExtra {
  validTransitions?: string[];
}
