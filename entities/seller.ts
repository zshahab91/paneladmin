export default interface Seller {
    id: number,
    name: string,
    username: string,
    nationalIdentifier: string,
}

export interface AddSellerCategory extends Omit<Seller, 'id'> {
}

export interface EditSellerCategory extends Omit<Seller, 'id'> {
}

export interface ISeller extends Seller {

}
