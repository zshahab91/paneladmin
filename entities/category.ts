export default interface Category {
  id: number;
  parent?: {
    id?: number;
  };
  code: string;
  title: string;
  subtitle: string;
  configurations?: [];
  image?: { path: string; alt: string };
  leaf: boolean
  level: number,
  pageTitle?: string;
  description ?: string;
  metaDescription ?: string;
}

export interface AddCategoryProps extends Omit<Category, 'id'  | 'parent' | 'leaf'> {
  parent: {};
  configurations: [];
  level: number;
}

export interface EditCategoryProps extends Omit<Category, 'id' | 'parent' | 'leaf'> {
  parent?: {};
  configurations?: [];
  level: number;
}
