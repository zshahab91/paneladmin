import ProductVariant, { ICustomProductVariant } from '@entities/productVariant';
import Seller from '@entities/seller';

export interface InventoryDTO {
  position: number

  code: string

  product: number

  optionValues: number[]

  seller: number

  stock: number

  price: number

  finalPrice: number

  isTrackable: true

  maxPurchasePerOrder: number

  suppliesIn: number | string
}

export default interface Inventory {
  id: number;
  title: string;
  stock: number;
  price: number;
  finalPrice: number;
  isTrackable: boolean;
  maxPurchasePerOrder: number;
  suppliesIn: number;
  orderCount: number;
  variant?: ProductVariant;
  seller?: Seller;
}

export interface ICustomInventory {
  id: number;
  title: string;
  stock: number;
  price: number;
  finalPrice: number;
  isTrackable: boolean;
  maxPurchasePerOrder: number;
  suppliesIn: number;
  orderCount: number;
  variant?: ICustomProductVariant;
  seller?: Seller;
}
