import ProductOption from "./productOption";

export default interface ProductOptionValue {
  id: number;
  value: string;
  code: string;
  attributes: { [s: string]: unknown };
  option: ProductOption
}
