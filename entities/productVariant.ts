import ProductOptionValue from "@entities/productOptionValue";
import Product from "@entities/product";

export default interface ProductVariant {
    id: number;
    code: string;
    position: number;
    options?: { [name: string]: ProductOptionValue };
    optionValues: Partial<ProductOptionValue>[];
    product: Product
}

export interface ICustomProductVariant {
    id: number;
    code: string;
    position: number;
    optionValues: { [name: string]: ProductOptionValue };
    product: Product
}

export interface AddProductVariant extends Omit<ProductVariant, 'id'> {}

export interface EditProductVariant extends Omit<ProductVariant, 'id'> {}
