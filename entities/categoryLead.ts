import Category from "@entities/category";

export default interface CategoryLead {
    id: number;
    value: number;
    category: Partial<Category>;
}
