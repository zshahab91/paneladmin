import { ISeller } from "@entities/seller";
import ProductOption from "./productOption";
import ProductVariant from "./productVariant";
import Brand from "@entities/brand";
import ShippingCategory from "@entities/shippingCategory";
import Category from "@entities/category";

export interface ImageResponse {
    path: string,
    alt: string,
    url?: string
}


export default interface Product {
    id: number,
    title: string,
    subtitle: string,
    code: string,
    description: string,
    isOriginal: boolean,
    isActive: boolean,
    weight: number,
    height: number,
    width: number,
    length: number,
    brand: Brand,
    maxLead?: number,
    metaDescription: string,
    shippingCategory: ShippingCategory,
    status: "SOON",
    productVariants: ProductVariant[],
    options: ProductOption[],
    category: Partial<Category>,
    featuredImage: ImageResponse,
    images: ImageResponse[] | undefined,
    seller: ISeller | number
}
export interface AddProductProps extends  Omit<Product, 'id'> {
}

export interface EditProductProps  extends Omit<Product, 'id'> {

}

// const x :EditProductProps = {id:1}
