
export interface IBalance {
    orderDocumentAmount: number

    totalTransactionAmounts: number

    totalOrderRefundDocumentAmounts: number

    totalRefundTransactionAmounts: number

    balanceAmount: number

    balanceStatus: string
}
