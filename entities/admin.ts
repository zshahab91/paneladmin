export  interface IAdmin {
    id?: number;

    name?: string;

    family?: string;

    email?: string;

    mobile?: string

    active?: boolean;

    password?: string;
}
