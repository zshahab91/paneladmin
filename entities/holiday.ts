import Seller from '@entities/seller';

export default interface Holiday {
  id: number;
  seller: Seller;
  title: string;
  date: string;
  supply: boolean;
}

export interface AddHoliday extends Omit<Holiday, 'id' | 'seller'> {
  seller: Seller;
}

export interface EditHoliday extends Omit<Holiday, 'id' | 'seller'> {
  seller: Seller;
}
