export interface ITransaction {
    amount: number

    createdAt: string

    gateway: string

    paidAt: string | null

    status: string

    trackingNumber: string | null

    updatedAt: string
}
