export default interface Configuration {
    id: number;
    code: string;
    value: "true" | "false" | string;
}

export enum PaymentMethods {
    cpg = "CPG_GATEWAY_AVAILABILITY",
    online = "DEFAULT_ONLINE_METHOD",
    offline = "OFFLINE_GATEWAY_AVAILABILITY"
}

export const paymentMethodsInfo = {
    [PaymentMethods.online]: {
        title: 'Online payment gateway',
        changeable: false
    },
    [PaymentMethods.cpg]: {
        title: 'Lendo Credit Gateway',
        changeable: true
    },
    [PaymentMethods.offline]: {
        title: 'Pay on the spot',
        changeable: true
    }
}