import ProductOptionValue from '@entities/productOptionValue';

export default interface ProductOption {
  id: number;
  code: string;
  name: string;
  values: Partial<ProductOptionValue>[];
}

export type Guarantee = ProductOption["values"][0];

export type ShoeSize = ProductOption["values"][0];

export type DressSize = ProductOption["values"][0];

export interface AddProductOption extends Omit<ProductOption, 'id'> {}

export interface EditProductOption extends Omit<ProductOption, 'id'> {}
export interface attribute {
  hex: string
}
export interface Color  {
  id: number;
  code: string;
  value: string;
  attributes: Partial<attribute>;
}
