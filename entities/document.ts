import { ITransaction } from "@entities/transaction";

export interface IDocument {
    amount: number

    completedAt: string

    createdAt: string

    type: string

    updatedAt: string

    transactions: ITransaction[]
}


export interface IOrderDocument extends IDocument {}

export interface IRefundDocument extends IDocument {
    description: string
}
