import Category from "@entities/category";

export default interface CategoryCommission {
    id: number;
    fee: number;
    category: Partial<Category>;
}
