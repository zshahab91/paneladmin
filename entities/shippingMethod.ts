import ShippingCategory from '@entities/shippingCategory';
import ShippingPeriod from '@entities/shippingPeriod';

export default interface ShippingMethod {
  id: number;
  name: string;
  categories: Partial<ShippingCategory>[];
  periods: Partial<ShippingPeriod>[];
  shippingMethodPrices: {
    zone: { id: number; name: string; code: number };
    price: number;
  }[];
}

export interface AddShippingMethod
  extends Omit<ShippingMethod, 'id' | 'shippingMethodPrices'> {
  configuration: [];
  shippingMethodPrices: { zone: number; price: number }[];
}

export interface EditShippingMethod
  extends Omit<ShippingMethod, 'id' | 'shippingMethodPrices'> {
  configuration: [];
  shippingMethodPrices: { zone: number; price: number }[];
}
