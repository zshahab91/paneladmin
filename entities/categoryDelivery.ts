export default interface CategoryDelivery {
  category: number,
  start: string, // todo isoString date
  end: string // todo isoString date
}

export interface AddCategoryDeliveryProps extends Omit<CategoryDelivery, 'id' | 'code'> {


}

export interface EditCategoryDeliveryProps extends Omit<CategoryDelivery, 'id' | 'code'> {

}
