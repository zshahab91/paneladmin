import { IResponse } from './response';

export type Nullable<T> = T | undefined;

export interface IPaginatedResponse<Entity, Extra = {}>
  extends IResponse<Entity> {
  metas: Extra & {
    page: number;
    perPage: number;
    totalItems: number;
    totalPages: number;
  };
}
