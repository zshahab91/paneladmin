export interface Province {
  id: number;
  code: string;
  name: string;
}

export interface City {
  id: number;
  code: string;
  name: string;
  province?: Province;
}

export default interface Zone {
  id: number;
  code: string;
  name: string;
  provinces: Province[]
}
