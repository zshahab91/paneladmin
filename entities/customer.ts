export interface CustomerAddress {
    "id": number;
    "fullAddress": string;
    "postalCode": number;
    "isDefault": boolean;
    "coordinates": [];
    "number": number;
    "unit": number;
    "name": string;
    "family": string;
    "nationalCode": number;
    "phone": string;
    "city": {
        "id": number;
        "name": string
    };
    "district": {
        "id": number;
        "name": string
    }
}

export default interface Customer {
    id: number;
    username: string;
    name: string;
    family: string;
    email: string;
    status: string;
    mobile: string;
    nationalNumber: string;
    addresses?: CustomerAddress[]
}

export interface ICustomer extends Customer {
    //
}