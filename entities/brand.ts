export default interface Brand {
  id: number;
  title: string;
  code: string;
  subtitle: string;
  metaDescription: string;
  image?: { path: string; alt: string };
}

export interface AddBrandProps extends Omit<Brand, 'id'> {
  image: { path: string; alt: string };
}

export interface EditBrandProps extends Omit<Brand, 'id'> {
  image: { path: string; alt: string };
}
