import Inventory, { ICustomInventory } from "@entities/inventories";

export default interface OrderItem {
    id: number,
    subtotal: number,
    grandTotal: number,
    quantity: number,
    price: number,
    inventory?: Inventory,
    isEdit?:boolean
}

export interface ICustomOrderItem {
    id: number,
    subtotal: number,
    grandTotal: number,
    quantity: number,
    price: number,
    inventory?: ICustomInventory,
    isEdit?:boolean
}
