export default interface ShippingCategory {
    id: number,
    name: string
}

export interface AddShippingCategory extends Omit<ShippingCategory, 'id' | 'code'> {
}

export interface EditShippingCategory extends Omit<ShippingCategory, 'id' | 'code'> {
}
