export enum PaymentMethodsEnum {
    'OFFLINE' = 'OFFLINE',
    'ONLINE' = 'ONLINE',
    'CPG' = 'CPG',
}

export enum PaymentStatusEnum {
    'PENDING' = 'PENDING',
    'PAID' = 'PAID',
}