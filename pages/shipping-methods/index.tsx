import React, { useState } from 'react';
import { Table, Tag, Typography } from 'antd';
import AdminLayout from '@components/AdminLayout';
// import TableEditButton from '@components/CP/TableEditButton';
import { useShippingMethods } from '@components/hooks/shippingMethods';
import ShippingMethod from '@entities/shippingMethod';
// import DeleteButton from '@components/CP/DeleteButton';
// import ShippingMethodsService from '@services/ShippingService/ShippingMethods.api.service';
// import TableAddButton from '@components/CP/TableAddButton';
// import TableViewButton from '@components/CP/TableViewButton';
import BreadcrumbPage from '@components/Breadcrumb';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  // {
  //   title: 'Actions',
  //   dataIndex: 'id',
  //   key: 'id',
  //   render: (id: number) => (
  //     <>
  //       <TableEditButton href={`shipping-methods/${id}/edit`} />
  //       <TableViewButton href={`shipping-methods/${id}/details`} />
  //       <DeleteButton onDelete={() => ShippingMethodsService.delete(id)} />
  //     </>
  //   ),
  // },
];

const ShippingMethodsListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const { data, isLoading, totalItems, perPage } = useShippingMethods(
    {},
    { page: pageIndex },
  );

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Shipping methods list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Shipping Methods' }]}
      />

      <Typography.Title level={4}>Shipping Methods</Typography.Title>
      {/* <TableAddButton href="shipping-methods/create" /> */}
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

export default ShippingMethodsListPage;
