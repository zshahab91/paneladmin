import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/ShippingMethodPage/Edit';

const ShippingMethodsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a shipping method">
      <Edit />
    </AdminLayout>
  );
};

export default ShippingMethodsEditPage;
