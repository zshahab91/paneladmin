import React, { useEffect, useState } from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import { useRouter } from 'next/router';
import { Card, Col, Descriptions, Divider, Row, Tag } from 'antd';
import TableEditButton from '@components/CP/TableEditButton';
import s from '@components/OrderPage/Details/styles.module.scss';
import ShippingMethod from '@entities/shippingMethod';
import { request } from '@services/http/adapter';
import BreadcrumbPage from '@components/Breadcrumb';

function useShippingMethodDataById(id?: number) {
  const [data, setData] = useState<ShippingMethod | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = (): void => {
    if (!id) return;

    setIsLoading(true);
    request
      .get('/admin/shipping-methods/' + id)
      .then((res) => {
        if (!res) return;

        const {
          data: { results, succeed },
        } = res;
        if (succeed) {
          setData(results);
        }
      })
      .catch(console.error)
      .finally(() => setIsLoading(false));
  };

  useEffect(() => fetchData(), [id]);

  return { data, isLoading };
}

const renderShippingMethodPriceDetails = (
  shippingMethodPrice: ShippingMethod['shippingMethodPrices'][0],
): React.ReactNode => {
  return (
    <Descriptions column={1}>
      <Descriptions.Item label="Code Zone">
        {shippingMethodPrice.zone.code}
      </Descriptions.Item>
      <Descriptions.Item label="Name Zone">
        {shippingMethodPrice.zone.name}
      </Descriptions.Item>
      <Descriptions.Item label="Price">
        {shippingMethodPrice.price}
      </Descriptions.Item>
    </Descriptions>
  );
};

const ShippingMethodDetailPage: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [titlePage, setTitlePage] = useState('');

  const { data } = useShippingMethodDataById(id ? Number(id) : undefined);
  //@ts-ignore
  setTitlePage(data?.name);

  if (!data)
    // eslint-disable-next-line @rushstack/no-null
    return null;

  return (
    <AdminLayout title="Shipping method detail">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Methods', route: '/shipping-methods' },
          { name: titlePage },
        ]}
      />

      <Row gutter={20} className={s.row}>
        <Col xs={24} md={12} lg={10}>
          <Card
            title="Send timeout details"
            extra={<TableEditButton href={`shipping-methods/${id}`} />}
          >
            <Descriptions column={1}>
              <Descriptions.Item label="ID">{data?.id}</Descriptions.Item>
              <Descriptions.Item label="Starting interval">
                {data?.name}
              </Descriptions.Item>
            </Descriptions>

            <Divider />

            <Descriptions column={1}>
              <Descriptions.Item label="Shipping Groups">
                {data?.categories.map((cat) => (
                  <Tag key={cat.id} style={{ marginTop: 5 }}>
                    {cat.name}
                  </Tag>
                ))}
              </Descriptions.Item>
            </Descriptions>

            <Divider />

            <Descriptions column={1}>
              <Descriptions.Item label="Submission Periods">
                {data?.periods.map((per) => (
                  <Tag
                    key={per.id}
                    color={per.isActive ? 'cyan' : undefined}
                    style={{ marginTop: 5 }}
                    title={per.isActive ? 'Active' : 'InActive'}
                  >
                    {per.start} تا {per.end}
                  </Tag>
                ))}
              </Descriptions.Item>
            </Descriptions>
          </Card>
        </Col>
        <Col xs={24} md={12} lg={8}>
          <Card title="Prices">
            {data?.shippingMethodPrices.map((price, ix) => (
              <React.Fragment key={ix}>
                {ix !== 0 && <Divider />}
                {renderShippingMethodPriceDetails(price)}
              </React.Fragment>
            ))}
          </Card>
        </Col>
      </Row>
    </AdminLayout>
  );
};

export default ShippingMethodDetailPage;
