import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/ShippingMethodPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ShippingMethodAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a shipping method">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Methods', route: '/shipping-methods' },
          { name: 'Add New Shipping Method' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ShippingMethodAddPage;
