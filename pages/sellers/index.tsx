import React, { useState } from 'react';
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Space,
  Table,
  Tag,
  Typography,
} from 'antd';
import AdminLayout from '@components/AdminLayout';
import TableEditButton from '@components/CP/TableEditButton';
import { useSellers } from '@components/hooks/sellers';
import TableAddButton from '@components/CP/TableAddButton';
import BreadcrumbPage from '@components/Breadcrumb';
import TableChangePasswordButton from '@components/CP/TableChangePasswordButton';
import FilterFormLayout from '@components/CP/FilterFormLayout';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Identifier',
    dataIndex: 'identifier',
    key: 'identifier',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Username',
    dataIndex: 'username',
    key: 'username',
  },
  {
    title: 'National Number',
    dataIndex: 'nationalNumber',
    key: 'nationalNumber',
  },
  {
    title: 'National Identifier',
    dataIndex: 'nationalIdentifier',
    key: 'nationalIdentifier',
  },
  {
    title: 'Limited',
    dataIndex: 'isLimited',
    key: 'isLimited',
    render: (isLimited: boolean) => (
      <Tag color={isLimited ? 'cyan' : 'magenta'}>
        {isLimited ? 'Yes' : 'No'}
      </Tag>
    ),
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableEditButton href={`sellers/${id}/edit`} />
        <TableChangePasswordButton href={`sellers/${id}/password`} />
      </>
    ),
  },
];

const SellersListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
    {},
  );
  const { data, isLoading, totalItems, perPage } = useSellers(filterQuery, {
    page: pageIndex,
  });

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Sellers list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Sellers' }]}
      />
      <Typography.Title level={4}>Sellers</Typography.Title>
      <FilterForm
        onFilter={(values) => setFilterQuery(values)}
        isLoading={isLoading}
      />
      <TableAddButton href="/sellers/create" />
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="identifier" label="Identifier">
            <Input />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={8}>
          <Form.Item name="name" label="Name">
            <Input />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={8}>
          <Form.Item name="username" label="Username">
            <Input />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={12}>
          <Form.Item name="nationalNumber" label="National Number">
            <Input />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={12}>
          <Form.Item name="nationalIdentifier" label="National Identifier">
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};

export default SellersListPage;
