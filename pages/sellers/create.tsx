import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import { Button, Card, Checkbox, Form, Input, message } from 'antd';
import React from 'react';
import { Store } from 'rc-field-form/lib/interface';
import { IValidationError } from '@entities/validation.error';
import { ISeller } from '@entities/seller';
import { SellerApiService } from '@services/seller/seller.api.service';
import Link from 'next/link';
import BreadcrumbPage from '@components/Breadcrumb';
import { useRouter } from 'next/router';
import FormAlignment from '@components/CP/FormAlignment';

const NOTIFICATION_KEY = 'SELLER_CREATE';

const Page: NextPage = () => {
  const router = useRouter();
  const service = new SellerApiService();
  const [form] = Form.useForm();

  const onFinish = async (seller: ISeller): Promise<void> => {
    try {
      message.loading({ content: 'IsLoading...!', key: NOTIFICATION_KEY });
      await service.create(seller);
      message.success({
        key: NOTIFICATION_KEY,
        content: 'Done Successfully.',
      });
      form.resetFields();
      router.push('/sellers');
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }
  };

  return (
    <AdminLayout title="Add a seller">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Sellers', route: '/sellers' },
          { name: 'Add New Seller' },
        ]}
      />

      <Card
        title={'Add Seller'}
        extra={
          <Link href={`/sellers`}>
            <a>View All</a>
          </Link>
        }
      >
        <FormAlignment>
          <Form
            form={form}
            name={'seller'}
            initialValues={{ active: true, isLimited: false }}
            wrapperCol={{ span: 18 }}
            labelCol={{ span: 6 }}
            onFinish={(onFinish as unknown) as (values: Store) => void}
          >
            <Form.Item
              label={'Full Name'}
              labelAlign={'right'}
              name="name"
              rules={[
                { required: true, message: 'Please Add Full Name!', min: 3 },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label={'Username'}
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please add a valid Username!',
                  pattern: /^\S+@\S+\.\S+$/,
                },
              ]}
            >
              <Input placeholder="example@example.com" />
            </Form.Item>

            <Form.Item
              label={'National Number'}
              name="nationalNumber"
              rules={[
                {
                  required: false,
                  message: 'Please Add Valid National Number !',
                  pattern: /[0-9]{10}/,
                },
              ]}
            >
              <Input max={10} />
            </Form.Item>

            <Form.Item
              label={'National Identifier'}
              name="nationalIdentifier"
              rules={[
                {
                  required: false,
                  message: 'Please Add Valid National Identifier !',
                  pattern: /[0-9]{10}/,
                },
              ]}
            >
              <Input max={10} />
            </Form.Item>

            <Form.Item
              label={'Password'}
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please Add Password!',
                  min: 6,
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item name="isLimited" label="Limited" valuePropName="checked">
              <Checkbox />
            </Form.Item>

            <Form.Item wrapperCol={{ dir: 'ltr' }}>
              <Button type={'primary'} htmlType="submit">
                Save
              </Button>
            </Form.Item>
          </Form>
        </FormAlignment>
      </Card>
    </AdminLayout>
  );
};

export default Page;
