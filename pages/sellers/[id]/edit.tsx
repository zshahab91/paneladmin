import React, { useEffect, useState } from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import { Button, Card, Checkbox, Form, Input, message, Spin } from 'antd';
import { Store } from 'rc-field-form/lib/interface';
import { IValidationError } from '@entities/validation.error';
import { useRouter } from 'next/router';
import { SellerApiService } from '@services/seller/seller.api.service';
import Link from 'next/link';
import { ISeller } from '@entities/seller';
import BreadcrumbPage from '@components/Breadcrumb';
import FormAlignment from '@components/CP/FormAlignment';

const NOTIFICATION_KEY = 'SELLER_UPDATE';

const Page: NextPage = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const service = new SellerApiService();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [data, setData] = useState<Partial<ISeller>>();
  const [titlePage, setTitlePage] = useState('');
  useEffect(() => {
    (async () => {
      if (router.query.id === undefined) {
        return;
      }

      const {
        data: { results: seller },
      } = await service.show((router.query.id as unknown) as number);
      setTitlePage(seller.name);
      setData(seller);
      setIsLoading(false);
    })();
  }, [router.query.id]);

  const onFinish = async (seller: ISeller): Promise<void> => {
    try {
      message.loading({ content: 'IsLoading...!', key: NOTIFICATION_KEY });
      await service.update((router.query.id as unknown) as number, seller);
      message.success({
        key: NOTIFICATION_KEY,
        content: 'Done Successfully.',
      });
      router.push('/sellers');
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }
  };

  if (isLoading) {
    return (
      <AdminLayout title="Edit a seller">
        <Spin />
      </AdminLayout>
    );
  }

  return (
    <AdminLayout title="Edit a seller">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Sellers', route: '/sellers' },
          { name: titlePage },
        ]}
      />

      <Card
        title={'Update Seller'}
        extra={
          <Link href={`/sellers`}>
            <a>View All</a>
          </Link>
        }
      >
        <FormAlignment>
          <Form
            form={form}
            name={'seller'}
            initialValues={data}
            onFinish={(onFinish as unknown) as (values: Store) => void}
          >
            <Form.Item
              label={'Name'}
              labelAlign={'right'}
              name="name"
              rules={[{ required: true, message: 'Please Add Name!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label={'Username'}
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please Add Username!',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item label={'National Number'} name="nationalNumber">
              <Input />
            </Form.Item>

            <Form.Item label={'National Identifier'} name="nationalIdentifier">
              <Input />
            </Form.Item>

            <Form.Item name="isLimited" label="Limited" valuePropName="checked">
              <Checkbox />
            </Form.Item>

            <Form.Item wrapperCol={{ dir: 'ltr' }}>
              <Button type={'primary'} htmlType="submit">
                Update
              </Button>
            </Form.Item>
          </Form>
        </FormAlignment>
      </Card>
    </AdminLayout>
  );
};

export default Page;
