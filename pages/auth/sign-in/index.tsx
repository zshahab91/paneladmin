import * as React from 'react';
import {useState} from 'react';
import AuthLayout from '@components/AuthLayout';
import {Alert, Button, Card, Form, Input} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons/lib";
import AuthService from "@services/AuthService";
import {Store} from "rc-field-form/es/interface";


export default function SignInPage(): React.ReactNode {
    const [isRequesting, setIsRequesting] = useState(false);
    const [isRedirecting, setIsRedirecting] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");


    function handleSubmit(formData: Store): void {
        setIsRequesting(true);
        AuthService.signIn(formData.username, formData.password)
            .then(isSucceeded => {
                if (isSucceeded)
                    setIsRedirecting(true);
                setErrorMessage("");
            })
            .catch(e => {
                if (e.response?.data.code === 401) {
                    setErrorMessage("Username or password in invalid.");
                } else {
                    setErrorMessage("Server Error.");
                }
            })
            .finally(() => setIsRequesting(false));
    }


    return (
        <AuthLayout title="Sign in">

            {errorMessage && <Alert message={errorMessage} showIcon type="error" style={{marginBottom: 15}}/>}

            <Card title="Login" style={{width: 350}}>

                <Form onFinish={handleSubmit}>

                    <Form.Item
                        name="username"
                        rules={[{required: true, message: "Please enter your username!"}]}
                    >
                        <Input size="large" placeholder="username" prefix={<UserOutlined/>}/>
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[{required: true, message: "Please enter your password!"}]}
                    >
                        <Input size="large" placeholder="password" prefix={<LockOutlined/>} type="password"/>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" block size="large" loading={isRequesting}
                                disabled={isRedirecting}>
                            Login
                        </Button>
                    </Form.Item>
                </Form>

            </Card>
        </AuthLayout>
    );
}
