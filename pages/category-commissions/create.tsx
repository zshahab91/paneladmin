import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import AddCategoryCommission from '@components/CategoryCommissionsPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';


const CategoryCommissionAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a category commissions">
        <BreadcrumbPage
          listLinks={[
            { name: 'Home', route: '/' },
            { name: 'Category Commissions', route: '/category-commissions' },
            { name: 'Add New Category Commissions' },
          ]}
        />
          <AddCategoryCommission />
    </AdminLayout>
  );
};

export default CategoryCommissionAddPage;
