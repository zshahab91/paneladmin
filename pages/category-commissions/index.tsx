import React, { useState } from 'react';
import { Button, Col, Form, Row, Space, Table, Typography } from 'antd';
import AdminLayout from '@components/AdminLayout';
import TableEditButton from '@components/CP/TableEditButton';
import { useCategoryCommissions } from '@components/hooks/categorieCommissions';
import CategoryCommission from '@entities/categoryCommission';
import TableAddButton from '@components/CP/TableAddButton';
import BreadcrumbPage from '@components/Breadcrumb';
import CategorySelector from '@components/CP/CategorySelector';
import RangeInput from '@components/CP/RangeInput';
import FilterFormLayout from '@components/CP/FilterFormLayout';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Price',
        dataIndex: 'fee',
        key: 'fee',
    },
    {
        title: 'Category',
        dataIndex: 'category',
        key: 'category',
        render: (category: CategoryCommission['category']) => category.title,
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => (
            <>
                <TableEditButton href={`category-commissions/${id}/edit`}/>
            </>
        ),
    },
];

const CategoryCommissionListPage: React.FC = () => {
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
        {},
    );
    const {
        data,
        isLoading,
        totalItems,
        perPage,
    } = useCategoryCommissions({ ...filterQuery, categoryId: filterQuery.categoryId?.id }, { page: pageIndex });

    const pagination = totalItems! > perPage! && {
        total: totalItems,
        pageSize: perPage,
        current: pageIndex,
        onChange: setPageIndex,
    };

    return (
        <AdminLayout title="Category commissions list">
            <BreadcrumbPage
                listLinks={[
                    { name: 'Home', route: '/' },
                    { name: 'Category Commissions' },
                ]}
            />

            <Typography.Title level={4}>Category Commissions</Typography.Title>
            <FilterForm
                onFilter={(values) => setFilterQuery(values)}
                isLoading={isLoading}
            />
            <TableAddButton href="/category-commissions/create"/>
            <Table
                rowKey={(e) => e.id}
                dataSource={data}
                bordered
                loading={isLoading}
                columns={columns}
                pagination={pagination}
                style={{ width: '100%' }}
            />
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
            <Row gutter={24}>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="fee" label="fee">
                        <RangeInput/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={8}>
                    <Form.Item name="categoryId" label="Category" valuePropName="value">
                        <CategorySelector isLeaf={true}/>
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </FilterFormLayout>
    );
};

export default CategoryCommissionListPage;
