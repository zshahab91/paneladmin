import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/CategoryCommissionsPage/Edit';

const CategoryCommissionEditPage: NextPage = () => {
    return (
        <AdminLayout title="Edit a category commissions">
            <Edit/>
        </AdminLayout>
    );
};

export default CategoryCommissionEditPage;
