import React, { useState } from 'react';
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Space,
  Table,
  Tag,
  Typography,
} from 'antd';
import AdminLayout from '@components/AdminLayout';
import { ColumnType } from 'antd/lib/table/interface';
import { useProductVariants } from '@components/hooks/productVariants';
import ProductVariant from '@entities/productVariant';
import TableViewButton from '@components/CP/TableViewButton';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import { useRouter } from 'next/router';

const columns: ColumnType<ProductVariant>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Slug',
    dataIndex: 'code',
    key: 'code',
  },
  {
    title: 'Position',
    dataIndex: 'position',
    key: 'position',
  },
  {
    title: 'Option Values',
    dataIndex: 'optionValues',
    key: 'optionValues',
    render: (optionValues: ProductVariant['optionValues']) => (
      <>
        {optionValues.map((r) => (
          <Tag key={r.id}>{r.value}</Tag>
        ))}
      </>
    ),
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableViewButton href={`product-variants/${id}/details`} />
      </>
    ),
  },
];

const CustomerListPage: React.FC = () => {
  const {
    query: { product },
  } = useRouter();
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
    {},
  );
  const { data, isLoading, totalItems, perPage } = useProductVariants(
    { ...filterQuery, product: product as unknown as number },
    { page: pageIndex },
  );

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  function renderTable(): React.ReactNode {
    return (
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    );
  }

  return (
    <AdminLayout title="Product variants list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Product Variants' }]}
      />

      <Typography.Title level={4}>Product Variant</Typography.Title>
      <FilterForm
        onFilter={(values) => setFilterQuery(values)}
        isLoading={isLoading}
      />

      {renderTable()}
    </AdminLayout>
  );
};

interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="code" label="Code">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="position" label="Position">
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};

export default CustomerListPage;
