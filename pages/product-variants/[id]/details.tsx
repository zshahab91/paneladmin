import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import View from '@components/ProductVariantsPage/View';
import { Col, Row } from 'antd';

const ProductVariantViewPage: NextPage = () => {
  return (
    <AdminLayout title="Product variants detail">
      <Row>
        <Col xs={24} md={24} lg={24} xl={24}>
          <View />
        </Col>
      </Row>
    </AdminLayout>
  );
};

export default ProductVariantViewPage;
