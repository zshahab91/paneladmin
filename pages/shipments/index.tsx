import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';
import CitySelector from '@components/CP/CitySelector';
import CPDatePicker from '@components/CP/CPDatePicker/CPDatePicker';
import CustomerSelector from '@components/CP/CustomerSelector';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import InventorySelector from '@components/CP/InventorySelector';
import ProductSelector from '@components/CP/ProductSelector';
import ProvinceSelector from '@components/CP/ProvinceSelector';
import SellerSelector from '@components/CP/SellerSelector';
import TableViewButton from '@components/CP/TableViewButton';
import { QueryProps, useShipments } from '@components/hooks/shipments';
import {
    Button,
    Col,
    Form,
    Input,
    Row,
    Select,
    Space,
    Table,
    Typography,
} from 'antd';
import {
    Key,
    SorterResult,
    TableCurrentDataSource,
    TablePaginationConfig,
} from 'antd/es/table/interface';
import moment from 'moment-jalaali';
import React, { useState } from 'react';
// const renderContent = (value: any, row: any, index: number) => {
//     console.log("index",index)
//     console.log("value",value)
//     console.log("row",row)

//     const obj = {
//       children: value,
//       props: {},
//     };
//     return obj;
//   };
const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Order ID',
        dataIndex: ['order', 'identifier'],
        key: ['order', 'identifier'],
    },
    {
        title: 'Delivery date',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        render: (date: string) => moment(date).format('jYYYY-jMM-jDD'),
        sorter: { multiple: 1 },
    },
    {
        title: 'Period',
        dataIndex: 'period',
        key: 'period',
        render:(value: any, row: any, index: number) => {            
            const obj = {
              children: value ? `${value.start} - ${value.end}`: '-',
              props: {},
            };
            return obj;
          },
        sorter: { multiple: 1 },
    },
    {
        title: 'Customer name',
        dataIndex: ['order', 'orderAddress'],
        key: ['order', 'orderAddress'],
        //@ts-ignore
        render: (orderAddress) => {
            return `${orderAddress.name} ${orderAddress.family}`
        }
    },
    {
        title: 'Payment method',
        dataIndex: ['order', 'paymentMethod'],
        key: ['order', 'paymentMethod'],
    },
    {
        title: 'Grand total',
        dataIndex: 'grandTotal',
        key: 'grandTotal',
        render: (grandTotal: number) => grandTotal.toLocaleString(),
    },
    {
        title: 'Shipping cost',
        dataIndex: 'subTotal',
        key: 'subTotal',
        render: (subTotal: number) => subTotal.toLocaleString(),
    },
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: 'Items count',
        dataIndex: 'orderItemsCount',
        key: 'orderItemsCount',
    },
    {
        title: 'Shipping category',
        dataIndex: ['shippingCategory', 'name'],
        key: ['shippingCategory', 'name'],
    },
    {
        title: 'Province',
        dataIndex: ['order', 'orderAddress', 'city', 'province', 'name'],
        key: ['order', 'orderAddress', 'city', 'province', 'name'],
    },
    {
        title: 'City',
        dataIndex: ['order', 'orderAddress', 'city', 'name'],
        key: ['order', 'orderAddress', 'city', 'name'],
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => <TableViewButton href={`/shipments/${id}`}/>,
    },
];

const Index: React.FC = () => {
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<{ [name: string]: any }>({});
    const { data, isLoading, totalItems, perPage } = useShipments({
        ...filterQuery,
        province: filterQuery.province?.id,
        city: filterQuery.city?.id,
    }, {
        page: pageIndex,
    });

    const pagination = totalItems! > perPage! && {
        total: totalItems,
        pageSize: perPage,
        current: pageIndex,
        onChange: setPageIndex,
    };

    const onTableChange = (
        pagination: TablePaginationConfig,
        filters: Record<string, Key[] | null>,
        sorter: SorterResult<object> | SorterResult<object>[],
        extra: TableCurrentDataSource<object>,
    ) => {
        if (Array.isArray(sorter)) {
            const res = sorter.map(
                (s) => `${s.order === 'ascend' ? '' : '-'}${s.columnKey}`,
            );
            setFilterQuery((prevState) => ({ ...prevState, sort: res }));
            return;
        }

        const res = `${sorter.order === 'ascend' ? '' : '-'}${sorter.columnKey}`;
        setFilterQuery((prevState) => ({ ...prevState, sort: [res] }));
    };

    return (
        <AdminLayout title="Shipments">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Shipments' }]}
            />
            <Typography.Title level={4}>Shipments</Typography.Title>
            <FilterForm
                onFilter={(values) => setFilterQuery(values as QueryProps)}
                isLoading={isLoading}
            />
            <Table
                rowKey={'id'}
                dataSource={data}
                bordered
                loading={isLoading}
                // @ts-ignore
                columns={columns}
                pagination={pagination}
                style={{ width: '100%' }}
                onChange={onTableChange}
            />
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
            <Row gutter={24}>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="identifier" label="Order identifier">
                        <Input/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={8}>
                    <Form.Item name="shipment" label="Shipment id">
                        <Input/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={8}>
                    <Form.Item name="customer" label="Customer">
                        <CustomerSelector/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="number" label="National code">
                        <Input/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="seller" label="Seller">
                        <SellerSelector/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="product" label="Product">
                        <ProductSelector/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="inventory" label="TVI">
                        <InventorySelector/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="shippingCategory" label="Shipping category">
                        <Select placeholder={'Shipping category'}>
                            <Select.Option value={'NORMAL'}>Normal</Select.Option>
                            <Select.Option value={'HEAVY'}>Heavy</Select.Option>
                            <Select.Option value={'SUPER_HEAVY'}>Super heavy</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="paymentMethod" label="Payment method">
                        <Select>
                            <Select.Option value={'ONLINE'}>Online</Select.Option>
                            <Select.Option value={'OFFLINE'}>Offline</Select.Option>
                            <Select.Option value={'CPF'}>CPG</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="province" label="Province">
                        <ProvinceSelector/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="city" label="City">
                        <CitySelector/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="status" label="Status">
                        <Select>
                            <Select.Option value={'NEW'}>New</Select.Option>
                            <Select.Option value={'WAITING_FOR_SUPPLY'}>
                                Waiting For Supply
                            </Select.Option>
                            <Select.Option value={'PREPARING'}>
                                Preparing
                            </Select.Option>
                            <Select.Option value={'WAITING_FOR_SEND'}>
                                Waiting For Send
                            </Select.Option>
                            <Select.Option value={'SENT'}>Sent</Select.Option>
                            <Select.Option value={'DELIVERED'}>Delivered</Select.Option>
                            <Select.Option value={'CANCELED'}>Canceled</Select.Option>
                            <Select.Option value={'AFTER_SALES'}>After Sales</Select.Option>
                            <Select.Option value={'RETURNING'}>Returning</Select.Option>
                            <Select.Option value={'RETURNED'}>Returned</Select.Option>
                            <Select.Option value={'THIRD_PARTY_LOGISTICS'}>
                                Third Party Logistics
                            </Select.Option>
                        </Select>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="deliveryDateFrom" label="Delivery date from">
                        <CPDatePicker
                            onChange={(value) =>
                                form.setFieldsValue({ deliveryDateFrom: value })
                            }
                        />
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={12}>
                    <Form.Item name="deliveryDateTo" label="Delivery date to">
                        <CPDatePicker
                            onChange={(value) =>
                                form.setFieldsValue({ deliveryDateTo: value })
                            }
                        />
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </FilterFormLayout>
    );
};

export default Index;
