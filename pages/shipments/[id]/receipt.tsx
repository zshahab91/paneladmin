import { ICustomShipment } from "@entities/shipment";
import { ShipmentApiService } from "@services/shipment/shipment.api.service";
import { sumBy } from "lodash"
import moment from "moment-jalaali";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

moment.loadPersian();
const PaymentMethodMapper = (method?: string): string => {
    switch (method) {
        case "ONLINE":
            return "پرداخت آنلاین";
        case "OFFLINE":
            return "پرداخت در محل";
        case "CPG":
            return "پرداخت اعتباری";
        default:
            return "نامشخص";
    }
}

const Receipt: React.FC = () => {
    const router = useRouter();
    const [shipment, setShipment] = useState<ICustomShipment>();

    useEffect(() => {
        (async () => {
            if (!router.query.id) {
                return;
            }

            const { data: { results } } = await ShipmentApiService.getById(router.query.id as string);
            setShipment(results as ICustomShipment);
        })();
    }, [router.query.id])

    // @ts-ignore
    return (
        <>
            <Head>
                <meta charSet="utf-8"/>
                <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport"
                      content="width= device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>

                <title>Receipt</title>
                <style>
                    {`
        /** fonts **/
        @font-face {
            font-family: dana-regular;
            src: url('/static/fonts/dana-regular.eot');
            src: url('/static/fonts/dana-regular.eot?#iefix') format('embedded-opentype'),
            url('/static/fonts/dana-regular.woff2') format('woff2'),
            url('/static/fonts/dana-regular.woff') format('woff'),
            url('/static/fonts/dana-regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: dana-regular;
            src: url('/static/fonts/dana-fanum-regular.eot');
            src: url('/static/fonts/dana-fanum-regular.eot?#iefix') format('embedded-opentype'),
            url('/static/fonts/dana-fanum-regular.woff2') format('woff2'),
            url('/static/fonts/dana-fanum-regular.woff') format('woff'),
            url('/static/fonts/dana-fanum-regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: dana-bold;
            font-style: normal;
            font-weight: bold;
            src: url('/static/fonts/dana-bold.eot');
            src: url('/static/fonts/dana-bold.eot?#iefix') format('embedded-opentype'), /* IE6-8 */
            url('/static/fonts/dana-bold.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/
            url('/static/fonts/dana-bold.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
            url('/static/fonts/dana-bold.ttf') format('truetype');
        }

        @font-face {
            font-family: dana-bold;
            font-style: normal;
            font-weight: bold;
            src: url('/static/fonts/dana-fanum-bold.eot');
            src: url('/static/fonts/dana-fanum-bold.eot?#iefix') format('embedded-opentype'), /* IE6-8 */
            url('/static/fonts/dana-fanum-bold.woff2') format('woff2'), /* FF39+,Chrome36+, Opera24+*/
            url('/static/fonts/dana-fanum-bold.woff') format('woff'), /* FF3.6+, IE9, Chrome6+, Saf5.1+*/
            url('/static/fonts/dana-fanum-bold.ttf') format('truetype');
        }
        /** end fonts **/
        *, *::before, *::after {
            box-sizing: inherit!important;
        }

        body {
            direction: rtl!important;
            text-align: right!important;
            font-size: 12px!important;
            font-family: dana-regular!important;
            color: #141414!important;
            min-height: 1440px!important;
            position: relative!important;
        }

        .en {
            font-family: verdana!important;
        }
        
        .wrapper{
            width: 1000px!important;
            margin: 0 auto!important;
            display: flex;
            flex-direction: column;
            height: 100%;
        }
        
        .bordered {
            border-width: 1px;
            border-style: solid;
        }
        
        .not-bordered {
            border-width: 0px;
            border-color: #aaaaaa;
            border-style: solid;
        }

        .main_table .caption_main {
            font-size: 18px!important;
            font-family: dana-bold!important;
            padding-bottom: 16px!important;
            text-align: center;
            caption-side: top;
            color: inherit;
        }

        .main_table_top .caption_main {
            font-size: 18px!important;
            font-family: dana-bold!important;
            padding-bottom: 16px!important;
            text-align: center;
            caption-side: top;
            color: inherit;
        }

        .main_table .table_head td {
            padding: 8px 4px!important;
        }

        .d_flex {
            display: flex!important;
            align-items: flex-start!important;
            justify-content: flex-start!important;
            line-height: 24px!important;
        }

        .main_table .table_head td .caption, .main_table .table_body td .caption {
            padding-left: 16px!important;
            font-family: dana-bold!important;
            display: inline-block!important;
            min-width: 75px!important;
        }

        .main_table .table_body {
            border-collapse: collapse!important;
        }

        .main_table .table_body th, .main_table .table_body td {
            padding: 4px!important;
            text-align: center!important;
        }

        .main_table .table_body th {
            background: #f0f0f0!important;
            width: calc(100% / 6)!important;
        }

        .main_table .table_body td.pro {
            text-align: right!important;
            min-width: 400px!important;
        }

        .main_table .table_body td.pro .code {
            text-align: left!important;
        }

        .main_table .table_body tfoot td {
            text-align: right!important;
        }

        .main_table .tfoot_holder {
            padding-top: 32px!important;
        }

        .main_table .table_foot td {
            width: 50%!important;
            border: solid 1px #141414!important;
            padding: 4px!important;
        }

        .main_table .table_foot .tfoot_title {
            font-size: 14px!important;
            font-family: dana-bold!important;
            border: none!important;
            padding-bottom: 8px!important;
        }

        .caption_foot {
            font-family: 'dana-bold'!important;
            font-size: 18px!important;
            padding-top: 24px!important;
            caption-side: bottom!important;
            color: inherit!important;
            text-align: center!important;
        }

        .bottom_table {
            border: solid 1px #141414!important;
            padding-bottom: 16px!important;
            bottom: 8px!important;
            margin: auto!important;
        }

        .bottom_table .title_table_bottom {
            font-size: 18px!important;
            font-family: dana-bold!important;
            text-align: center!important;
        }

        .bottom_table td {
            padding: 8px 4px!important;
        }

        .bottom_table td .caption {
            padding-left: 16px!important;
            font-family: dana-bold!important;
            display: inline-block!important;
            min-width: 128px!important;
            text-align: left!important;
        }
        
        
        .main_table_top {
            border-collapse: collapse;
        }
        .main_table_top .table_body {
        }
        .main_table_top .table_body >tbody>tr>td{
            border: solid 1px #141414;
        }
        .main_table_top .table_body td {
            padding: 6px 4px;
        }
        .main_table_top .table_body td .caption {
            padding-left: 16px;
            font-family: dana-bold;
            display: inline-block;
        }
        .main_table_top .table_body .row_title {
            background: #f0f0f0;
            font-family: dana-bold;
            text-align: center;
            font-size: 14px;
        }
        .main_table_top .small_cell td .value {
            text-align: left;
            float: left;
        }
        .main_table_top .tfoot_holder {
            padding: 2px;
        }
        .main_table_top .table_foot {
            border-collapse: collapse;
            background: #f0f0f0;
        }
        .main_table_top .table_foot td{
            padding: 8px;
        }
        .main_table_top .table_foot td .caption{
            padding-left: 16px;
            font-family: dana-bold;
            display: inline-block;
        }

        .separator {
            height: 2px!important;
            border-bottom: dashed 2px #141414!important;
            margin: 32px 0!important;
        }
                `}
                </style>
            </Head>

            <div className="wrapper">

                <table className="main_table_top not-bordered" width="1000" cellPadding="0" cellSpacing="0">
                    <caption className="caption_main">برگ تحویل کالا</caption>
                    <tbody>
                    <tr>
                        <td>
                            <table className="table_body not-bordered" width="100%" cellSpacing={2} cellPadding="0">
                                <tbody>
                                <tr>
                                    <td className="row_title">
                                        گیرنده
                                    </td>
                                    <td>
                                        <table className="main_table_top_inner not-bordered" width="100%" cellSpacing="0"
                                               cellPadding="0">
                                            <tr>
                                                <td>
                                                    <span className="caption">خریدار:</span>
                                                    <span className="value">
                                                        {`${shipment?.order.customer?.name} ${shipment?.order.customer?.family}`}
                                                    </span>
                                                </td>
                                                <td>
                                                    <span className="caption">تحویل گیرنده:</span>
                                                    <span className="value">
                                                        {`${shipment?.order.orderAddress?.name} ${shipment?.order.orderAddress?.family}`}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="caption">شماره تماس خریدار:</span>
                                                    <span className="value">{shipment?.order?.customer?.mobile}</span>
                                                </td>
                                                <td>
                                                    <span className="caption">شماره تماس تحویل گیرنده:</span>
                                                    <span className="value">{shipment?.order?.orderAddress?.phone}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colSpan={3}>
                                                    <span className="caption"> آدرس تحویل گیرنده:</span>
                                                    <span className="value">
                                                        {shipment?.order?.orderAddress?.city?.province?.name + '، '}
                                                        {shipment?.order?.orderAddress?.fullAddress}
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table className="small_cell not-bordered" width="100%" cellPadding="0"
                                               cellSpacing="0">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <span className="caption"> تاریخ:</span>
                                                    <span className="value">
                                                        {moment(shipment?.order?.createdAt ?? undefined).format('jYYYY/jMM/jDD')}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="caption">شماره سفارش:</span>
                                                    <span className="value">{shipment?.order?.id}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="caption">شماره مرسوله:</span>
                                                    <span className="value">{shipment?.id}</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="row_title">
                                        فرستنده
                                    </td>
                                    <td>
                                        <table className="main_table_top_inner not-bordered" width="100%" cellSpacing="0"
                                               cellPadding="0">
                                            <tbody>
                                            <tr>
                                                <td colSpan={2}>
                                                    <span className="caption">فرستنده:</span>
                                                    <span className="value">شرکت نوین تجارت هوشمند آریا</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span className="caption">آدرس فرستنده:</span>
                                                    <span className="value"> تهران، شادآباد، خیابان شهید وثوقی، جنب کلانتری 151 شادآباد</span>
                                                </td>
                                                <td>
                                                    <span className="caption">کدپستی:</span>
                                                    <span className="value">1595-13185</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table className="small_cell not-bordered" width="100%" cellPadding="0"
                                               cellSpacing="0">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <span className="caption">شماره تماس :</span>
                                                    <span className="value">021-91070210</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td className="tfoot_holder" style={{ padding: '4px 0'}}>
                            <table className="table_foot bordered" width="100%" cellPadding="0" cellSpacing="0">
                                <tr>
                                    <td className="bordered">
                                        <span className="caption"> نوع پرداخت :</span>
                                        <span className="value">
                                            {PaymentMethodMapper(shipment?.order?.paymentMethod)}
                                        </span>
                                    </td>
                                    <td className="bordered">
                                        <span className="caption"> مبلغ قابل پرداخت :</span>
                                        <span className="value"><span>
                                            {((shipment?.grandTotal ?? 0) + sumBy(shipment?.orderItems, 'grandTotal')).toLocaleString()}
                                        </span> تومان </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tfoot>
                </table>

                <div className="separator" />

                <table className="main_table not-bordered" width="1000" cellPadding="0" cellSpacing="0">
                    <caption className="caption_main">برگ تحویل کالا</caption>
                    <thead>
                    <tr>
                        <td>
                            <table className="table_head  not-bordered" width="100%" cellPadding="0" cellSpacing="0">
                                <tbody>
                                <tr>
                                    <td>
                                        <span className="caption">شماره پیگیری:</span>
                                        <span className="value">{shipment?.order?.id}</span>
                                    </td>
                                    <td>
                                        <span className="caption">تاریخ:</span>
                                        <span className="value">
                                            {moment(shipment?.order?.createdAt ?? undefined).format('jYYYY/jMM/jDD')}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="caption">مرکز توزیع:</span>
                                        <span className="value">تیمچه</span>
                                    </td>
                                    <td>
                                        <span className="caption">شماره مرسوله:</span>
                                        <span className="value">{shipment?.id}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="caption">خریدار:</span>
                                        <span className="value">
                                            {`${shipment?.order.customer?.name} ${shipment?.order.customer?.family}`}
                                        </span>
                                    </td>
                                    <td>
                                        <span className="caption">تحویل گیرنده:</span>
                                        <span className="value">
                                            {`${shipment?.order.orderAddress?.name} ${shipment?.order.orderAddress?.family}`}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="caption">شماره تماس:</span>
                                        <span className="value">{shipment?.order.customer?.mobile}</span>
                                    </td>
                                    <td>
                                        <span className="caption">شماره تماس:</span>
                                        <span className="value">{shipment?.order.orderAddress?.phone}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan={2}>
                                        <div className="d_flex">
                                            <span className="caption">تحویل آدرس: </span>
                                            <span className="value">
                                                {shipment?.order?.orderAddress?.city?.province?.name + '، '}
                                                {shipment?.order?.orderAddress?.fullAddress}
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <table className="table_body bordered" width="100%" cellPadding="0" cellSpacing="0">
                                <thead>
                                <tr>
                                    <th className="bordered">ردیف</th>
                                    <th className="bordered">کد تنوع</th>
                                    <th className="bordered">شرح کالا</th>
                                    <th className="bordered">فروشنده</th>
                                    <th className="bordered">تعداد</th>
                                    <th className="bordered">مبلغ واحد</th>
                                </tr>
                                </thead>
                                <tbody>

                                {shipment?.orderItems?.map((item, i) => (
                                    <tr key={i} className="bordered">
                                        <td className="bordered">{i + 1}</td>
                                        <td className="bordered">TVI-{item.inventory?.id}</td>
                                        <td className="pro bordered">
                                            <div>
                                                {item.inventory?.variant?.product?.title}
                                                {' | '}
                                                {item.inventory?.variant?.optionValues['color'].value}
                                                {' | '}
                                                {item.inventory?.variant?.optionValues['guarantee'].value}
                                            </div>
                                        </td>
                                        <td className="bordered">{item.inventory?.seller?.name}</td>
                                        <td className="bordered">{item.quantity}</td>
                                        <td className="bordered">{item.price.toLocaleString()}</td>
                                    </tr>
                                ))}
                                <tr className="bordered">
                                    <td className="bordered">{(shipment?.orderItems?.length ?? 0) + 1}</td>
                                    <td className="bordered">-</td>
                                    <td className="pro boredered">
                                        <div>خدمات بسته&zwnj;بندی، بیمه حمل و ارسال</div>
                                    </td>
                                    <td className="bordered">-</td>
                                    <td className="bordered">-</td>
                                    <td className="bordered">{shipment?.grandTotal?.toLocaleString()}</td>
                                </tr>
                                <tr className="total_price bordered">
                                    <td colSpan={3} className="bordered">
                                        <span className="caption">تعداد کل آیتم ها:</span>
                                        <span className="value">{sumBy(shipment?.orderItems, 'quantity') + 1}</span>
                                    </td>
                                    <td colSpan={3} className="bordered">
                                        <span className="caption">جمع مبلغ کل (ریال): </span>
                                        <span className="value">
                                            {((shipment?.grandTotal ?? 0) + sumBy(shipment?.orderItems, 'grandTotal')).toLocaleString()}
                                        </span>
                                    </td>
                                </tr>
                                <tr className="bordered">
                                    <td colSpan={6}>
                                        <table className="not-bordered" width="100%" cellPadding="0" cellSpacing="0">
                                            <tbody>
                                            <tr />
                                            </tbody>
                                            <tbody>
                                            <tr>
                                                <td colSpan={2}>
                                                    <span className="caption">نوع پرداخت:</span>
                                                    <span
                                                        className="value">{PaymentMethodMapper(shipment?.order?.paymentMethod)}</span>
                                                </td>
                                                <td colSpan={2}>
                                                    <span className="caption">قابل پرداخت:</span>
                                                    <span className="value">
                                                        {((shipment?.grandTotal ?? 0) + sumBy(shipment?.orderItems, 'grandTotal')).toLocaleString()}
                                                    </span>
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot className="bordered">
                                <tr>
                                    <td colSpan={6}>
                                        <span className="caption"> منطقه و زمان تحویل:</span>
                                        <span className="value">
                                            {moment(shipment?.deliveryDate ?? undefined).format('dddd jYYYY/jMM/jDD')}
                                            {' ، ساعت '}
                                            {shipment?.period?.start + '-' + shipment?.period?.end}
                                        </span>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default Receipt;
