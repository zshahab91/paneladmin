import { PlusOutlined } from "@ant-design/icons";
import { PrinterOutlined } from '@ant-design/icons/lib';
import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';
import CPDatePicker from "@components/CP/CPDatePicker/CPDatePicker";
import AddressesModal from '@components/OrderPage/Details/AddressesModal';
import OrderShipments from '@components/OrderPage/Details/OrderShipments';
import s from '@components/OrderPage/Details/styles.module.scss';
import { IBalance } from "@entities/balance";
import Customer from '@entities/customer';
import Order, { OrderShipment, OrderStatusEnum } from '@entities/order';
import { IShipmentLog } from '@entities/shipment';
import { ITransaction } from "@entities/transaction";
import CustomersApiService from '@services/CustomerService/Customers.api.service';
import OrdersApiService from '@services/OrderService/Orders.api.service';
import { ShipmentApiService } from '@services/shipment/shipment.api.service';
import Link from "next/link";
import {
    Button,
    Card,
    Col,
    Descriptions, Form,
    Input,
    message,
    Modal,
    Row,
    Table,
    Typography,
} from 'antd';
import moment from 'jalali-moment';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';

const Shipment: React.FC = () => {
    const router = useRouter();
    const { id } = router.query;
    const [data, setData] = useState<Order>();
    const [logs, setLogs] = useState<IShipmentLog[]>([]);
    const [shipments, setShipments] = useState<OrderShipment[]>();
    const [customer, setCustomer] = useState<Customer>();
    const [addressModalVisible, setAddressModalVisibility] = useState(false);
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewTitle, setPreviewTitle] = useState('');
    const [itemEditPrice, setItemEditPrice] = useState('');
    const [itemEditPriceID, setItemEditPriceID] = useState(0);


    const [refund] = Form.useForm();
    const [balance, setBalance] = useState<IBalance>();
    const [isRefundModalVisible, setRefundModalVisibility] = useState<boolean>(false);
    const [isRefundSubmissionLoading, setRefundSubmissionLoadingStatus] = useState<boolean>(false);

    const onAddressUpdate = useCallback(async () => {
        const [orders, shipments] = await Promise.all([
            await OrdersApiService.getById(id as string),
            await OrdersApiService.getShipments(id as string),
        ]);
        setData(orders.data.results);
        setShipments(shipments.data.results);
    }, [id]);

    const onShipmentUpdate = useCallback(async () => {
        const {
            data: {
                results: { orderShipmentStatusLogs },
            },
        } = await ShipmentApiService.getById(id as string);
        const [shipments] = await Promise.all([
            await OrdersApiService.getShipments(id as string),
        ]);
        setShipments(shipments.data.results);
        setLogs(orderShipmentStatusLogs);
    }, [id]);

    const onRefundSubmitted = async () => {
        const data = refund.getFieldsValue();
        setRefundSubmissionLoadingStatus(true);
        await OrdersApiService.refund(id as string, { ...data, paid_at: `${data.paid_at} 00:00:00` });
        const { data: { results: b } } = await OrdersApiService.getBalance(id as string);
        const { data: { results: o } } = await OrdersApiService.getById(id as string)
        setBalance(b);
        setData(o);
        setRefundSubmissionLoadingStatus(false);
        setRefundModalVisibility(false);
    };

    const onRefundModalCanceled = async () => {
        refund.resetFields();
        setRefundSubmissionLoadingStatus(false);
        setRefundModalVisibility(false);
    }

    useEffect(() => {
        (async () => {
            if (id) {
                try {
                    const {
                        data: {
                            results: { order, orderShipmentStatusLogs },
                        },
                    } = await ShipmentApiService.getById(id as string);
                    const [orders, shipments, balance] = await Promise.all([
                        await OrdersApiService.getById((order.id as unknown) as string),
                        await OrdersApiService.getShipments((order.id as unknown) as string),
                        await OrdersApiService.getBalance(order.id as unknown as string),
                    ]);
                    setData(orders.data.results);
                    setShipments(shipments.data.results);
                    setBalance(balance.data.results);
                    setLogs(orderShipmentStatusLogs);
                    const customer = await CustomersApiService.getById(
                        orders.data.results.customer.id as number,
                    );
                    setCustomer(customer.data.results);
                } catch {
                    message.error('Error receiving information');
                }
            }
        })();
    }, [id]);

    const showModalEditPrice = useCallback((title, item) => {
        setPreviewTitle(title);
        setItemEditPrice(item.price);
        setItemEditPriceID(item.id);
        setPreviewVisible(true);
    }, []);

    const onSavePriceItem = () => {
        const dto = {
            items: [
                {
                    id: itemEditPriceID,
                    price: itemEditPrice,
                },
            ],
        };
        try {
            //@ts-ignore
            OrdersApiService.editPriceItem(dto, id as string);
            message.success('Edited successfully.');
            setPreviewVisible(false);
            onShipmentUpdate();
        } catch (e) {
            message.error(
                `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
            );
            setPreviewVisible(false);
        }
    };

    if (!data) {
        return <></>;
    }

    return (
        <AdminLayout title="Shipment">
            <Row gutter={20} className={s.row}>
                <BreadcrumbPage
                    listLinks={[
                        { name: 'Home', route: '/' },
                        { name: 'Shipments', route: '/shipments' },
                        { name: `${id}` },
                    ]}
                />

                <Col xs={24} md={12} lg={8}>
                    <Card
                        title="Detail Order"
                        extra={
                            <Link as={`/shipments/${id}/receipt`} href="/shipments/[id]/receipt">
                                <a target="_blank">
                                    <PrinterOutlined style={{ fontSize: 20 }}/>
                                </a>
                            </Link>
                        }
                    >
                        <Descriptions column={1}>
                            <Descriptions.Item label="Customer Name">
                                {data.customer.name} {data.customer.family}{' '}
                                {`(${data.customer.id})`}
                            </Descriptions.Item>
                            <Descriptions.Item label="Order Date">
                                {moment(data.createdAt).format('jYYYY-jMM-jDD')}
                            </Descriptions.Item>
                            <Descriptions.Item label="Order ID">
                                {data?.identifier}
                            </Descriptions.Item>
                            <Descriptions.Item label="Payment Method">
                                {data?.paymentMethod}
                            </Descriptions.Item>
                            <Descriptions.Item label="Post Status" className={s.status}>
                                {data?.status && OrderStatusEnum[data!.status]}
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>
                </Col>
                <Col xs={24} md={12} lg={16}>
                    <Card title="Shipping" className={s.shippingCard}>
                        <Descriptions
                            title="Recipient Address"
                            layout="vertical"
                            column={6}
                        >
                            <Descriptions.Item label="Recipient Name" span={2}>
                                {data?.orderAddress?.name} {data?.orderAddress?.family}
                            </Descriptions.Item>
                            <Descriptions.Item label="National Code">
                                {data?.orderAddress?.nationalCode}
                            </Descriptions.Item>
                            <Descriptions.Item label="Phone number">
                                {data?.orderAddress?.phone}
                            </Descriptions.Item>
                            <Descriptions.Item label="State">
                                {data?.orderAddress?.city?.name}
                            </Descriptions.Item>
                            <Descriptions.Item label="City">
                                {data?.orderAddress?.district?.name}
                            </Descriptions.Item>
                            <Descriptions.Item label="Address" span={3}>
                                {data?.orderAddress?.fullAddress}
                            </Descriptions.Item>
                            <Descriptions.Item label="Plaque">
                                {data?.orderAddress?.number}
                            </Descriptions.Item>
                            <Descriptions.Item label="Unit">
                                {data?.orderAddress?.unit}
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>
                </Col>
            </Row>
            <Row gutter={15}>
                <Col xs={24} md={12} lg={8}>
                    <Card title="Summary">
                        <Descriptions column={1}>
                            <Descriptions.Item label="Total">
                                {data?.subtotal.toLocaleString()} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Payable">
                                {data?.grandTotal.toLocaleString()} Toman
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <br/>

                    <Card title="Balance" extra={balance?.balanceStatus === "CREDITOR" && (
                        <PlusOutlined
                            onClick={() => setRefundModalVisibility(true)}
                            style={{ fontSize: 20 }}
                        />
                    )}>
                        <Descriptions column={1}>
                            <Descriptions.Item label="Status">
                                {balance?.balanceStatus}
                            </Descriptions.Item>
                            <Descriptions.Item label="Remaining">
                                {balance?.balanceStatus === "DEBTOR" ? balance?.balanceAmount.toLocaleString() : 0} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Total payable">
                                {balance?.balanceStatus === "DEBTOR" ? balance?.balanceAmount.toLocaleString() : 0} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Balance">
                                {balance?.balanceStatus === "CREDITOR" || balance?.balanceStatus === "BALANCE" ? "" : "-"}
                                {balance?.balanceAmount.toLocaleString()} Toman
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <br/>

                    <Table
                        title={() => (
                            <Typography.Text
                                style={{ paddingLeft: 8, fontWeight: 500, fontSize: 16 }}
                            >
                                Activity (Status)
                            </Typography.Text>
                        )}
                        dataSource={logs}
                        pagination={false}
                        columns={[
                            {
                                title: 'User',
                                dataIndex: [],
                                key: 'id',
                                render: (log: Order['statusLogs'][0]) =>
                                    `${log.user?.name ?? 'System'} ${
                                        log.user?.family ?? 'Administrator'
                                    }`,
                            },
                            {
                                title: 'From',
                                dataIndex: 'statusFrom',
                                key: 'statusFrom',
                            },
                            {
                                title: 'To',
                                dataIndex: 'statusTo',
                                key: 'statusTo',
                            },
                            {
                                title: 'Date',
                                dataIndex: 'createdAt',
                                key: 'createdAt',
                                render: (date: string) =>
                                    moment(date).format('jYYYY-jMM-jDD HH:mm:ss'),
                            },
                        ]}
                    />
                </Col>
                <Col xs={24} lg={6} xl={16}>
                    <Card title="Shipping Details">
                        <OrderShipments
                            order={data.identifier}
                            onShipmentUpdate={onShipmentUpdate}
                            data={
                                shipments?.length
                                    ? shipments.filter((os) => Number(os.id) === Number(id))
                                    : undefined
                            }
                            showModalEditPrice={showModalEditPrice}
                            isStatusOnly
                        />
                    </Card>

                    <br/>

                    <Table
                        tableLayout="fixed"
                        title={() => (
                            <Typography.Text
                                style={{ paddingLeft: 8, fontWeight: 500, fontSize: 16 }}
                            >Transactions</Typography.Text>
                        )}
                        dataSource={[
                            ...data.orderDocument.transactions.map(t => ({ ...t, dtype: 'Order' })),
                            ...data.refundDocuments.map(rd => rd.transactions.map(t => ({ ...t, dtype: 'Refund' }))),
                        ]}
                        pagination={false}
                        columns={[
                            {
                                title: 'Date',
                                dataIndex: 'createdAt',
                                key: 'createdAt',
                                render: (date: string) => moment(date).format('jYYYY-jMM-jDD HH:mm:ss')
                            },
                            {
                                title: 'Gateway',
                                dataIndex: 'gateway',
                                key: 'gateway',
                            },
                            {
                                title: 'Status',
                                dataIndex: 'status',
                                key: 'status',
                            },
                            {
                                title: 'Amount',
                                dataIndex: [],
                                key: 'amount',
                                render: (data: ITransaction & { dtype: string }) => <span style={{
                                    color: data.dtype === 'Order' ? 'green' : 'red'
                                }}>{data.amount.toLocaleString()}</span>
                            },
                            {
                                title: 'Type',
                                dataIndex: 'dtype',
                                key: 'dtype',
                            },
                            {
                                title: 'Reference',
                                dataIndex: 'trackingNumber',
                                key: 'trackingNumber'
                            },
                        ]}
                    />
                </Col>
            </Row>
            {customer && (
                <AddressesModal
                    activeId={data?.orderAddress?.id}
                    data={customer}
                    visible={addressModalVisible}
                    setVisible={setAddressModalVisibility}
                    onOk={onAddressUpdate}
                    orderId={data?.id}
                />
            )}
            <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={[
                    <Button key="back" onClick={() => setPreviewVisible(false)}>
                        Cancle
                    </Button>,
                    <Button key="submit" danger onClick={onSavePriceItem}>
                        Edit
                    </Button>,
                ]}
                onCancel={() => setPreviewVisible(false)}
            >
                <p>New Price:</p>
                <Input
                    value={itemEditPrice}
                    onChange={(e) => setItemEditPrice(e.target.value)}
                />
            </Modal>

            <Modal
                title="Refund"
                visible={isRefundModalVisible}
                onOk={onRefundSubmitted}
                confirmLoading={isRefundSubmissionLoading}
                onCancel={onRefundModalCanceled}
            >
                <Form form={refund}>
                    <Form.Item name="tracking_number" label="Tracking number">
                        <Input/>
                    </Form.Item>
                    <Form.Item name="paid_at" label="Paid at">
                        <CPDatePicker/>
                    </Form.Item>
                    <Form.Item name="description" label="Description">
                        <Input.TextArea/>
                    </Form.Item>
                </Form>
            </Modal>
        </AdminLayout>
    );
};

export default Shipment;
