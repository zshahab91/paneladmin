import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/ShippingCategoryPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ShippingCategoryAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a shipping category">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Categories', route: '/shipping-categories' },
          { name: 'Add New Shipping Category' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ShippingCategoryAddPage;
