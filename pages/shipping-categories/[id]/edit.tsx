import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/ShippingCategoryPage/Edit';

const ShippingCategoryEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a shipping category">
      <Edit />
    </AdminLayout>
  );
};

export default ShippingCategoryEditPage;
