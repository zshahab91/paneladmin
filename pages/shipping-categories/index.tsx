import React, {useState} from 'react';
import {Table, Typography} from 'antd';
import AdminLayout from '@components/AdminLayout';
import {useShippingCategories} from '@components/hooks/shippingCategories';
import BreadcrumbPage from '@components/Breadcrumb';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  }
];

const ShippingCategoriesListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const { data, isLoading, totalItems, perPage } = useShippingCategories(
    {},
    { page: pageIndex },
  );

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Shipping categories list">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Categories' },
        ]}
      />

      <Typography.Title level={4}>
        Shipping Categories
        </Typography.Title>
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

export default ShippingCategoriesListPage;
