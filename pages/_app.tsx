import React from 'react';
import type { AppProps } from 'next/app';
import { ConfigProvider } from 'antd';
import * as Sentry from '@sentry/node';
import 'react-modern-calendar-datepicker/lib/DatePicker.css';
import 'antd/dist/antd.css';
// import 'public/static/css/farsi-font.css';
import 'public/static/css/iransans.css';
import 'public/static/css/public.css';
// import 'public/static/css/antd-rtl-431.css';
import useRouteGuard from './guard';
import { LoadingOutlined } from '@ant-design/icons';

Sentry.init({
  dsn: 'https://953d62119f694d1a99b558e4562c93b3@sentry.lendo.ir/8',
  enabled: true,
  environment: 'production',
});

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  const isPageAllowed = useRouteGuard();

  if (!isPageAllowed) return <LoadingOutlined />;

  return (
    <ConfigProvider direction="ltr">
      <Component {...pageProps} />
    </ConfigProvider>
  );
};

export default MyApp;
