/**
 * Custom error handling {https://nextjs.org/docs#custom-error-handling}
 * The pages/_error.js component is only used in production.
 * In development you get an error with call stack to know where the error originated from
 */
import React from 'react';
import Error, { ErrorProps } from 'next/error';
import * as Sentry from '@sentry/node';
import { Button } from 'antd';
import Link from 'next/link';
import { NextPage } from 'next';

// todo Must implement sentry to log the errors
const MyError: NextPage<ErrorProps> = ({
  statusCode, // @ts-ignore
  hasGetInitialPropsRun, // @ts-ignore
  err,
}) => {
  if (!hasGetInitialPropsRun && err) {
    // getInitialProps is not called in case of
    // https://github.com/zeit/next.js/issues/8592. As a workaround, we pass
    // err via _app.js so it can be captured
    Sentry.captureException(err);
  }

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <span>
        <span>{statusCode}</span>
        <p>خطایی رخ داده است</p>
        <Link href="/">
          <Button>بازگشت</Button>
        </Link>
      </span>
    </div>
  );
};

MyError.getInitialProps = async ({ res, err, asPath }) => {
  // @ts-ignore
  const errorInitialProps = await Error.getInitialProps({ res, err });

  // Workaround for https://github.com/zeit/next.js/issues/8592, mark when
  // getInitialProps has run
  // @ts-ignore
  errorInitialProps.hasGetInitialPropsRun = true;

  if (res) {
    // Running on the server, the response object is available.
    //
    // Next.js will pass an err on the server if a page's `getInitialProps`
    // threw or returned a Promise that rejected

    if (res.statusCode === 404) {
      // Opinionated: do not record an exception in Sentry for 404
      return { statusCode: 404 };
    }

    if (err) {
      Sentry.captureException(err);

      return errorInitialProps;
    }
  } else if (err) {
    // Running on the client (browser).
    //
    // Next.js will provide an err if:
    //
    //  - a page's `getInitialProps` threw or returned a Promise that rejected
    //  - an exception was thrown somewhere in the React lifecycle (render,
    //    componentDidMount, etc) that was caught by Next.js's React Error
    //    Boundary. Read more about what types of exceptions are caught by Error
    //    Boundaries: https://reactjs.org/docs/error-boundaries.html
    Sentry.captureException(err);
    return errorInitialProps;
  }

  // If this point is reached, getInitialProps was called without any
  // information about what the error might be. This is unexpected and may
  // indicate a bug introduced in Next.js, so record it in Sentry
  Sentry.captureException(
    // @ts-ignore
    new Error(`_error.js getInitialProps missing data at path: ${asPath}`),
  );

  return errorInitialProps;
};

export default MyError;
