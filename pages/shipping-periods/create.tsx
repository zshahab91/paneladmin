import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/ShippingPeriodsPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ShippingPeriodAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a shipping period">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Periods', route: 'shipping-periods' },
          { name: 'Add New Shipping Period' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ShippingPeriodAddPage;
