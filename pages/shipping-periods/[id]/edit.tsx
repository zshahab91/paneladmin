import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/ShippingPeriodsPage/Edit';

const ShippingPeriodEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a shipping period">
      <Edit />
    </AdminLayout>
  );
};

export default ShippingPeriodEditPage;
