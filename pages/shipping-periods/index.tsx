import React, { useState } from 'react';
import { Table, Tag, Typography } from 'antd';
import AdminLayout from '@components/AdminLayout';
import { useShippingPeriods } from '@components/hooks/shippingPeriods';
// import TableEditButton from '@components/CP/TableEditButton';
// import TableAddButton from '@components/CP/TableAddButton';
import BreadcrumbPage from '@components/Breadcrumb';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Start',
    dataIndex: 'start',
    key: 'start',
  },
  {
    title: 'End',
    dataIndex: 'end',
    key: 'end',
  },
  {
    title: 'Traceable',
    dataIndex: 'isActive',
    key: 'isActive',
    render: (isActive: boolean) => (
      <Tag color={isActive ? 'cyan' : 'magenta'}>{isActive ? 'Yes' : 'No'}</Tag>
    ),
  },
  // {
  //   title: 'Actions',
  //   dataIndex: 'id',
  //   key: 'id',
  //   render: (id: number) => (
  //     <>
  //       <TableEditButton href={`shipping-periods/${id}/edit`} />
  //     </>
  //   ),
  // },
];

const ShippingPeriodsListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const { data, isLoading, totalItems, perPage } = useShippingPeriods(
    {},
    { page: pageIndex },
  );

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Shipping periods list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Shipping Periods' }]}
      />

      <Typography.Title level={4}>Shipping Periods</Typography.Title>
      {/* <TableAddButton href="/shipping-periods/create" /> */}
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

export default ShippingPeriodsListPage;
