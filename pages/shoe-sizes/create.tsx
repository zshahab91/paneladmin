import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/ShoeSizesPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ProductOptionsAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a shoe size">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shoe size', route: '/shoe-sizes' },
          { name: 'Add New Shoe size' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ProductOptionsAddPage;
