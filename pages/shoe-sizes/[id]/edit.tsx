import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/ShoeSizesPage/Edit';

const ProductOptionsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a shoe size">
      <Edit />
    </AdminLayout>
  );
};

export default ProductOptionsEditPage;
