import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/GuaranteesPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ProductOptionsAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a guarantee">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Guarantee', route: '/guarantees' },
          { name: 'Add New Guarantee' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ProductOptionsAddPage;
