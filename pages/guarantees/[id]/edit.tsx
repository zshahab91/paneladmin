import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/GuaranteesPage/Edit';

const ProductOptionsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a guarantee">
      <Edit />
    </AdminLayout>
  );
};

export default ProductOptionsEditPage;
