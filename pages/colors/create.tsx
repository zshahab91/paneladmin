import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/ColorsPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ProductOptionsAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a guarantee">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Colors', route: '/colors' },
          { name: 'Add New Color' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ProductOptionsAddPage;
