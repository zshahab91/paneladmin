import React, { useState } from 'react';
import { Button, Col, Form, Input, Row, Space, Table } from 'antd';
import AdminLayout from '@components/AdminLayout';
import { ColorsQueryProps, GuaranteesQueryProps, useColors, useGuarantees } from '@components/hooks/productOptions';
import TableAddButton from '@components/CP/TableAddButton';
import TableEditButton from '@components/CP/TableEditButton';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import { Color } from '@entities/productOption';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Slug',
        dataIndex: 'code',
        key: 'code',
    },
    {
        title: 'Value',
        key: 'value',
        render: (data: Color) => {
            return (
                <Space align={'center'}>
                    <div
                        style={{
                            backgroundColor: data.attributes.hex,
                            width: 15,
                            height: 15,
                            borderRadius: '50%',
                        }}
                    />
                    <div>{data.value} </div>
                </Space>
            );
        },
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => <TableEditButton href={`colors/${id}/edit`}/>,
    },
];

const CustomerListPage: React.FC = () => {
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<ColorsQueryProps>({});
    const { data, isLoading, totalItems, perPage } = useColors(filterQuery, { page: pageIndex });

    function renderTable(): React.ReactNode {
        return (
            <Table
                dataSource={data}
                bordered
                loading={isLoading}
                columns={columns}
                style={{ width: '100%' }}
                pagination={{
                    pageSize: perPage,
                    total: totalItems,
                    current: pageIndex,
                }}
                //@ts-ignore
                onChange={(e) => setPageIndex(e.current)}
            />
        );
    }

    return (
        <AdminLayout title="Colors list">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Colors' }]}
            />
            <FilterForm
                onFilter={(values: ColorsQueryProps) => setFilterQuery(values)}
                isLoading={isLoading}
            />
            <TableAddButton href="/colors/create"/>
            <br/>
            {renderTable()}
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
            <Row gutter={24}>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="code" label="Code">
                        <Input/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="value" label="Value">
                        <Input/>
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </FilterFormLayout>
    );
};

export default CustomerListPage;
