import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/ColorsPage/Edit';

const ProductOptionsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a Color">
      <Edit />
    </AdminLayout>
  );
};

export default ProductOptionsEditPage;
