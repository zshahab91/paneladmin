import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/CategoryLeadsPage/Edit';

const CategoryLeadEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a Category lead">
      <Edit />
    </AdminLayout>
  );
};

export default CategoryLeadEditPage;
