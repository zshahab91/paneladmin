import CategorySelector from "@components/CP/CategorySelector";
import React, { useState } from 'react';
import { Button, Form, Input, Space, Table, Typography } from 'antd';
import AdminLayout from '@components/AdminLayout';
import TableEditButton from '@components/CP/TableEditButton';
import { useCategoryLeads } from '@components/hooks/categorieLeads';
import CategoryLead from '@entities/categoryLead';
import TableAddButton from '@components/CP/TableAddButton';
import BreadcrumbPage from '@components/Breadcrumb';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Value',
        dataIndex: 'value',
        key: 'value',
    },
    {
        title: 'Category',
        dataIndex: 'category',
        key: 'category',
        render: (category: CategoryLead['category']) => category.title,
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => (
            <>
                <TableEditButton href={`category-leads/${id}/edit`}/>
            </>
        ),
    },
];

const CategoryLeadListPage: React.FC = () => {
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
        {},
    );
    const { data, isLoading, totalItems, perPage } = useCategoryLeads(
        { ...filterQuery, category: filterQuery.category?.id },
        { page: pageIndex },
    );

    const pagination = totalItems! > perPage! && {
        total: totalItems,
        pageSize: perPage,
        current: pageIndex,
        onChange: setPageIndex,
    };

    return (
        <AdminLayout title="Category leads list">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Category Leads' }]}
            />

            <Typography.Title level={4}>Category Leads</Typography.Title>
            <FilterForm
                onFilter={(values) => setFilterQuery(values)}
                isLoading={isLoading}
            />
            <TableAddButton href="/category-leads/create"/>
            <Table
                rowKey={(e) => e.id}
                dataSource={data}
                bordered
                loading={isLoading}
                columns={columns}
                pagination={pagination}
                style={{ width: '100%' }}
            />
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <Form
            form={form}
            name="control-hooks"
            onFinish={(e) => onFilter(e)}
            style={{ width: '500px' }}
        >
            <Form.Item name="value" label="Value">
                <Input/>
            </Form.Item>
            <Form.Item name="category" label="Category">
            <CategorySelector isLeaf={true}/>
            </Form.Item>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </Form>
    );
};

export default CategoryLeadListPage;
