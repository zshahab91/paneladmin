import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import AddCategoryLead from '@components/CategoryLeadsPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';


const CategoryLeadAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a Category lead">
      <BreadcrumbPage listLinks={[{ name: 'Home', route: '/' }, { name: 'Category Leads', route: '/category-leads' }, {name: 'Add New Category Leads'}]} />
      <AddCategoryLead />
    </AdminLayout>
  );
};

export default CategoryLeadAddPage;
