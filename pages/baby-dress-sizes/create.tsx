import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/BabyDressSizesPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ProductOptionsAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a baby dress size">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Baby dress size', route: '/baby-dress-sizes' },
          { name: 'Add New Baby Dress size' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ProductOptionsAddPage;
