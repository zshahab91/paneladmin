import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import TableAddButton from '@components/CP/TableAddButton';
import TableEditButton from '@components/CP/TableEditButton';
import { BabyDressSizesQueryProps, useBabyDressSizes } from '@components/hooks/productOptions';
import { Button, Col, Form, Input, Row, Space, Table, Typography } from 'antd';
import React, { useState } from 'react';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Slug',
        dataIndex: 'code',
        key: 'code',
    },
    {
        title: 'Value',
        dataIndex: 'value',
        key: 'value',
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => <TableEditButton href={`baby-dress-sizes/${id}/edit`}/>,
    },
];

const CustomerListPage: React.FC = () => {
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<BabyDressSizesQueryProps>({});
    const { data, isLoading, totalItems, perPage } = useBabyDressSizes(filterQuery, { page: pageIndex });

    function renderTable(): React.ReactNode {
        return (
            <Table
                dataSource={data}
                bordered
                loading={isLoading}
                columns={columns}
                style={{ width: '100%' }}
                pagination={{
                    pageSize: perPage,
                    total: totalItems,
                    current: pageIndex,
                }}
                //@ts-ignore
                onChange={(e) => setPageIndex(e.current)}
            />
        );
    }

    return (
        <AdminLayout title="Baby dress sizes list">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Baby dress sizes' }]}
            />
            <Typography.Title level={4}>Baby dress sizes</Typography.Title>
            <FilterForm
                onFilter={(values: BabyDressSizesQueryProps) => setFilterQuery(values)}
                isLoading={isLoading}
            />
            <TableAddButton href="/baby-dress-sizes/create"/>
            <br/>
            {renderTable()}
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
            <Row gutter={24}>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="code" label="Code">
                        <Input/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="value" label="Value">
                        <Input/>
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </FilterFormLayout>
    );
};

export default CustomerListPage;
