import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/BabyDressSizesPage/Edit';

const ProductOptionsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a baby dress size">
      <Edit />
    </AdminLayout>
  );
};

export default ProductOptionsEditPage;
