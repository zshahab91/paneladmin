import React, { useEffect, useState } from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Input,
  message,
  Row,
  Spin,
} from 'antd';
import { Store } from 'rc-field-form/lib/interface';
import { IAdmin } from '@entities/admin';
import { IValidationError } from '@entities/validation.error';
import { AdminApiService } from '@services/admin/admin.api.service';
import { useRouter } from 'next/router';
import BreadcrumbPage from '@components/Breadcrumb';
import Link from 'next/link';
import FormAlignment from '@components/CP/FormAlignment';

const NOTIFICATION_KEY = 'ADMIN_UPDATE';

const Page: NextPage = () => {
  const router = useRouter();
  const service = new AdminApiService();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [data, setData] = useState<IAdmin>({});
  useEffect(() => {
    (async () => {
      if (router.query.id === undefined) {
        return;
      }

      const {
        data: { results: admin },
      } = await service.show((router.query.id as unknown) as number);
      setData(admin);
      setIsLoading(false);
    })();
  }, [router.query.id]);

  const onFinish = async (admin: IAdmin): Promise<void> => {
    try {
      message.loading({ content: 'IsLoading...!', key: NOTIFICATION_KEY });
      await service.update((router.query.id as unknown) as number, admin);
      message.success({ key: NOTIFICATION_KEY, content: 'Successed!' });
      router.back();
    } catch (e) {
      const response: IValidationError = e.response.data;
      message.error({ key: NOTIFICATION_KEY, content: response.message });
    }
  };

  if (isLoading) {
    return (
      <AdminLayout title="Change password">
        <Spin />
      </AdminLayout>
    );
  }

  return (
    <AdminLayout title="Change password">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Admins', route: '/brands' },
          { name: `Change ${data.name} ${data.family} password` },
        ]}
      />
      <Card
        title="Update Admin"
        extra={
          <Link href="/admins">
            <a>View All</a>
          </Link>
        }
      >
        <FormAlignment>
          <Form
            name={'admin'}
            layout={'vertical'}
            initialValues={data}
            onFinish={(onFinish as unknown) as (values: Store) => void}
          >
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  label={'First Name'}
                  labelAlign={'right'}
                  name="name"
                  rules={[
                    { required: true, message: 'Please Add First Name!' },
                  ]}
                >
                  <Input disabled />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  label={'Last Name'}
                  name="family"
                  rules={[{ required: true, message: 'Please Add Last Name!' }]}
                >
                  <Input disabled />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  label={'Email'}
                  name="email"
                  rules={[
                    { required: true, message: 'Please Add Email!' },
                    { type: 'email', message: 'Email is Incorrect!' },
                  ]}
                >
                  <Input disabled />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  label={'Mobile'}
                  name="mobile"
                  rules={[
                    { required: true, message: 'Please Add Mobile!' },
                    {
                      pattern: /^(((98)|(\+98)|(0098)|0)(9){1}[0-9]{9})+$/,
                      message: 'Mobile is Incorrect!',
                    },
                  ]}
                >
                  <Input disabled />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={24}>
                <Form.Item label={'Password'} name="password">
                  <Input.Password />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={24}>
                <Form.Item>
                  <Form.Item name="active" valuePropName="checked" noStyle>
                    <Checkbox disabled>IsActive</Checkbox>
                  </Form.Item>
                </Form.Item>
              </Col>
            </Row>

            <Form.Item wrapperCol={{ dir: 'ltr' }}>
              <Button type={'primary'} htmlType="submit">
                Save
              </Button>
            </Form.Item>
          </Form>
        </FormAlignment>
      </Card>
    </AdminLayout>
  );
};

export default Page;
