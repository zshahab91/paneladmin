//"@typescript-eslint/no-explicit-any"
import React, { useCallback, useState } from 'react';
import AdminLayout from '@components/AdminLayout';
import { ColumnsType } from 'antd/lib/table';
import { IAdmin } from '@entities/admin';
import {
  GetRowKey,
  Key,
  SorterResult,
  TableCurrentDataSource,
  TablePaginationConfig,
} from 'antd/lib/table/interface';
import { NextPage } from 'next';
import BreadcrumbPage from '@components/Breadcrumb';
import TableEditButton from '@components/CP/TableEditButton';
import TableChangePasswordButton from '@components/CP/TableChangePasswordButton';
import TableAddButton from '@components/CP/TableAddButton';
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Space,
  Table,
  Tag,
} from 'antd';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import { useAdmins } from '@components/hooks/admin';

const columns: ColumnsType<IAdmin> = [
  {
    title: 'ID',
    key: 'index',
    render: (v, r, i) => (i + 1).toString(),
  },
  {
    title: 'First Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Last Name',
    dataIndex: 'family',
    key: 'family',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Mobile',
    dataIndex: 'mobile',
    key: 'mobile',
  },
  {
    title: 'Active',
    dataIndex: 'active',
    key: 'active',
    render: (v) => (
      <Tag color={v ? 'green' : 'red'}>{v ? 'Active' : 'InActive'}</Tag>
    ),
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id) => (
      <>
        <TableEditButton href={`admins/${id}/edit`} />
        <TableChangePasswordButton href={`admins/${id}/password`} />
      </>
    ),
  },
];

interface IChangeEvent<T> {
  (
    pagination: TablePaginationConfig,
    filters: Record<string, Key[] | null>,
    sorter: SorterResult<T> | SorterResult<T>[],
    extra: TableCurrentDataSource<T>,
  ): void;
}

const Index: NextPage = () => {
  const rowKeyProvider: GetRowKey<IAdmin> = useCallback<GetRowKey<IAdmin>>(
    (r) => r.id || 1,
    [],
  );
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
    {},
  );
  const { data, isLoading, totalItems, perPage } = useAdmins(filterQuery, {
    page: pageIndex,
  });

  return (
    <AdminLayout title="Admins list">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Admins', route: 'admins' },
        ]}
      />
        <FilterForm
        onFilter={(values) => setFilterQuery(values)}
        isLoading={isLoading}
      />
      <br />
       <TableAddButton href="/admins/create" />
      <br />
      <Table
        bordered
        loading={isLoading}
        columns={columns}
        rowKey={rowKeyProvider}
        pagination={{
          pageSize: perPage,
          total: totalItems,
          current: pageIndex,
        }}
        dataSource={data}
        //@ts-ignore
        onChange={(e)=> setPageIndex(e.current)}
      />
    </AdminLayout>
  );
};
interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="name" label="Name">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="family" label="Family">
            <Input />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={8}>
          <Form.Item name="mobile" label="Mobile">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="email" label="Email">
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};
export default Index;
