import { NextPage } from "next";
import AdminLayout from "@components/AdminLayout";
import { Form, Input, Button, Checkbox, Row, Col, Typography, message, Alert } from 'antd';
import React, { useState } from "react";
import { IAdmin } from "../../entities/admin";
import { Store } from "rc-field-form/lib/interface";
import { AdminApiService } from "@services/admin/admin.api.service";
import { IValidationError } from "../../entities/validation.error";
import { useRouter } from "next/router";

const NOTIFICATION_KEY = 'ADMIN_CREATE';

const Page: NextPage = () => {
    const service = new AdminApiService();
    const [errors, setErrors] = useState<{ [name: string]: string[] }>({});
    const router = useRouter();

    const onFinish = async (admin: IAdmin): Promise<void> => {
        try {
            message.loading({ content: 'IsLoading...', key: NOTIFICATION_KEY })
            await service.create(admin);
            message.success({ key: NOTIFICATION_KEY, content: 'Successed!' });
            setTimeout(() => {
                router.push(`/admins`);
              }, 500);
        } catch (e) {
            const response: IValidationError = e.response.data;
            message.error({ key: NOTIFICATION_KEY, content: response.message })
            setErrors(response.results);
        }
    };


    return (
        <AdminLayout title="Add an admin">
            <Row>
                <Col span={12} offset={6}>
                    {Object.keys(errors).map(key => <Alert key={key} type="error" message={errors[key]}/>)}

                    <Typography.Title level={3}>Create Admin</Typography.Title>

                    <Form
                        name={"admin"}
                        layout={"vertical"}
                        initialValues={{ active: true }}
                        onFinish={onFinish as unknown as (values: Store) => void}
                    >
                        <Row gutter={24}>
                            <Col span={12}>
                                <Form.Item
                                    label={"First Name"}
                                    labelAlign={"right"}
                                    name="name"
                                    rules={[
                                        { required: true, message: 'Please Add First Name' }
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                    label={"Last Name"}
                                    name="family"
                                    rules={[{ required: true, message: 'Please Add Last Name' }]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={24}>
                            <Col span={12}>
                                <Form.Item
                                    label={"Email"}
                                    name="email"
                                    rules={[
                                        { required: true, message: 'Please Enter Email!' },
                                        { type: "email", message: "Email is Incorrect!" }
                                    ]}
                                >
                                    <Input style={{direction: "ltr", textAlign: "left"}}/>
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                    label={"Mobile"}
                                    name="mobile"
                                    rules={[
                                        { required: true, message: 'Please Add Mobile!' },
                                        {
                                            pattern: /^(((98)|(\+98)|(0098)|0)(9){1}[0-9]{9})+$/,
                                            message: 'Mobile is Incorrect!'
                                        }
                                    ]}
                                >
                                    <Input style={{direction: "ltr", textAlign: "left"}}/>
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={24}>
                            <Col span={24}>
                                <Form.Item
                                    label={"Password"}
                                    name="password"
                                    rules={[{ required: true, message: 'Please Add Password!' }]}
                                >
                                    <Input.Password/>
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={24}>
                            <Col span={24}>
                                <Form.Item>
                                    <Form.Item name="active" valuePropName="checked" noStyle>
                                        <Checkbox>IsActive</Checkbox>
                                    </Form.Item>
                                </Form.Item>
                            </Col>
                        </Row>


                        <Form.Item wrapperCol={{ dir: "ltr" }}>
                            <Button type={"primary"} htmlType="submit">Save</Button>
                        </Form.Item>
                    </Form>

                </Col>
            </Row>
        </AdminLayout>
    )
}

export default Page;