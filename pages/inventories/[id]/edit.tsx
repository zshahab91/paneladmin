import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';
import Inventory from '@entities/inventories';
import { IValidationError } from '@entities/validation.error';
import InventoryApiService from '@services/InventoryService/Inventory.api.service';
import {
  Button,
  Card,
  Checkbox,
  Col,
  Descriptions,
  Divider,
  Form,
  Input,
  InputNumber,
  message,
  Row,
} from 'antd';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/lib/interface';
import React, { useEffect, useState } from 'react';

const NOTIFICATION_KEY = 'CUSTOMER_CREATE';

const Page: NextPage = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const [inventory, setInventory] = useState<Inventory>();
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (router.query.id === undefined) {
        return;
      }

      const {
        data: { results: inventory },
      } = await InventoryApiService.show(
        (router.query.id as unknown) as number,
      );
      //@ts-ignore
      setTitlePage(inventory?.stock);
      form.setFieldsValue(inventory);
      setInventory(inventory);
    })();
  }, [router.query.id]);

  const onFinish = async (inventory: Inventory): Promise<void> => {
    try {
      message.loading({ content: 'IsLoading...!', key: NOTIFICATION_KEY });
      await InventoryApiService.update(
        (router.query.id as unknown) as number,
        inventory,
      );
      message.success({
        key: NOTIFICATION_KEY,
        content: 'Done Successfully.',
      });
      // form.resetFields();
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }
  };

  return (
    <AdminLayout title="Edit an inventory">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Inventories', route: '/inventories' },
          { name: titlePage },
        ]}
      />
      <Card
        title={'Update Inventory'}
        extra={
          <Link href={`/inventories`}>
            <a>View All</a>
          </Link>
        }
      >
        <Descriptions column={3}>
          <Descriptions.Item span={3} label="Seller">
            {inventory?.seller?.name}
          </Descriptions.Item>

          {inventory?.variant?.optionValues?.map((item) => (
            <Descriptions.Item key={item.id} label={item.option?.name}>
              {item.value}
            </Descriptions.Item>
          ))}
        </Descriptions>
        <Divider />

        <Form
          form={form}
          name={'inventory'}
          layout={'vertical'}
          initialValues={{ active: true }}
          onFinish={(onFinish as unknown) as (values: Store) => void}
        >
          <Row gutter={24}>
            <Col span={8}>
              <Form.Item
                label={'Stock'}
                name="stock"
                rules={[
                  {
                    required: true,
                    message: 'Please Add Stock!',
                  },
                ]}
              >
                <Input type={'number'} />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                label={'Price'}
                name="price"
                rules={[
                  {
                    required: true,
                    message: 'Please Add Price!',
                  },
                ]}
              >
                <InputNumber
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={(value) => (value || '').replace(/(,*)/g, '')}
                  onChange={(price) => form.setFieldsValue({ price })}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                label={'Finel Price'}
                name="finalPrice"
                rules={[
                  {
                    required: true,
                    message: 'Please Add Final Price!',
                  },
                ]}
              >
                <InputNumber
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                  parser={(value) => (value || '').replace(/(,*)/g, '')}
                  onChange={(finalPrice) => form.setFieldsValue({ finalPrice })}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={12}>
              <Form.Item
                label={'Max Purchase Per Order'}
                name="maxPurchasePerOrder"
                rules={[
                  {
                    required: true,
                    message: 'Please Add Max Purchase Per Order!',
                  },
                ]}
              >
                <Input type={'number'} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label={'Lead time'}
                name="suppliesIn"
                rules={[
                  {
                    required: true,
                    message: 'Please Add Supplies In!',
                  },
                ]}
              >
                <InputNumber min={0}  step={1}  />
                {/* <Input type={'number'} min="0" max="1" /> */}
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col span={24}>
              <Form.Item
                label={'Is Trackable Inventory'}
                name="isTrackable"
                valuePropName="checked"
              >
                <Checkbox>Reduce inventory if purchased</Checkbox>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col span={24}>
              <Form.Item
                label={'Inventory active status'}
                name="isActive"
                valuePropName="checked"
              >
                <Checkbox>If inventory is active and available</Checkbox>
              </Form.Item>
            </Col>
          </Row>

          <Form.Item wrapperCol={{ dir: 'ltr' }}>
            <Button type={'primary'} htmlType="submit">
              Save
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </AdminLayout>
  );
};

export default Page;
