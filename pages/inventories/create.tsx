import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  message,
  Row,
  Select,
  Checkbox,
} from 'antd';
import React, { useEffect, useState } from 'react';
import { Store } from 'rc-field-form/lib/interface';
import { IValidationError } from '@entities/validation.error';
import Link from 'next/link';
import InventoryApiService from '@services/InventoryService/Inventory.api.service';
import Inventory from '@entities/inventories';
import { ISeller } from '@entities/seller';
import SellerApiService from '@services/SellerService/Seller.api.service';
import { ProductVariantApiService } from '@services/ProductServices/product.variant.api.service';
import ProductVariant from '@entities/productVariant';
import BreadcrumbPage from '@components/Breadcrumb';

const NOTIFICATION_KEY = 'CUSTOMER_CREATE';

const Add: NextPage = () => {
  const [form] = Form.useForm();
  const [sellers, setSellers] = useState<ISeller[]>();
  const [variants, setVariants] = useState<ProductVariant[]>();
  useEffect(() => {
    (async () => {
      const {
        data: { results: sellers },
      } = await SellerApiService.getList();
      setSellers(sellers);
      const {
        data: { results: variants },
      } = await ProductVariantApiService.index();
      setVariants(variants);
    })();
  }, []);

  const onFinish = async (inventory: Inventory): Promise<void> => {
    try {
      message.loading({ content: 'IsLoading...!', key: NOTIFICATION_KEY });
      await InventoryApiService.create(inventory);
      message.success({
        key: NOTIFICATION_KEY,
        content: 'Done Successfully.',
      });
      form.resetFields();
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }
  };

  return (
    <AdminLayout title="Add an inventory">
        <BreadcrumbPage
          listLinks={[
            { name: 'Home', route: '/' },
            { name: 'Inventories', route: '/inventories' },
            { name: 'Add New Inventory' },
          ]}
        />
          <Card
            title={'Create Inventory'}
            extra={
              <Link href={`/inventories`}>
                <a>View All</a>
              </Link>
            }
          >
            <Form
              form={form}
              name={'inventory'}
              layout={'vertical'}
              initialValues={{ active: true }}
              onFinish={(onFinish as unknown) as (values: Store) => void}
            >
              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label={'Seller'}
                    labelAlign={'right'}
                    name="seller"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Seller!',
                      },
                    ]}
                  >
                    <Select>
                      {sellers?.map((seller) => (
                        <Select.Option key={seller.id} value={seller.id}>
                          {seller.name}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label={'Variant'}
                    labelAlign={'right'}
                    name="variant"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Variant!',
                      },
                    ]}
                  >
                    <Select>
                      {variants?.map((variant) => (
                        <Select.Option key={variant.id} value={variant.id}>
                          {variant.code}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col span={8}>
                  <Form.Item
                    label={'Stock'}
                    name="stock"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Stock!',
                      },
                    ]}
                  >
                    <Input type={'number'} />
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item
                    label={'Price'}
                    name="price"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Price!',
                      },
                    ]}
                  >
                    <Input type={'number'} />
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item
                    label={'Final Price'}
                    name="finalPrice"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Finel Price!',
                      },
                    ]}
                  >
                    <Input type={'number'} />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label={'Max Purchase'}
                    name="maxPurchasePerOrder"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Max Purchase Per Order!',
                      },
                    ]}
                  >
                    <Input type={'number'} />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label={'Supplies In'}
                    name="suppliesIn"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Supplies In!',
                      },
                    ]}
                  >
                    <Input type={'number'} />
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col span={24}>
                  <Form.Item
                    label={'Is Trackable'}
                    name="isTrackable"
                    valuePropName="checked"
                  >
                    <Checkbox>Reduce inventory if purchased</Checkbox>
                  </Form.Item>
                </Col>
              </Row>

              <Form.Item wrapperCol={{ dir: 'ltr' }}>
                <Button type={'primary'} htmlType="submit">
                  Save
                </Button>
              </Form.Item>
            </Form>
          </Card>
    </AdminLayout>
  );
};

export default Add;
