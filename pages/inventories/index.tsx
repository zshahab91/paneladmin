import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import ProductSelector from '@components/CP/ProductSelector';
import SellerSelector from '@components/CP/SellerSelector';
import TableEditButton from '@components/CP/TableEditButton';
import { useInventories } from '@components/hooks/inventories';
import Product from "@entities/product";
import Seller from "@entities/seller";
import { Button, Col, Form, Input, Row, Select, Space, Table, Tag } from 'antd';
import { useRouter } from 'next/router';
import React, { useState } from 'react';

const columns = [
    {
        title: 'TVI',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Product',
        dataIndex: ['variant', 'product', 'title'],
        key: 'product',
    },
    {
        title: 'Seller',
        dataIndex: ['seller', 'name'],
        key: 'seller',
    },
    {
        title: 'Stock',
        dataIndex: 'stock',
        key: 'stock',
    },
    {
        title: 'Price ( Toman )',
        dataIndex: 'price',
        key: 'price',
        render: (price: number) => price.toLocaleString(),
    },
    {
        title: 'Final Price ( Toman )',
        dataIndex: 'finalPrice',
        key: 'finalPrice',
        render: (price: number) => price.toLocaleString(),
    },
    {
        title: 'Most purchases per order',
        dataIndex: 'maxPurchasePerOrder',
        key: 'maxPurchasePerOrder',
    },
    {
        title: 'Lead time',
        dataIndex: 'suppliesIn',
        key: 'suppliesIn',
    },
    {
        title: 'The number of purchases',
        dataIndex: 'orderCount',
        key: 'orderCount',
    },
    {
        title: 'Traceable',
        dataIndex: 'isTrackable',
        key: 'isTrackable',
        render: (isTrackable: boolean) => (
            <Tag color={isTrackable ? 'cyan' : 'magenta'}>
                {isTrackable ? 'Yes' : 'No'}
            </Tag>
        ),
    },
    {
        title: 'Is active',
        dataIndex: 'isActive',
        key: 'isActive',
        render: (isActive: boolean) => (
            <Tag color={isActive ? 'cyan' : 'magenta'}>{isActive ? 'Yes' : 'No'}</Tag>
        ),
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => (
            <>
                <TableEditButton href={`inventories/${id}/edit`}/>
                {/* <DeleteButton onDelete={() => InventoryApiService.delete(id)} /> */}
            </>
        ),
    },
];

const InventoriesListPage: React.FC = () => {
    const {
        query: { product },
    } = useRouter();
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<{
        product?: Product;
        seller?: Seller;
        stock?: number;
        isActive?: boolean;
        hasDiscount?: boolean;
    }>({});
    const { data, isLoading, totalItems, perPage } = useInventories({
        ...filterQuery,
        // product: product ? (product as unknown) as number : filterQuery.product?.id,
        seller: filterQuery.seller?.id,
    }, { page: pageIndex });

    const pagination = totalItems! > perPage! && {
        total: totalItems,
        pageSize: perPage,
        current: pageIndex,
        onChange: setPageIndex,
    };

    return (
        <AdminLayout title="Inventories list">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Inventories' }]}
            />
            <FilterForm
                onFilter={(values) => setFilterQuery(values)}
                isLoading={isLoading}
            />
            <Table
                rowKey={(e) => e.id}
                dataSource={data}
                bordered
                loading={isLoading}
                columns={columns}
                pagination={pagination}
                style={{ width: '100%' }}
            />
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
            <Row gutter={24}>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="id" label="TVI">
                        <Input/>
                    </Form.Item>
                </Col>
                {/*<Col span={24} xl={6} md={8}>*/}
                {/*    <Form.Item name="product" label="Product">*/}
                {/*        <ProductSelector/>*/}
                {/*    </Form.Item>*/}
                {/*</Col>*/}
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="seller" label="Seller">
                        <SellerSelector/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="stock" label="Stock">
                        <Input/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="isActive" label="Is active">
                        <Select>
                            <Select.Option value={1}>Active</Select.Option>
                            <Select.Option value={0}>Inactive</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="hasDiscount" label="Has discount">
                        <Select>
                            <Select.Option value={1}>Yes</Select.Option>
                            <Select.Option value={0}>No</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="productId" label="TPI">
                        <Input />
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="productSlug" label="TPS">
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </FilterFormLayout>
    );
};

export default InventoriesListPage;
