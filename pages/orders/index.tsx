import React, { useState } from 'react';
import {
    Button,
    Col,
    Form,
    Input,
    Row,
    Select,
    Space,
    Table,
    Typography,
} from 'antd';
import CPDatePicker from "@components/CP/CPDatePicker/CPDatePicker";
import AdminLayout from '@components/AdminLayout';
import ShippingMethod from '@entities/shippingMethod';
import { useOrders } from '@components/hooks/orders';
import TableViewButton from '@components/CP/TableViewButton';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import Order, { OrderStatusEnum } from '@entities/order';
import { PaymentMethodsEnum, PaymentStatusEnum } from '@entities/payment';
import CustomerSelector from '@components/CP/CustomerSelector';
import ProvinceSelector from '@components/CP/ProvinceSelector';
import CitySelector from '@components/CP/CitySelector';
import SellerSelector from '@components/CP/SellerSelector';
import Customer from '@entities/customer';
import moment from "jalali-moment";

const columns = [
    {
        title: 'Identifier',
        dataIndex: 'identifier',
        key: 'identifier',
    },
    {
        title: 'Customer',
        dataIndex: 'customer',
        key: 'customer',
        render: (customer: Customer) => (
            <span style={{ direction: 'rtl' }}>
        {`${customer?.name || ''} ${customer?.family || ''} (${customer.id})`}
      </span>
        ),
    },
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
    },
    {
        title: 'Sub Total ( Toman )',
        dataIndex: 'subtotal',
        key: 'subtotal',
        render: (subtotal: Order['subtotal']) => subtotal?.toLocaleString(),
    },
    {
        title: 'Grand Total ( Toman )',
        dataIndex: 'grandTotal',
        key: 'grandTotal',
        render: (grandTotal: Order['grandTotal']) => grandTotal?.toLocaleString(),
    },
    {
        title: 'Payment Method',
        dataIndex: 'paymentMethod',
        key: 'paymentMethod',
    },
    {
        title: 'Order Items',
        dataIndex: 'orderItems',
        key: 'orderItems',
        render: (orderItems: ShippingMethod['periods']) =>
            orderItems.reduce((previousValue) => previousValue + 1, 0),
    },
    {
        title: 'Create At',
        dataIndex: 'createdAt',
        key: 'createdAt',
        render: (createdAt: string) => moment(createdAt).format('jYYYY-jMM-jDD HH:mm:ss')
    },
    {
        title: 'Updated At',
        dataIndex: 'updatedAt',
        key: 'updatedAt',
        render: (updatedAt: string) => moment(updatedAt).format('jYYYY-jMM-jDD HH:mm:ss')
    },
    {
        title: 'Actions',
        dataIndex: 'id',
        key: 'id',
        render: (id: number) => <TableViewButton href={`/orders/${id}/details`}/>,
    },
];

const OrdersListPage: React.FC = () => {
    const [pageIndex, setPageIndex] = useState(1);
    const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
        {},
    );
    const { data, isLoading, totalItems, perPage } = useOrders({
        ...filterQuery,
        seller: filterQuery.seller?.id,
        customer: filterQuery.customer?.id,
        province: filterQuery.province?.id,
        city: filterQuery.city?.id,
    }, { page: pageIndex });

    const pagination = totalItems! > perPage! && {
        total: totalItems,
        pageSize: perPage,
        current: pageIndex,
        onChange: setPageIndex,
    };

    return (
        <AdminLayout title="Orders list">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Orders' }]}
            />

            <Typography.Title level={4}>Orders</Typography.Title>
            <FilterForm
                onFilter={(values) => setFilterQuery(values)}
                isLoading={isLoading}
            />
            <Table
                rowKey={(e) => e.id}
                dataSource={data}
                bordered
                loading={isLoading}
                columns={columns}
                pagination={pagination}
                style={{ width: '100%' }}
            />
        </AdminLayout>
    );
};

interface FilterFormProps {
    onFilter: (values: { [key: string]: unknown }) => void;
    isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
    const [form] = Form.useForm();

    return (
        <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
            <Row gutter={24}>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="identifier" label="Identifier">
                        <Input/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="seller" label="Seller" valuePropName="value">
                        <SellerSelector/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="nationalNumber" label="National Number">
                        <Input/>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="status" label="Status">
                        <Select>
                            {Object.keys(OrderStatusEnum).map((key) => (
                                <Select.Option key={key} value={key}>
                                    {OrderStatusEnum[key as keyof typeof OrderStatusEnum]}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="paymentMethod" label="Payment method">
                        <Select>
                            {Object.keys(PaymentMethodsEnum).map((key) => (
                                <Select.Option key={key} value={key}>
                                    {PaymentMethodsEnum[key as keyof typeof PaymentMethodsEnum]}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="paymentStatus" label="Payment status">
                        <Select>
                            {Object.keys(PaymentStatusEnum).map((key) => (
                                <Select.Option key={key} value={key}>
                                    {PaymentStatusEnum[key as keyof typeof PaymentStatusEnum]}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="customer" label="Customer" valuePropName="value">
                        <CustomerSelector />
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="province" label="Province" valuePropName="value">
                        <ProvinceSelector />
                    </Form.Item>
                </Col>
                <Col span={24} xl={6} md={8}>
                    <Form.Item name="city" label="City" valuePropName="value">
                        <CitySelector />
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={8}>
                    <Form.Item name="createdAtFrom" label="Created from">
                        <CPDatePicker onChange={value => form.setFieldsValue({ createdAtFrom: value })}/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={8}>
                    <Form.Item name="createdAtTo" label="Created to">
                        <CPDatePicker onChange={value => form.setFieldsValue({ createdAtTo: value })}/>
                    </Form.Item>
                </Col>

                <Col span={24} xl={6} md={8}>
                    <Form.Item name="balanceStatus" label="Balance">
                        <Select>
                            <Select.Option value={'CREDITOR'}>Creditor</Select.Option>
                            <Select.Option value={'DEBTOR'}>Debtor</Select.Option>
                            <Select.Option value={'BALANCE'}>Balance</Select.Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={isLoading}>
                        Filter
                    </Button>
                    <Button
                        htmlType="button"
                        onClick={() => {
                            form.resetFields();
                            location.reload();
                        }}
                    >
                        Reset
                    </Button>
                </Space>
            </Form.Item>
        </FilterFormLayout>
    );
};

export default OrdersListPage;
