import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Details from "@components/OrderPage/Details";

const OrderDetailsPage: NextPage = () => {
  return (
    <AdminLayout title="Category detail">
      <Details />
    </AdminLayout>
  );
};

export default OrderDetailsPage;
