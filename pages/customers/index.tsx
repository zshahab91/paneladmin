import React, { useState } from 'react';
import moment from 'moment-jalaali';
import {
  Button,
  Col,
  Form,
  Input,
  Row, Select,
  Space,
  Table,
  Typography,
} from 'antd';
import AdminLayout from '@components/AdminLayout';
import { useCustomers } from '@components/hooks/customers';
import BreadcrumbPage from '@components/Breadcrumb';
import TableEditButton from '@components/CP/TableEditButton';
import CPDatePicker from '@components/CP/CPDatePicker/CPDatePicker';
import FilterFormLayout from '@components/CP/FilterFormLayout';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Mobile',
    dataIndex: 'mobile',
    key: 'mobile',
  },
  {
    title: 'First Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Last Name',
    dataIndex: 'family',
    key: 'family',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Birthday',
    dataIndex: 'birthday',
    key: 'birthday',
    render: (date: string) =>
      moment(date, 'YYYY-M-D HH:mm:ss').format('jYYYY/jM/jD'),
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableEditButton href={`customers/${id}/edit`} />
      </>
    ),
  },
];

const CustomerListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
    {},
  );
  const { data, isLoading, totalItems, perPage } = useCustomers(filterQuery, {
    page: pageIndex,
  });

  return (
    <AdminLayout title="Customers list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Customers' }]}
      />

      <Typography.Title level={4}>Customers</Typography.Title>
      <FilterForm
        onFilter={(values) => setFilterQuery(values)}
        isLoading={isLoading}
      />
      <br />
      {/* <TableAddButton href="/customers/create" /> */}

      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={{
          pageSize: perPage,
          total: totalItems,
          current: pageIndex,
        }}
        //@ts-ignore
        onChange={(e)=> setPageIndex(e.current)}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="name" label="Name">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="family" label="Family">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="nationalNumber" label="National Number">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="mobile" label="Mobile">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="email" label="Email">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="birthday" label="Birthday">
            <CPDatePicker
              onChange={(value) => {
                form.setFieldsValue({
                  date: value,
                });
              }}
            />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="gender" label="Gender">
            <Select>
              <Select.Option value={'MAlE'}>Male</Select.Option>
              <Select.Option value={'FEMALE'}>Female</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};

export default CustomerListPage;
