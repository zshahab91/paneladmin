import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import { Button, Card, Col, Form, Input, message, Row, Spin, } from 'antd';
import React, { useEffect, useState } from 'react';
import { Store } from 'rc-field-form/lib/interface';
import { IValidationError } from '@entities/validation.error';
import Link from 'next/link';
import { ICustomer } from '@entities/customer';
import { CustomerApiService } from '@services/customer/customer.api.service';
import CPDatePicker from '@components/CP/CPDatePicker/CPDatePicker';
import { useRouter } from 'next/router';
import BreadcrumbPage from '@components/Breadcrumb';
import FormAlignment from "@components/CP/FormAlignment";

const NOTIFICATION_KEY = 'CUSTOMER_CREATE';

const Page: NextPage = () => {
  const router = useRouter();
  const service = new CustomerApiService();
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [titlePage, setTitlePage] = useState('');
  useEffect(() => {
    (async () => {
      if (router.query.id === undefined) {
        return;
      }

      const {
        data: { results: customer },
      } = await service.show((router.query.id as unknown) as number);
      //@ts-ignore
      setTitlePage(
        `Update ${
          customer.name.charAt(0).toUpperCase() + customer.name.slice(1)
        } ${
          customer.family.charAt(0).toUpperCase() + customer.family.slice(1)
        }`,
      );
      form.setFieldsValue(customer);
      setIsLoading(false);
    })();
  }, [router.query.id]);

  const onFinish = async (customer: ICustomer): Promise<void> => {
    try {
      message.loading({ content: 'IsLoading...!', key: NOTIFICATION_KEY });
      await service.update((router.query.id as unknown) as number, customer);
      message.success({
        key: NOTIFICATION_KEY,
        content: 'Done Successfully.',
      });
      router.back()
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: key,
              errors: response.results[key],
            }),
        );
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }
  };

  if (isLoading) {
    return (
      <AdminLayout title="Edit a customer">
        <Spin />
      </AdminLayout>
    );
  }

  return (
    <AdminLayout title="Edit a customer">
        <BreadcrumbPage
          listLinks={[
            { name: 'Home', route: '/' },
            { name: 'Customers', route: '/customers' },
            { name: titlePage },
          ]}
        />

          <Card
            title={'Create Customer'}
            extra={
              <Link href={`/customers`}>
                <a>View All</a>
              </Link>
            }
          >
            <FormAlignment>
            <Form
              form={form}
              name={'customer'}
              layout={'vertical'}
              initialValues={{ active: true }}
              onFinish={(onFinish as unknown) as (values: Store) => void}
            >
              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label={'First Name'}
                    labelAlign={'right'}
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add First Name!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label={'Last Name'}
                    labelAlign={'right'}
                    name="family"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Last Name!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label={'National Number'}
                    name="nationalNumber"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add National Number!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label={'Mobile'}
                    name="mobile"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Mobile!',
                      },
                      {
                        required: true,
                        pattern: /^(((98)|(\+98)|(0098)|0)(9){1}[0-9]{9})+$/,
                        message: 'The entered mobile is not correct.',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label={'Email'}
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Email!',
                      },
                      {
                        required: true,
                        type: 'email',
                        message: 'The entered email is not correct.',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label={'Birthday'}
                    name="birthday"
                    rules={[
                      {
                        required: true,
                        message: 'Please Add Birthday!',
                      },
                    ]}
                  >
                    <CPDatePicker
                      initialValue={form.getFieldValue('birthday')}
                      onChange={(value) => {
                        form.setFieldsValue({
                          birthday: value,
                        });
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>

              <Form.Item wrapperCol={{ dir: 'ltr' }}>
                <Button type={'primary'} htmlType="submit">
                  Save
                </Button>
              </Form.Item>
            </Form>
            </FormAlignment>
          </Card>
    </AdminLayout>
  );
};

export default Page;
