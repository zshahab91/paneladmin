import CategorySelector from "@components/CP/CategorySelector";
import React, { useState } from 'react';
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Space,
  Table,
  Typography,
} from 'antd';
import AdminLayout from '@components/AdminLayout';
import TableEditButton from '@components/CP/TableEditButton';
import DeleteButton from '@components/CP/DeleteButton';
import CategoryDeliveryService from '@services/CategoryDeliveryService';
import { useCategoryDeiveries } from '@components/hooks/categoryDeliveries';
import TableAddButton from '@components/CP/TableAddButton';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from '@components/CP/FilterFormLayout';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Send range from',
    dataIndex: 'start',
    key: 'start',
  },
  {
    title: 'Send interval up',
    dataIndex: 'end',
    key: 'end',
  },
  {
    title: 'Category',
    dataIndex: ['category', 'title'],
    key: 'category',
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableEditButton href={`category-deliveries/${id}/edit`} />
        <DeleteButton onDelete={() => CategoryDeliveryService.delete(id)} />
      </>
    ),
  },
];

const CategoryDeliveryListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
    {},
  );

  const {
    data,
    isLoading,
    totalItems,
    perPage,
  } = useCategoryDeiveries({ ...filterQuery, category: filterQuery.category?.id }, { page: pageIndex });

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Category deliveries list">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Category Deliveries' },
        ]}
      />

      <Typography.Title level={4}>Schedule Categories</Typography.Title>
      <FilterForm
        onFilter={(values) => {
          setFilterQuery(values);
        }}
        isLoading={isLoading}
      />
      <br />
      <TableAddButton href="/category-deliveries/create" />

      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={8} md={16}>
          <Form.Item name="start" label="From">
            <Input placeholder="1" type="number" />
          </Form.Item>
        </Col>
        <Col span={24} xl={8} md={16}>
          <Form.Item name="end" label="To">
            <Input placeholder="10" type="number" />
          </Form.Item>
        </Col>
        <Col span={24} xl={8} md={8}>
          <Form.Item name="category" label="Category">
          <CategorySelector isLeaf={true}/>
          </Form.Item>
        </Col>
      </Row>

      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};

export default CategoryDeliveryListPage;
