import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import AddCategoryDelivery from '@components/CategoryDeliveriesPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';


const CategoriesAddPage: NextPage = () => {
    return (
        <AdminLayout title="Add a category delivery">
            <BreadcrumbPage listLinks={[{name: 'Home', route: '/'}, {
                name: 'Category Deliveries',
                route: '/category-deliveries'
            }, {name: 'Add New Category Deliveries'}]}/>
            <AddCategoryDelivery/>
        </AdminLayout>
    );
};

export default CategoriesAddPage;
