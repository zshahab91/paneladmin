import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/CategoryDeliveriesPage/Edit';

const CategoryDelivaryEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a category delivery">
      <Edit />
    </AdminLayout>
  );
};

export default CategoryDelivaryEditPage;
