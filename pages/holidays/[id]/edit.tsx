import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/HolidayPage/Edit';

const HolidayEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a holiday">
      <Edit />
    </AdminLayout>
  );
};

export default HolidayEditPage;
