import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/HolidayPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const HolidayAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a holiday">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Holidays', route: '/holidays' },
          { name: 'Add New Holiday' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default HolidayAddPage;
