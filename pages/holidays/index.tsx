import SellerSelector from "@components/CP/SellerSelector";
import React, { useEffect, useState } from 'react';
import moment from 'moment-jalaali';
import {
  Button,
  Checkbox,
  Col,
  Form,
  Input,
  message,
  Row,
  Space,
  Table,
  Tag,
  Typography,
} from 'antd';
import AdminLayout from '@components/AdminLayout';
import TableEditButton from '@components/CP/TableEditButton';
import { useHolidays } from '@components/hooks/holidays';
import DeleteButton from '@components/CP/DeleteButton';
import HolidayApiService from '@services/HolidayService/Holiday.api.service';
import SellerApiService from '@services/SellerService/Seller.api.service';
import BreadcrumbPage from '@components/Breadcrumb';
import TableAddButton from '@components/CP/TableAddButton';
import Seller from '@entities/seller';
import CPDatePicker from '@components/CP/CPDatePicker/CPDatePicker';
import SelectSearch from '@components/SelectSearch/index';
import FilterFormLayout from '@components/CP/FilterFormLayout';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Name Fa',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
    render: (date: string) =>
      moment(date, 'YYYY-M-D HH:mm:ss').format('jYYYY/jM/jD'),
  },
  {
    title: 'Supply',
    dataIndex: 'supply',
    key: 'supply',
    render: (supply: boolean) => (
      <Tag color={supply ? 'cyan' : 'magenta'}>{supply ? 'Yes' : 'No'}</Tag>
    ),
  },
  {
    title: 'Seller',
    dataIndex: ['seller', 'name'],
    key: 'seller',
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableEditButton href={`holidays/${id}/edit`} />
        <DeleteButton onDelete={() => HolidayApiService.delete(id)} />
      </>
    ),
  },
];

const HolidayListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);

  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>(
    {},
  );
  const { data, isLoading, totalItems, perPage } = useHolidays({ ...filterQuery, seller: filterQuery.seller?.id }, {
    page: pageIndex,
  });

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Holidays list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Holidays' }]}
      />

      <Typography.Title level={4}>Holidays</Typography.Title>
      <FilterForm
        onFilter={(values) => {
          setFilterQuery(values);
        }}
        isLoading={isLoading}
      />
      <br />
      <TableAddButton href="/holidays/create" />
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};

interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="title" label="Name Fa">
          <Input  style={{direction: "rtl"}} />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={8}>
          <Form.Item name="seller" label="Seller">
            <SellerSelector />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="date" label="Date">
            <CPDatePicker
              onChange={(value) => {
                form.setFieldsValue({
                  // @ts-ignore
                  date: value,
                });
              }}
            />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={8}>
          <Form.Item name="supply" valuePropName="checked" label="Supply">
            <Checkbox />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};

export default HolidayListPage;
