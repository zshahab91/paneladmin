import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/DressSizesPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ProductOptionsAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a dress size">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Dress size', route: '/dress-sizes' },
          { name: 'Add New Dress size' },
        ]}
      />
      <Add />
    </AdminLayout>
  );
};

export default ProductOptionsAddPage;
