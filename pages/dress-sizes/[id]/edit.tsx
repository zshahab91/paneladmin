import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/DressSizesPage/Edit';

const ProductOptionsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit dress size">
      <Edit />
    </AdminLayout>
  );
};

export default ProductOptionsEditPage;
