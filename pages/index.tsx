import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';

// interface Props {
//   userAgent?: string;
// }

const IndexPage: NextPage = () => {
    return (
        <AdminLayout title="Dashboard">
            <BreadcrumbPage listLinks={[{name: 'Home', route: '/'}]} />
            <div>
                Welcom to Dashboard
            </div>
        </AdminLayout>
    );
};

// IndexPage.getInitialProps = async ({ req }) => {
//   const userAgent = req ? req.headers['user-agent'] : navigator.userAgent;
//   return { userAgent };
// };

export default IndexPage;
