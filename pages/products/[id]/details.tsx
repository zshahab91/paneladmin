import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import View from '@components/ProductsPage/View';
import { Col, Row } from 'antd';

const ProductsViewPage: NextPage = () => {
  return (
    <AdminLayout title="Product detail">
      <Row>
        <Col xs={24} md={24} lg={24} xl={24}>
          <View />
        </Col>
      </Row>
    </AdminLayout>
  );
};

export default ProductsViewPage;
