import React from "react";
import { NextPage } from "next";
import AdminLayout from "@components/AdminLayout";
import Inventory from "@components/ProductsPage/Inventory";

const Page: NextPage = () => {
    
    return (
        <AdminLayout title="Create inventory">
            <Inventory />
        </AdminLayout>
    );
}

export default Page;