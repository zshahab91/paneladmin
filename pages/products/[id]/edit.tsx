import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/ProductsPage/Edit';
import { Col, Row } from 'antd';

const ProductsEditPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a product">
      <Row>
        <Col xs={24} md={24} lg={24} xl={24}>
          <Edit />
        </Col>
      </Row>
    </AdminLayout>
  );
};

export default ProductsEditPage;
