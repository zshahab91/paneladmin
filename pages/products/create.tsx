import React from 'react';
import { Col, Row } from 'antd';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import AddProduct from '@components/ProductsPage/Add';
import BreadcrumbPage from '@components/Breadcrumb';

const ProductAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a product">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Products', route: '/products' },
          { name: 'Add New Product' },
        ]}
      />

      <Row gutter={15}>
        <Col xs={24} md={24} lg={24} xl={24}>
          <AddProduct />
        </Col>
      </Row>
    </AdminLayout>
  );
};

export default ProductAddPage;
