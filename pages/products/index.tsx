import AdminLayout from '@components/AdminLayout';
import BreadcrumbPage from '@components/Breadcrumb';
import BrandSelector from '@components/CP/BrandSelector';
import CategorySelector from '@components/CP/CategorySelector';
import FilterFormLayout from '@components/CP/FilterFormLayout';
import SellerSelector from '@components/CP/SellerSelector';
import TableAddButton from '@components/CP/TableAddButton';
import TableEditButton from '@components/CP/TableEditButton';
import TableInventoryButton from '@components/CP/TableInventoryButton';
import TableViewButton from '@components/CP/TableViewButton';
import { useProducts } from '@components/hooks/products';
import Product, { ImageResponse } from '@entities/product';
import { ISeller } from '@entities/seller';
import { ImageProxy } from '@services/image';
import {
  Button,
  Col,
  Form,
  Input,
  Row,
  Select,
  Space,
  Table,
  Tag,
  Typography,
} from 'antd';
import { ColumnType } from 'antd/lib/table/interface';
import React, { useState } from 'react';

const proxy = new ImageProxy();
const columns: ColumnType<Product>[] = [
  {
    title: 'TPI',
    dataIndex: 'id',
    key: 'id',
    fixed: 'left',
  },
  {
    title: 'Image',
    dataIndex: 'featuredImage',
    key: 'featuredImage',
    render: (featuredImage: ImageResponse | undefined) =>
      featuredImage && (
        <img
          src={proxy.generate(featuredImage.url as string, 250, 250)}
          width={65}
          alt=""
        />
      ),
  },
  {
    title: 'Seller',
    dataIndex: 'seller',
    key: 'seller',
    render: (seller: ISeller | undefined) => (seller ? seller.name : 'Timcheh'),
  },
  {
    title: 'Brand',
    dataIndex: 'brand',
    key: 'brand',
    render: (brand: Product['brand']) => brand.title,
  },
  {
    title: 'Category',
    dataIndex: 'category',
    key: 'category',
    render: (category: Product['category']) => category.title,
  },
  {
    title: 'Name Fa',
    dataIndex: 'title',
    key: 'title',
    render: (title: string) => (
      <div style={{ direction: 'rtl', textAlign: 'right' }}>{title}</div>
    ),
  },
  /*{
          title: 'Name En',
          dataIndex: 'subtitle',
          key: 'subtitle',
      },*/
  {
    title: 'Slug',
    dataIndex: 'code',
    key: 'code',
  },
  {
    title: 'Shipping Category',
    dataIndex: 'shippingCategory',
    key: 'shippingCategory',
    render: (shippingCategory: Product['shippingCategory']) =>
      shippingCategory.name,
  },
  {
    title: 'Options',
    dataIndex: 'options',
    key: 'options',
    render: (options: Product['options']) => (
      <>
        {options.map((r) => (
          <Tag key={r.id}>{r.name}</Tag>
        ))}
      </>
    ),
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: 'Is active',
    dataIndex: 'isActive',
    key: 'isActive',
    render: (isActive: boolean) => (
      <Tag color={isActive ? 'cyan' : 'magenta'}>
        {isActive ? 'Active' : 'InActive'}
      </Tag>
    ),
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableInventoryButton href={`products/${id}/inventory`} />
        <TableEditButton href={`products/${id}/edit`} />
        <TableViewButton href={`products/${id}/details`} />
      </>
    ),
    fixed: 'right',
  },
];

interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();
  const status = [
    'DRAFT',
    'EDITING',
    'WAITING_FOR_ACCEPT',
    'CONFIRMED',
    'REJECTED',
    'SOON',
    'UNAVAILABLE',
    'SHUTDOWN',
  ];

  return (
    <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
      <Row gutter={24}>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="id" label="TPI">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="code" label="Slug">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="title" label="Name Fa">
            <Input style={{ direction: 'rtl' }} />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="subtitle" label="Name En">
            <Input />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="status" label="Status">
            <Select placeholder="Select Status" allowClear>
              {status.map((item, inx) => (
                <Select.Option key={inx} value={item}>
                  {item}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="category" label="Category">
            <CategorySelector />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="brand" label="Brand">
            <BrandSelector />
          </Form.Item>
        </Col>
        <Col span={24} xl={6} md={8}>
          <Form.Item name="seller" label="Seller">
            <SellerSelector />
          </Form.Item>
        </Col>

        <Col span={24} xl={6} md={8}>
          <Form.Item name="isActive" label="Is active">
            <Select>
              <Select.Option value={1}>Active</Select.Option>
              <Select.Option value={0}>Inactive</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>

      <Form.Item>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Filter
          </Button>
          <Button
            htmlType="button"
            onClick={() => {
              form.resetFields();
              location.reload();
            }}
          >
            Reset
          </Button>
        </Space>
      </Form.Item>
    </FilterFormLayout>
  );
};

const ProductListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>({});
  const { data, isLoading, totalItems, perPage } = useProducts(
    {
      ...filterQuery,
      brand: filterQuery?.brand?.id,
      category: filterQuery?.category?.id,
      seller: filterQuery?.seller?.id,
    },
    {
      page: pageIndex,
    },
  );

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Products list">
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Products' }]}
      />

      <Typography.Title level={4}>Products</Typography.Title>
      <FilterForm
        onFilter={(values) => setFilterQuery(values)}
        isLoading={isLoading}
      />
      <br />
      <TableAddButton href="/products/create" />
      <br />
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        tableLayout="auto"
        scroll={{ x: 1300 }}
      />
    </AdminLayout>
  );
};

export default ProductListPage;
