import Category from "@entities/category";
import React, { useState } from 'react';
import { Button, Col, Form, Input, Row, Space, Table, Typography, Select } from 'antd';
import AdminLayout from '@components/AdminLayout';
import TableEditButton from '@components/CP/TableEditButton';
import { useCategories } from '@components/hooks/categories';
import TableAddButton from '@components/CP/TableAddButton';
import BreadcrumbPage from '@components/Breadcrumb';
import FilterFormLayout from "@components/CP/FilterFormLayout";

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Slug',
    dataIndex: 'code',
    key: 'code',
  },
  {
    title: 'Name Fa',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: 'Name En',
    dataIndex: 'subtitle',
    key: 'subtitle',
  },
  {
    title: 'Page Title',
    dataIndex: 'pageTitle',
    key: 'pageTitle',
  },
  {
    title: 'Level',
    key: 'level',
    render: (category: Category) => category.leaf ? 'Leaf' : category.level
  },
  {
    title: 'Actions',
    dataIndex: 'id',
    key: 'id',
    render: (id: number) => (
      <>
        <TableEditButton href={`categories/${id}/edit`} />
      </>
    ),
  },
];

const CategoryListPage: React.FC = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const [filterQuery, setFilterQuery] = useState<{ [key: string]: any }>({});
  const { data, isLoading, totalItems, perPage } = useCategories(filterQuery, { page: pageIndex });

  const pagination = totalItems! > perPage! && {
    total: totalItems,
    pageSize: perPage,
    current: pageIndex,
    onChange: setPageIndex,
  };

  return (
    <AdminLayout title="Categories list">
      <Typography.Title level={4}>Categories</Typography.Title>
      <BreadcrumbPage
        listLinks={[{ name: 'Home', route: '/' }, { name: 'Categories' }]}
      />
      <FilterForm
          onFilter={(values) => {
            setFilterQuery(values);
          }}
          isLoading={isLoading}
      />
      <TableAddButton href="/categories/create" />
      <Table
        rowKey={(e) => e.id}
        dataSource={data}
        bordered
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        style={{ width: '100%' }}
      />
    </AdminLayout>
  );
};


interface FilterFormProps {
  onFilter: (values: { [key: string]: unknown }) => void;
  isLoading: boolean;
}

const FilterForm: React.FC<FilterFormProps> = ({ onFilter, isLoading }) => {
  const [form] = Form.useForm();

  return (
      <FilterFormLayout form={form} onFinish={(e) => onFilter(e)}>
        <Row gutter={24}>
          <Col span={24} xl={6} md={8}>
            <Form.Item name="title" label="Name (Fa)">
              <Input />
            </Form.Item>
          </Col>
          <Col span={24} xl={6} md={8}>
            <Form.Item name="subtitle" label="Name (En)">
              <Input />
            </Form.Item>
          </Col>
          <Col span={24} xl={6} md={8}>
            <Form.Item name="isLeaf" label="Is Leaf ?" valuePropName="checked">
                <Select>
                    <Select.Option value={1}>Yes</Select.Option>
                    <Select.Option value={0}>No</Select.Option>
                </Select>
            </Form.Item>
          </Col>

        </Row>

        <Form.Item>
          <Space size="small">
            <Button type="primary" htmlType="submit" loading={isLoading}>
              Filter
            </Button>
            <Button
                htmlType="button"
                onClick={() => {
                  form.resetFields();
                  location.reload();
                }}
            >
              Reset
            </Button>
          </Space>
        </Form.Item>
      </FilterFormLayout>
  );
};

export default CategoryListPage;
