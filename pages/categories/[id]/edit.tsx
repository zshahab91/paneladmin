import React from 'react';
import {NextPage} from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/CategoriesPage/EditCategory';

const CategoriesAddPage: NextPage = () => {
    return (
        <AdminLayout title="Edit a category">
            <Edit/>
        </AdminLayout>
    );
};

export default CategoriesAddPage;
