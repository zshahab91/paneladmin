import React from 'react';
import { Col, Row } from 'antd';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import AddCategory from '@components/CategoriesPage/AddCategory';
import BreadcrumbPage from '@components/Breadcrumb';

const CategoriesAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a category">
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Categories', route: '/categories' },
          { name: 'Add New Category' },
        ]}
      />
      <Row gutter={15}>
        <Col xs={24} md={24} lg={24} xl={24}>
          <AddCategory />
        </Col>
      </Row>
    </AdminLayout>
  );
};

export default CategoriesAddPage;
