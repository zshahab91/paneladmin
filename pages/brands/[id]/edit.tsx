import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Edit from '@components/BrandsPage/Edit';

const BrandAddPage: NextPage = () => {
  return (
    <AdminLayout title="Edit a brand">
      <Edit />
    </AdminLayout>
  );
};

export default BrandAddPage;
