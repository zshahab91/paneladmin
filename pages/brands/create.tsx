import React from 'react';
import { NextPage } from 'next';
import AdminLayout from '@components/AdminLayout';
import Add from '@components/BrandsPage/Add';

const BrandAddPage: NextPage = () => {
  return (
    <AdminLayout title="Add a brand">
      <Add />
    </AdminLayout>
  );
};

export default BrandAddPage;
