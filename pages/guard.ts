import {useRouter} from "next/router";
import {useToken} from "@components/hooks/token";

const redirectUrlIfAuthenticated = '/';
const signInUrl = '/auth/sign-in';
const urlsAllowedOnlyForNotAuthenticatedUsers = [signInUrl]


export default function useRouteGuard() {
    const router = useRouter();
    const token = useToken();

    function getRedirectUrlIfNeeded(): string | undefined {
        if (urlsAllowedOnlyForNotAuthenticatedUsers.includes(router.pathname)) {
            if (token) {
                const returnUrl = router.asPath.split("returnUrl=")[1];
                return returnUrl || redirectUrlIfAuthenticated;
            }
        } else {
            if (!token) {
                const returnUrl = router.pathname.split('[')[0];
                return `${signInUrl}?returnUrl=${returnUrl}`;
            }
        }
    }

    function tryRedirectToUrl(redirectUrl: string) {
        try {
            if (router.pathname.split('?')[0] !== redirectUrl.split('?')[0])
                router.replace(redirectUrl).catch(console.error);
        } catch (e) {
            console.warn('unable to redirect ' + redirectUrl + '. due to SSR');
        }
    }

    const redirectUrl = getRedirectUrlIfNeeded();
    if (redirectUrl) {
        tryRedirectToUrl(redirectUrl);
        return false
    }
    return true;
}
