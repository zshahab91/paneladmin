import React, { useState } from 'react';
import AdminLayout from '@components/AdminLayout';
import { Button, Card, Cascader, Checkbox, message, Typography } from 'antd';
import { CheckboxOptionType } from 'antd/lib/checkbox/Group';
import { CascaderOptionType } from 'antd/lib/cascader';
import { useConfigurations, usePaymentOnlineAdapters, } from '@components/hooks/configurations';
import Configuration, { paymentMethodsInfo } from '@entities/configurations';
import ConfigurationsService from '@services/ConfigurationsApiService/configurations.api.service';
import BreadcrumbPage from '@components/Breadcrumb';

const CheckboxGroup = Checkbox.Group;

const paymentMethodCheckboxOptions: (CheckboxOptionType &
    CascaderOptionType)[] = Object.entries(paymentMethodsInfo).map(
    ([key, value]) => {
        return { label: value.title, value: key, disabled: !value.changeable };
    },
);

const PaymentMethodsPage: React.FC = () => {
    const {
        data: configuration,
        isLoading: isConfigurationLoading,
    } = useConfigurations();
    const {
        data: paymentOnlineAdapters,
        isLoading: isPaymentOnlineAdapters,
    } = usePaymentOnlineAdapters();

    const [activePaymentMethods, setActivePaymentMethods] = useState<string[] | undefined>();
    const [defaultPaymentMethod, setDefaultPaymentMethod] = useState<string | undefined>();

    if (configuration && !activePaymentMethods) {
        const defaultActivePaymentMethods: string[] = configuration
            .filter((r) => r.value === "true")
            .map((r) => r.code);
        setActivePaymentMethods(defaultActivePaymentMethods);
    }

    function renderActivePaymentMethodsCheckList(): React.ReactNode {
        return (
            <CheckboxGroup
                options={paymentMethodCheckboxOptions}
                value={activePaymentMethods || []}
                onChange={(e) => setActivePaymentMethods(e as string[])}
            />
        );
    }

    function renderDefaultPaymentMethodSelector(): React.ReactNode {
        const paymentMethodCascadeOptions: (CheckboxOptionType & CascaderOptionType)[] = paymentOnlineAdapters?.map((r) => ({
            label: r,
            value: r,
        })) || [];

        return (
            <Cascader
                size="large"
                options={paymentMethodCascadeOptions}
                onChange={(e) => {
                    const selectedValue = (e as string[])[0];
                    setDefaultPaymentMethod(selectedValue);
                }}
                value={defaultPaymentMethod ? [defaultPaymentMethod] : [configuration?.find(v => v.code === "DEFAULT_ONLINE_METHOD")?.value ?? ""]}
            />
        );
    }

    function getActivePaymentConfigs(): Partial<Configuration>[] {
        return Object.entries(paymentMethodsInfo)
            .filter(([key, value]) => value.changeable)
            .map(([key, value]) => {
                return {
                    code: key,
                    value: activePaymentMethods?.includes(key) ? 'true' : 'false',
                };
            });
    }

    function getDefaultPaymentConfigs(): Partial<Configuration>[] {
        if (defaultPaymentMethod)
            return [{ code: 'DEFAULT_ONLINE_METHOD', value: defaultPaymentMethod }];
        return [];
    }

    function submit(): void {
        const newConfigs = [
            ...getDefaultPaymentConfigs(),
            ...getActivePaymentConfigs(),
        ];

        const changedSettingsName = newConfigs.map((r) => r.code);
        const previousConfigsThatDidntChanged = configuration!
            .filter((r) => !changedSettingsName.includes(r.code))
            .map((r) => ({ code: r.code, value: r.value }));

        const mergeConfigs = [...previousConfigsThatDidntChanged, ...newConfigs];
        ConfigurationsService.edit(mergeConfigs).catch(console.error);
        message.success('Successfully updated.');
    }

    return (
        <AdminLayout title="Payment methods list">
            <BreadcrumbPage
                listLinks={[{ name: 'Home', route: '/' }, { name: 'Payment Methods' }]}
            />
            <Card>
                <Typography.Title level={4}>Payment Methods</Typography.Title>
                <form
                    onSubmit={(e) => {
                        e.preventDefault();
                        submit();
                    }}
                >
                    {renderActivePaymentMethodsCheckList()}
                    <br/>
                    <br/>
                    {renderDefaultPaymentMethodSelector()}

                    <br/>
                    <br/>
                    <Button
                        type="primary"
                        htmlType="submit"
                        loading={isConfigurationLoading || isPaymentOnlineAdapters}
                    >
                        Save
                    </Button>
                </form>
            </Card>
        </AdminLayout>
    );
};

export default PaymentMethodsPage;
