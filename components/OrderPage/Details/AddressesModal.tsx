import React, { useState } from 'react';
import { message, Modal, Radio, Select } from 'antd';
import Customer from '@entities/customer';
import OrdersApiService from '@services/OrderService/Orders.api.service';

interface AddressesModalProps {
    visible: boolean;
    setVisible: (arg0: boolean) => void;
    onOk: () => void;
    data: Customer;
    activeId: number | undefined;
    orderId: number | undefined;
}

const AddressesModal: React.FC<AddressesModalProps> = ({
                                                           visible,
                                                           setVisible,
                                                           data,
                                                           activeId,
                                                           onOk,
                                                           orderId,
                                                       }) => {
    const [id, setId] = useState(activeId);
    const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };

    const onSubmit = async (): Promise<void> => {
        try {
            await OrdersApiService.changeDeliveryAddress(orderId as number, {
                address: id as number,
            });
            message.success('Edited successfully.');
        } catch (e) {
            message.error(
                `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
            );
        }
        onOk();
    };

    if (!data) {
        return <></>;
    }

    return (
        <Modal
            title="Edit shipping address"
            visible={visible}
            onOk={onSubmit}
            onCancel={() => setVisible(false)}
            width={1000}
            okText="Save"
        >
            <Select
                onChange={(id: number) => {
                    setId(id);
                }}
                value={id}
                defaultValue={activeId}
                style={{ width: '100%' }}
            >
                {data?.addresses!.map((item) => (
                    <Select.Option key={item.id} value={item.id}>
                        {item.fullAddress} | {item.number} | {item.unit} | {item.name} |{' '}
                        {item.family} | {item.postalCode}
                    </Select.Option>
                ))}
            </Select>
        </Modal>
    );
};

export default AddressesModal;
