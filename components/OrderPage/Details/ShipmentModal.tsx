import { OrderShipment, Period, ShipmentStatusEnum } from '@entities/order';
import OrdersApiService from '@services/OrderService/Orders.api.service';
import { Button, Form, InputNumber, message, Modal, Select } from 'antd';
import moment from 'jalali-moment';
import { Store } from 'rc-field-form/es/interface';
import React, { useEffect, useState } from 'react';
import s from './styles.module.scss';

// import CPDatePicker from '@components/CP/CPDatePicker/CPDatePicker';

interface Props {
  activeShipment: OrderShipment | undefined;
  onCancel: () => void;
  onShipmentUpdate: () => void;
  isStatusOnly?: boolean | undefined;
}

const ShipmentModal: React.FC<Props> = ({
  activeShipment,
  onCancel,
  onShipmentUpdate,
  isStatusOnly,
}) => {
  const [dates, setDates] = useState<string[]>();
  const [periods, setPriods] = useState<Period[]>();
  useEffect(() => {
    (async () => {
      if (activeShipment?.id) {
        try {
          //@ts-ignore
          const dataShippment = await OrdersApiService.getDateShipments(
            activeShipment?.id,
          );

          if (dataShippment?.data) {
            const { dates, periods } = dataShippment.data.results;
            setDates(dates);
            setPriods(periods);
          }
        } catch (e) {
          message.error(
            `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
          );
        }
      }
    })();
  }, [activeShipment?.id]);

  const onUpdateShipmentStatus = async ({ status }: Store): Promise<void> => {
    try {
      await OrdersApiService.updateShipmentStatus(activeShipment!.id, status);
      onShipmentUpdate(); // Update all shipments
      onCancel(); // Close the modal
      message.success('Edited successfully.');
    } catch (e) {
      message.error(
        `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
      );
    }
  };

  const onUpdateShipmentGrandTotal = async ({
    grand_total,
  }: Store): Promise<void> => {
    try {
      await OrdersApiService.updateShipmentGrandTotal(
        activeShipment!.id,
        grand_total,
      );
      onShipmentUpdate(); // Update all shipments
      onCancel(); // Close the modal
      message.success('Edited successfully.');
    } catch (e) {
      message.error(
        `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
      );
    }
  };

  const onUpdateShipmentDeliveryDate = async ({
    date,
    period,
  }: Store): Promise<void> => {
    const delivery_date = `${date} ${period}`;
    try {
      await OrdersApiService.updateShipmentDeliveryDate(
        activeShipment!.id,
        delivery_date,
      );
      onShipmentUpdate(); // Update all shipments
      onCancel(); // Close the modal
      message.success('Edited successfully.');
    } catch (e) {
      message.error(
        `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
      );
    }
  };

  const UpdateStatusForm = () => (
    <Form
      layout="inline"
      onFinish={onUpdateShipmentStatus}
      initialValues={{ status: activeShipment?.status }}
    >
      <Form.Item name="status" label="Status">
        <Select className={s.select}>
          {Object.keys(ShipmentStatusEnum).map((key) => (
            <Select.Option key={key} value={key}>
              {ShipmentStatusEnum[key as keyof typeof ShipmentStatusEnum]}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Save
        </Button>
      </Form.Item>
    </Form>
  );

  const UpdateGrandTotalForm = () => (
    <Form
      layout="inline"
      onFinish={onUpdateShipmentGrandTotal}
      initialValues={{ grand_total: activeShipment?.grandTotal }}
    >
      <Form.Item name="grand_total" label="Shipment final price">
        <InputNumber
          formatter={(value) =>
            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          }
          parser={(value) => (value || '').replace(/(,*)/g, '')}
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Save
        </Button>
      </Form.Item>
    </Form>
  );

  const UpdateDeliveryDateForm = () => {
    const [form] = Form.useForm();

    return (
      <Form
        form={form}
        layout="inline"
        onFinish={onUpdateShipmentDeliveryDate}
        initialValues={{
          date: moment(activeShipment?.deliveryDate).format('YYYY-MM-DD'),
          period: '00:00:00',
        }}
      >
        <Form.Item label="Date" name="date">
          <Select
            allowClear
            showSearch
            className={s.select}
            placeholder="Select a Date"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {dates &&
              dates.map((item, inx) => (
                <Select.Option
                  key={inx}
                  //@ts-ignore
                  value={item}
                >
                  {moment(item).format('jYYYY-jMM-jDD')}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>
        <br />
        <br />
        <Form.Item label="Period" name="period">
          <Select
            allowClear
            showSearch
            className={s.select}
            placeholder="Select a Period"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {periods &&
              periods.map((item, inx) => (
                <Select.Option key={inx} value={item.start}>
                  {item.start} - {item.end}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>
        {/* <Form.Item label="Postage Date" shouldUpdate>
          {() => (
            <CPDatePicker
              initialValue={activeShipment?.deliveredAt}
              onChange={(e) => {
                form.setFieldsValue({ delivery_date: e });
              }}
            />
          )}
        </Form.Item>
        <Form.Item name="delivery_date" style={{ margin: 0 }}>
          <Input type="hidden" />
        </Form.Item> */}
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
    );
  };

  return (
    <Modal
      title="Edit Shipment"
      visible={!!activeShipment}
      onCancel={onCancel}
      cancelText="Close"
      footer={null}
      className={s.modal}
    >
      {activeShipment &&
        (isStatusOnly ? (
          <UpdateStatusForm />
        ) : (
          <>
            <UpdateStatusForm />
            <br />
            <UpdateGrandTotalForm />
            <br />
            <UpdateDeliveryDateForm />
          </>
        ))}
    </Modal>
  );
};

export default ShipmentModal;
