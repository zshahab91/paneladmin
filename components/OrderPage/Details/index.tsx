import { HistoryOutlined, PlusOutlined } from '@ant-design/icons/lib';
import BreadcrumbPage from '@components/Breadcrumb';
import CPDatePicker from "@components/CP/CPDatePicker/CPDatePicker";
import AddressesModal from '@components/OrderPage/Details/AddressesModal';
import OrderShipments from '@components/OrderPage/Details/OrderShipments';
import { IBalance } from "@entities/balance";
import Customer from '@entities/customer';
import Order, { IExtra, Note, OrderShipment, OrderStatusEnum, } from '@entities/order';
import { IPaginatedResponse } from '@entities/paginated.response';
import { ITransaction } from "@entities/transaction";
import { IValidationError } from '@entities/validation.error';
import CustomersApiService from '@services/CustomerService/Customers.api.service';
import OrdersApiService from '@services/OrderService/Orders.api.service';
import {
    Button,
    Card,
    Checkbox,
    Col,
    DatePicker,
    Descriptions,
    Divider,
    Drawer,
    Form,
    Input,
    message,
    Modal,
    Radio,
    Row,
    Space,
    Table,
    Typography,
} from 'antd';
import moment from 'jalali-moment';
import { flatten } from 'lodash'
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useEffect, useState } from 'react';
import s from './styles.module.scss';

const { TextArea } = Input;

const Details: React.FC = () => {
    const router = useRouter();
    const { id } = router.query;
    const [orderData, setOrderData] = useState<IPaginatedResponse<Order, IExtra>>();
    const [notes, setNotes] = useState<IPaginatedResponse<Note[]>>();
    const [histories, setHistories] = useState<Order[]>([]);
    const [balance, setBalance] = useState<IBalance>();
    const [isHistoryDrawerVisible, setHistoryDrawerVisibility] = useState<boolean>(false);
    const [shipments, setShipments] = useState<OrderShipment[]>();
    const [customer, setCustomer] = useState<Customer>();
    const [addressModalVisible, setAddressModalVisibility] = useState(false);
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewTitle, setPreviewTitle] = useState('');
    const [itemEditPrice, setItemEditPrice] = useState('');
    const [itemEditPriceID, setItemEditPriceID] = useState(0);
    const [force, setForce] = useState(0);

    const [refund] = Form.useForm();
    const [isRefundModalVisible, setRefundModalVisibility] = useState<boolean>(false);
    const [isRefundSubmissionLoading, setRefundSubmissionLoadingStatus] = useState<boolean>(false);

    const [form] = Form.useForm();

    const onReset = () => {
        form.resetFields();
    };

    const onAddressUpdate = useCallback(async () => {
        const [orders, shipments] = await Promise.all([
            await OrdersApiService.getById(id as string),
            await OrdersApiService.getShipments(id as string),
        ]);
        setOrderData(orders.data);
        setShipments(shipments.data.results as OrderShipment[]);
    }, [id]);

    const onShipmentUpdate = useCallback(async () => {
        const [shipments] = await Promise.all([
            await OrdersApiService.getShipments(id as string),
        ]);
        setShipments(shipments.data.results);
    }, [id]);

    const onRefundSubmitted = async () => {
        const data = refund.getFieldsValue();
        setRefundSubmissionLoadingStatus(true);
        await OrdersApiService.refund(id as string, { ...data, paid_at: `${data.paid_at} 00:00:00` });
        const { data: { results: b } } = await OrdersApiService.getBalance(id as string);
        const { data: o } = await OrdersApiService.getById(id as string)
        setBalance(b);
        setOrderData(o);
        setRefundSubmissionLoadingStatus(false);
        setRefundModalVisibility(false);
    };

    const onRefundModalCanceled = async () => {
        refund.resetFields();
        setRefundSubmissionLoadingStatus(false);
        setRefundModalVisibility(false);
    }

    useEffect(() => {
        (async () => {
            if (id) {
                try {
                    const [orders, shipments, notes, balance] = await Promise.all([
                        await OrdersApiService.getById(id as string),
                        await OrdersApiService.getShipments(id as string),
                        await OrdersApiService.getNotes(id as string),
                        await OrdersApiService.getBalance(id as string),
                    ]);
                    setBalance(balance.data.results)
                    setOrderData(orders.data);
                    setNotes(notes.data);
                    setShipments(shipments.data.results as OrderShipment[]);
                    const customer = await CustomersApiService.getById(
                        orders.data.results.customer.id as number,
                    );
                    const {
                        data: { results: histories },
                    } = await OrdersApiService.findByCustomerId(
                        orders.data.results.customer.id as number,
                    );
                    setCustomer(customer.data.results);
                    setHistories(histories);
                } catch {
                    message.error('Error receiving information');
                }
            }
        })();
    }, [id]);

    const onFinish = async ({ status }: Store): Promise<void> => {
        if (status) {
            try {
                await OrdersApiService.changeStatus(id as string, { status, force });
                const response = await OrdersApiService.getById(id as string);
                setOrderData(response.data);
                message.success('Edited successfully.');
            } catch (e) {
                message.error(
                    `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
                );
                const response: IValidationError = e.response?.data;
                if (response?.results) {
                    Object.keys(response.results).forEach((key: string) => {
                        message.error(`${response.results[key]}`);
                    });
                }
            }
        }
    };

    const onAddNote = async ({ description }: Store): Promise<void> => {
        if (description) {
            try {
                await OrdersApiService.addNote(id as string, description);
                const notes = await OrdersApiService.getNotes(id as string);
                setNotes(notes.data);
                onReset();
                message.success('Add successfully.');
            } catch (e) {
                message.error(
                    `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
                );
            }
        }
    };

    const showModalEditPrice = useCallback((title, item) => {
        setPreviewTitle(title);
        setItemEditPrice(item.price);
        setItemEditPriceID(item.id);
        setPreviewVisible(true);
    }, []);

    const onSavePriceItem = () => {
        const dto = {
            items: [
                {
                    id: itemEditPriceID,
                    price: itemEditPrice,
                },
            ],
        };
        try {
            //@ts-ignore
            OrdersApiService.editPriceItem(dto, id as string);
            message.success('Edited successfully.');
            setPreviewVisible(false);
            onShipmentUpdate();
        } catch (e) {
            message.error(
                `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
            );
            setPreviewVisible(false);
        }
    };

    const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };

    const data = orderData?.results;
    const metas = orderData?.metas;

    if (!data) {
        return <></>;
    }

    return (
        <>
            <Row gutter={20} className={s.row}>
                <BreadcrumbPage
                    listLinks={[
                        { name: 'Home', route: '/' },
                        { name: 'Orders', route: '/orders' },
                        { name: data?.identifier },
                    ]}
                />

                <Col xs={24} md={12} lg={8}>
                    <Card
                        title="Detail Order"
                        extra={
                            <HistoryOutlined
                                onClick={() => setHistoryDrawerVisibility(true)}
                                style={{ fontSize: 20 }}
                            />
                        }
                    >
                        <Descriptions column={1}>
                            <Descriptions.Item label="Customer Name">
                                {data.customer.name} {data.customer.family}{' '}
                                {`(${data.customer.id})`}
                            </Descriptions.Item>
                            <Descriptions.Item label="Order Date">
                                {moment(data.createdAt).format('jYYYY-jMM-jDD')}
                            </Descriptions.Item>
                            <Descriptions.Item label="Order ID">
                                {data?.identifier}
                            </Descriptions.Item>
                            <Descriptions.Item label="Payment Method">
                                {data?.paymentMethod}
                            </Descriptions.Item>
                            <Descriptions.Item label="Post Status" className={s.status}>
                                {data?.status && OrderStatusEnum[data!.status]}
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <br/>

                    <Card title="Status">
                        <Space align="center" direction="horizontal">
                            <Form
                                layout="inline"
                                onFinish={onFinish}
                                name="changeStatus"
                                className={s.inlineForm}
                                initialValues={{ status: data.status }}
                            >
                                <Form.Item name="force" label="Change Force">
                                    <Checkbox
                                        value={force}
                                        onChange={(e) =>
                                            e.target.checked ? setForce(1) : setForce(0)
                                        }
                                    />
                                </Form.Item>
                                <Form.Item name="status" label="Change status to">
                                    <Radio.Group>
                                        {Object.keys(OrderStatusEnum).map((key) => (
                                            <Radio
                                                key={key}
                                                style={radioStyle}
                                                value={key}
                                                disabled={!metas?.validTransitions?.includes(key)}
                                            >
                                                {OrderStatusEnum[key as keyof typeof OrderStatusEnum]}
                                            </Radio>
                                        ))}
                                    </Radio.Group>
                                </Form.Item>
                                <div className={s.buttonsRow}>
                                    <Button size="middle" type={'primary'} htmlType="submit">
                                        Update
                                    </Button>
                                </div>
                            </Form>
                        </Space>
                    </Card>

                    <br/>

                    <Card title="Summary">
                        <Descriptions column={1}>
                            <Descriptions.Item label="Total">
                                {data?.subtotal.toLocaleString()} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Payable">
                                {data?.grandTotal.toLocaleString()} Toman
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <br/>

                    <Card title="Balance" extra={balance?.balanceStatus === "CREDITOR" && (
                        <PlusOutlined
                            onClick={() => setRefundModalVisibility(true)}
                            style={{ fontSize: 20 }}
                        />
                    )}>
                        <Descriptions column={1}>
                            <Descriptions.Item label="Status">
                                {balance?.balanceStatus}
                            </Descriptions.Item>
                            <Descriptions.Item label="Remaining">
                                {balance?.balanceStatus === "DEBTOR" ? balance?.balanceAmount.toLocaleString() : 0} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Total payable">
                                {balance?.balanceStatus === "DEBTOR" ? balance?.balanceAmount.toLocaleString() : 0} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Balance">
                                {balance?.balanceStatus === "CREDITOR" || balance?.balanceStatus === "BALANCE" ? "" : "-"}
                                {balance?.balanceAmount.toLocaleString()} Toman
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <br/>

                    <Table
                        tableLayout="fixed"
                        title={() => (
                            <Typography.Text
                                style={{ paddingLeft: 8, fontWeight: 500, fontSize: 16 }}
                            >
                                Activity (Status)
                            </Typography.Text>
                        )}
                        dataSource={data.statusLogs}
                        pagination={false}
                        columns={[
                            {
                                title: 'User',
                                dataIndex: [],
                                key: 'id',
                                render: (log: Order['statusLogs'][0]) =>
                                    `${log.user?.name ?? 'System'} ${
                                        log.user?.family ?? 'Administrator'
                                    }`,
                            },
                            {
                                title: 'From',
                                dataIndex: 'statusFrom',
                                key: 'statusFrom',
                            },
                            {
                                title: 'To',
                                dataIndex: 'statusTo',
                                key: 'statusTo',
                            },
                            {
                                title: 'Date',
                                dataIndex: 'createdAt',
                                key: 'createdAt',
                                render: (date: string) =>
                                    moment(date).format('jYYYY-jMM-jDD HH:mm:ss'),
                            },
                        ]}
                    />
                </Col>

                <Col xs={24} md={12} lg={16}>
                    <Card
                        title="Shipping"
                        className={s.shippingCard}
                        extra={
                            <Button
                                type="primary"
                                size="small"
                                onClick={() => setAddressModalVisibility(true)}
                            >
                                Edit shipping address
                            </Button>
                        }
                    >
                        <Descriptions
                            title="Recipient Address"
                            layout="vertical"
                            column={6}
                        >
                            <Descriptions.Item label="Recipient Name" span={2}>
                                {data?.orderAddress?.name} {data?.orderAddress?.family}
                            </Descriptions.Item>
                            <Descriptions.Item label="National Code">
                                {data?.orderAddress?.nationalCode}
                            </Descriptions.Item>
                            <Descriptions.Item label="Phone number">
                                {data?.orderAddress?.phone}
                            </Descriptions.Item>
                            <Descriptions.Item label="State">
                                {data?.orderAddress?.city?.name}
                            </Descriptions.Item>
                            <Descriptions.Item label="City">
                                {data?.orderAddress?.district?.name}
                            </Descriptions.Item>
                            <Descriptions.Item label="Address" span={3}>
                                {data?.orderAddress?.fullAddress}
                            </Descriptions.Item>
                            <Descriptions.Item label="Plaque">
                                {data?.orderAddress?.number}
                            </Descriptions.Item>
                            <Descriptions.Item label="Unit">
                                {data?.orderAddress?.unit}
                            </Descriptions.Item>
                            <Descriptions.Item label="Postal Code">
                                {data?.orderAddress?.postalCode}
                            </Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <br/>

                    <Card title="Shipping Details">
                        <OrderShipments
                            order={data.identifier}
                            onShipmentUpdate={onShipmentUpdate}
                            data={shipments}
                            showModalEditPrice={showModalEditPrice}
                        />
                    </Card>

                    <br/>

                    <Card title="Notes">
                        <div className={s.notes}>
                            {notes?.results?.map((item) => (
                                <>
                                    <Card
                                        type="inner"
                                        size="small"
                                        key={item.id}
                                        title={`Written by: ${item.admin?.name} ${
                                            item.admin?.family
                                        } | ${moment(item.createdAt).format(
                                            'jYYYY-jMM-jDD HH:mm',
                                        )}`}
                                    >
                                        <Descriptions column={2}>
                                            <Descriptions.Item label="" span={2}>
                                                {item.description}
                                            </Descriptions.Item>
                                        </Descriptions>
                                    </Card>
                                    <br/>
                                </>
                            ))}
                        </div>
                        <Form
                            form={form}
                            layout="vertical"
                            onFinish={onAddNote}
                            name="addNote"
                            className={s.inlineForm}
                            initialValues={{ description: '' }}
                        >
                            <Form.Item name="description" label="Add note">
                                <TextArea rows={2}/>
                            </Form.Item>
                            <div className={s.buttonsRow}>
                                <Button size="middle" type={'primary'} htmlType="submit">
                                    Add
                                </Button>
                            </div>
                        </Form>
                    </Card>

                    <br/>

                    <Table
                        tableLayout="fixed"
                        title={() => (
                            <Typography.Text
                                style={{ paddingLeft: 8, fontWeight: 500, fontSize: 16 }}
                            >Transactions</Typography.Text>
                        )}
                        dataSource={[
                            ...data.orderDocument.transactions.map(t => ({ ...t, dtype: 'Order' })),
                            ...flatten(data.refundDocuments.map(rd => rd.transactions.map(t => ({
                                ...t,
                                dtype: 'Refund'
                            })))),
                        ]}
                        pagination={false}
                        columns={[
                            {
                                title: 'Date',
                                dataIndex: 'createdAt',
                                key: 'createdAt',
                                render: (date: string) => moment(date).format('jYYYY-jMM-jDD HH:mm:ss')
                            },
                            {
                                title: 'Gateway',
                                dataIndex: 'gateway',
                                key: 'gateway',
                            },
                            {
                                title: 'Status',
                                dataIndex: 'status',
                                key: 'status',
                            },
                            {
                                title: 'Amount',
                                dataIndex: [],
                                key: 'amount',
                                render: (data: ITransaction & { dtype: string }) => <span style={{
                                    color: data.dtype === 'Order' ? 'green' : 'red'
                                }}>{data.amount?.toLocaleString()}</span>
                            },
                            {
                                title: 'Type',
                                dataIndex: 'dtype',
                                key: 'dtype',
                            },
                            {
                                title: 'Reference',
                                dataIndex: 'trackingNumber',
                                key: 'trackingNumber'
                            },
                        ]}
                    />
                </Col>
            </Row>

            {customer && (
                <AddressesModal
                    activeId={data?.orderAddress?.id}
                    data={customer}
                    visible={addressModalVisible}
                    setVisible={setAddressModalVisibility}
                    onOk={onAddressUpdate}
                    orderId={data?.id}
                />
            )}
            <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={[
                    <Button key="back" onClick={() => setPreviewVisible(false)}>
                        Cancle
                    </Button>,
                    <Button key="submit" danger onClick={onSavePriceItem}>
                        Edit
                    </Button>,
                ]}
                onCancel={() => setPreviewVisible(false)}
            >
                <p>New Price:</p>
                <Input
                    value={itemEditPrice}
                    onChange={(e) => setItemEditPrice(e.target.value)}
                />
            </Modal>

            <Drawer
                title="Order history"
                height={640}
                placement="bottom"
                closable={true}
                visible={isHistoryDrawerVisible}
                onClose={() => setHistoryDrawerVisibility(false)}
                key={'history'}
            >
                {histories.map((history) => {
                    return (
                        <>
                            <Descriptions size={'small'} bordered>
                                <Descriptions.Item label="Identifier" span={1}>
                                    <Link
                                        href={'/orders/[id]/details'}
                                        as={`/orders/${history.id}/details`}
                                        passHref
                                    >
                                        <a target={'_blank'}>{history.identifier}</a>
                                    </Link>
                                </Descriptions.Item>
                                <Descriptions.Item label="Payment Method" span={1}>
                                    {history.paymentMethod}
                                </Descriptions.Item>
                                <Descriptions.Item label="Status" span={1}>
                                    {history.status}
                                </Descriptions.Item>
                                <Descriptions.Item label="Submit date" span={1}>
                                    {moment(history.createdAt).format('jYYYY-jMM-jDD')}
                                </Descriptions.Item>
                                <Descriptions.Item label="Subtotal">
                                    {history.subtotal.toLocaleString()} Toman
                                </Descriptions.Item>
                                <Descriptions.Item label="Grand Total">
                                    {history.grandTotal.toLocaleString()} Toman
                                </Descriptions.Item>
                                <Descriptions.Item label="Items count">
                                    {history.orderItems.length}
                                </Descriptions.Item>
                                <Descriptions.Item label="Address" span={4}>
                                    {history.orderAddress?.fullAddress}
                                </Descriptions.Item>
                            </Descriptions>

                            <Divider/>
                        </>
                    );
                })}
            </Drawer>

            <Modal
                title="Refund"
                visible={isRefundModalVisible}
                onOk={onRefundSubmitted}
                confirmLoading={isRefundSubmissionLoading}
                onCancel={onRefundModalCanceled}
            >
                <Form form={refund}>
                    <Form.Item name="tracking_number" label="Tracking number">
                        <Input/>
                    </Form.Item>
                    <Form.Item name="paid_at" label="Paid at">
                        <CPDatePicker/>
                    </Form.Item>
                    <Form.Item name="description" label="Description">
                        <Input.TextArea/>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};

export default Details;
