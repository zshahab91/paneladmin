import ShipmentModal from '@components/OrderPage/Details/ShipmentModal';
import Item from "@components/shipment/item";
import Order, { OrderShipment, ShipmentStatusEnum } from '@entities/order';
import OrderItem from '@entities/orderItems';
import { ImageProxy } from "@services/image";
import { Button, Card, Col, Descriptions, Divider, Row, Space, Typography } from 'antd';
import moment from 'moment-jalaali';
import React, { useCallback, useState } from 'react';
import s from './styles.module.scss';

moment.loadPersian();


interface Props {
    order?: string;
    data: OrderShipment[] | undefined;
    isStatusOnly?: boolean | undefined;
    showModalEditPrice: (title: string, item: OrderItem) => void;
    onShipmentUpdate: () => void;
}

const OrderShipments: React.FC<Props> = ({
                                             order,
                                             data,
                                             showModalEditPrice,
                                             onShipmentUpdate,
                                             isStatusOnly
                                         }) => {
    const [activeShipment, setActiveShipment] = useState<OrderShipment>();

    const onCancel = useCallback(() => {
        setActiveShipment(undefined);
    }, []);

    const openModal = (shipment: OrderShipment) => {
        setActiveShipment(shipment);
    };

    return (
        <>
            {data?.map((shipment, index) => {
                const grandtotal = shipment.grandTotal + shipment.orderItems.reduce((accu, item) => accu + item.grandTotal, 0);
                return (
                    <Card
                        title={<b
                            className={s.title}>Shipment {shipment.trackingCode} ({index + 1} of {data?.length})</b>}
                        key={shipment.id}
                        className={s.shipments}
                        type="inner"
                        extra={
                            <Button size="small" type="primary" onClick={(): void => openModal(shipment)}
                                    style={{ height: 30 }}>
                                Edit The Shipment
                            </Button>
                        }
                    >
                        <Descriptions
                            title="Recipient Address"
                            layout="horizontal"
                            column={3}
                        >
                            <Descriptions.Item label="Warehouse">
                                {'Tehran'}
                            </Descriptions.Item>
                            <Descriptions.Item label="Time Scope">
                                {moment(shipment.deliveryDate).format('jYYYY/jMM/jDD')}( {shipment?.period?.start} - {shipment?.period?.end} )
                            </Descriptions.Item>
                            <Descriptions.Item label="Shipment price">
                                {shipment.subTotal.toLocaleString()} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Shipment final price">
                                {shipment.grandTotal.toLocaleString()} Toman
                            </Descriptions.Item>
                            <Descriptions.Item label="Shipment Status">
                                {ShipmentStatusEnum[shipment.status] || shipment.status}
                            </Descriptions.Item>
                            <Descriptions.Item label="Grand total">
                                {grandtotal.toLocaleString()} Toman
                            </Descriptions.Item>
                        </Descriptions>
                        <Divider/>

                        {shipment.orderItems.map((oi, index) => {
                            return (
                                <React.Fragment key={oi.id}>
                                    <Item order={order} {...oi}/>
                                    {shipment.orderItems.length - 1 !== index && <Divider/>}
                                </React.Fragment>
                            )
                        })}
                    </Card>
                );
            })}
            <ShipmentModal
                activeShipment={activeShipment}
                onCancel={onCancel}
                onShipmentUpdate={onShipmentUpdate}
                isStatusOnly={isStatusOnly}
            />
        </>
    );
};

export default OrderShipments;
