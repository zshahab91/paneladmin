import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {request} from "@services/http/adapter";
import {AxiosResponse} from "axios";
import ShippingCategory from "@entities/shippingCategory";


export function useShippingCategories(query?: {}, pagination?: PaginationProps): IServiceDataHookWithPagination<ShippingCategory[]> {

    return useFetchingDataWithPagination<ShippingCategory>({
        url:"/admin/shipping-categories",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage
        }
    });
}


export function useShippingCategory(id: number): IServiceDataHook<ShippingCategory> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/shipping-categories/" + id)
    }

    return useFetchingDataByKey<ShippingCategory>(createRequest);
}
