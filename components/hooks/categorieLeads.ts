import {IServiceDataHookWithPagination, PaginationProps, useFetchingDataWithPagination} from "./utils";
import CategoryLead from "@entities/categoryLead";


interface QueryProps {
    id?: number;
    value?: string;
    category?: string;
}

export function useCategoryLeads(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<CategoryLead[]> {

    return useFetchingDataWithPagination<CategoryLead>({
        url: "/admin/category-leads",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[id]': query?.id ? `${query.id}` : undefined,
            'filter[value]': query?.value ? `${query.value}` : undefined,
            'filter[category.id]': query?.category ?? undefined,
        }
    });
}

