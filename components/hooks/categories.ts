import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import Category from "@entities/category";


interface QueryProps {
    leafChildrenOnly?: boolean;
    start?: string;
    end?: string;
    category?: string;
    title?:string;
    subtitle?:string;
    isLeaf?: boolean;
}


function findLeafCategories(categories: Category[]): Category[] {
    return categories.filter(value => {
        const filterData = categories.filter(item => item.parent && item.parent.id === value.id);
        return filterData.length === 0
    });
}


export function useCategories(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Category[]> {

    const {data, ...restFetchInfo} = useFetchingDataWithPagination<Category>({
        url: "/admin/categories",
        params: {
            page: pagination?.page,
            limit: 30,
            'filter[start][like]': query?.start ? `${query.start}` : undefined,
            'filter[end][like]': query?.end ? `${query.end}` : undefined,
            'filter[category][like]': query?.category ? `${query.category}` : undefined,
            'filter[title][like]': query?.title ? `${query.title}` : undefined,
            'filter[subtitle][like]': query?.subtitle ? `${query.subtitle}` : undefined,
            'filter[isLeaf]': query?.isLeaf ??  undefined,
        }
    });

    const filteredData = data && query?.leafChildrenOnly ? findLeafCategories(data) : data;
    return {...restFetchInfo, data: filteredData}
}


export function useCategory(id: number): IServiceDataHook<Category> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/categories/" + id)
    }

    return useFetchingDataByKey<Category>(createRequest);
}
