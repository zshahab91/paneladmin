import {
  IServiceDataHook,
  IServiceDataHookWithPagination,
  PaginationProps,
  useFetchingDataByKey,
  useFetchingDataWithPagination,
} from './utils';
import { AxiosResponse } from 'axios';
import { request } from '@services/http/adapter';
import Inventory from '@entities/inventories';

export interface InventoryQueryProps {
  id?: string
  // product?: number
  productId?: number
  productSlug?: string
  seller?: number, stock?: number
  isActive?: boolean
  hasDiscount?: boolean
}

export function useInventories(
  query?: InventoryQueryProps,
  pagination?: PaginationProps,
): IServiceDataHookWithPagination<Inventory[]> {
  return useFetchingDataWithPagination<Inventory>({
    url: '/admin/inventories',
    params: {
      page: pagination?.page,
      limit: pagination?.perPage,
      'filter[id]': query?.id ? `${query.id}` : undefined,
      'filter[variant.product.id]': query?.productId ?? undefined,
      'filter[variant.product.code][like]': query?.productSlug ?? undefined,
      'filter[seller.id]': query?.seller ?? undefined,
      'filter[stock]': query?.stock ?? undefined,
      'filter[isActive]': query?.isActive ?? undefined,
      'filter[hasDiscount]': query?.hasDiscount ?? undefined,
    },
  });
}

export function useInventory(id: number): IServiceDataHook<Inventory> {
  function createRequest(): Promise<AxiosResponse> {
    return request.get('/admin/inventories/' + id);
  }

  return useFetchingDataByKey<Inventory>(createRequest);
}
