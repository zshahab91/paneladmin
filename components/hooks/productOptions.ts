import ProductOption from '@entities/productOption';
import { request } from '@services/http/adapter';
import { AxiosResponse } from 'axios';
import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination,
} from './utils';

interface QueryProps {
    code?: string;
    name?: number;
}

export interface ColorsQueryProps {
    code?: string;
    value?: string;
}

export function useProductOptions(
    query?: QueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<ProductOption[]> {
    return useFetchingDataWithPagination<ProductOption>({
        url: '/admin/product/options',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ? `${query.code}` : undefined,
            'filter[name][like]': query?.name ? `${query.name}` : undefined,
        },
    });
}

export interface GuaranteesQueryProps {
    code?: string;
    value?: string;
}

export function useGuarantees(
    query?: GuaranteesQueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<ProductOption[]> {
    return useFetchingDataWithPagination<ProductOption>({
        url: '/admin/product/options/guarantee/values',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ?? undefined,
            'filter[value][like]': query?.value ?? undefined,
        }
    });
}

export interface ShoeSizesQueryProps {
    code?: string;
    value?: string;
}

export function useShoeSizes(
    query?: ShoeSizesQueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<ProductOption[]> {
    return useFetchingDataWithPagination<ProductOption>({
        url: '/admin/product/options/shoe-size/values',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ?? undefined,
            'filter[value][like]': query?.value ?? undefined,
        }
    });
}

export interface DressSizesQueryProps {
    code?: string;
    value?: string;
}

export function useDressSizes(
    query?: DressSizesQueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<ProductOption[]> {
    return useFetchingDataWithPagination<ProductOption>({
        url: '/admin/product/options/dress-size/values',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ?? undefined,
            'filter[value][like]': query?.value ?? undefined,
        }
    });
}

export interface BabyDressSizesQueryProps {
    code?: string;
    value?: string;
}

export function useBabyDressSizes(
    query?: BabyDressSizesQueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<ProductOption[]> {
    return useFetchingDataWithPagination<ProductOption>({
        url: '/admin/product/options/baby-dress-size/values',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ?? undefined,
            'filter[value][like]': query?.value ?? undefined,
        }
    });
}

export function useProductOption(id: number): IServiceDataHook<ProductOption> {
    function createRequest(): Promise<AxiosResponse> {
        return request.get('/admin/product/options/' + id);
    }

    return useFetchingDataByKey<ProductOption>(createRequest);
}

export function useColors(
    query?: ColorsQueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<ProductOption[]> {
    return useFetchingDataWithPagination<ProductOption>({
        url: '/admin/product/options/color/values',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ?? undefined,
            'filter[value][like]': query?.value ?? undefined,
        }
    });
}


// Singulars


export function useGuarantee(id: number): IServiceDataHook<ProductOption> {
    function createRequest(): Promise<AxiosResponse> {
        return request.get(`/admin/product/options/values/${id}`);
    }

    return useFetchingDataByKey<ProductOption>(createRequest);
}
