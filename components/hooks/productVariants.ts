import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import ProductVariant from "@entities/productVariant";


interface QueryProps {
    code?: string;
    position?: number;
    product?: number;
}

export function useProductVariants(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<ProductVariant[]> {

    return useFetchingDataWithPagination<ProductVariant>({
        url: "/admin/product/variants",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[code][like]': query?.code ? `${query.code}` : undefined,
            'filter[position]': query?.position ? `${query.position}` : undefined,
            'filter[product.id]': query?.product ? `${query.product}` : undefined
        }
    });
}


export function useProductVariant(id: number): IServiceDataHook<ProductVariant> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/product/variants/" + id)
    }

    return useFetchingDataByKey<ProductVariant>(createRequest);
}
