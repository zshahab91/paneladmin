import {
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataWithPagination
} from "./utils";

import Category from "@entities/category";

interface QueryProps {
    start?: string;
    end?: string;
    category?: string;
}

export function useCategoryDeiveries(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Category[]> {

    return useFetchingDataWithPagination<Category>({
        url: "/admin/category-deliveries",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[start][like]': query?.start ? `${query.start}` : undefined,
            'filter[end][like]': query?.end ? `${query.end}` : undefined,
            'filter[category.id]': query?.category ? `${query.category}` : undefined,
        }
    });
}



