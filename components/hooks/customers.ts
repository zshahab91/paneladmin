import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import Customer from "@entities/customer";

interface QueryProps {
    name?: string;
    family?: string;
    mobile?: string;
    nationalNumber?: string;
    email?:string;
    birthday?: string;
    gender?: string;
}
export function useCustomers(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Customer[]> {
    return useFetchingDataWithPagination<Customer>({
        url:"/admin/customers",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[name][like]': query?.name ? `${query.name}` : undefined,
            'filter[family][like]': query?.family ? `${query.family}` : undefined,
            'filter[mobile][like]': query?.mobile ? `${query.mobile}` : undefined,
            'filter[nationalNumber][like]': query?.nationalNumber ? `${query.nationalNumber}` : undefined,
            'filter[email]': query?.email ? query.email : undefined,
            'filter[birthday]': query?.birthday ? `${query.birthday}` : undefined,
            'filter[gender]': query?.gender ? `${query.gender}` : undefined,
        }
    });
}


export function useCustomer(id: number): IServiceDataHook<Customer> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/customers/" + id)
    }

    return useFetchingDataByKey<Customer>(createRequest);
}
