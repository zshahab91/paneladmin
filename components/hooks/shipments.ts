import { IShipment } from '@entities/shipment';
import { request } from '@services/http/adapter';
import { AxiosResponse } from 'axios';
import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination,
} from './utils';

export interface QueryProps {
    customer?: string;
    identifier?: string;
    inventory?: string;
    number?: string;
    product?: string;
    seller?: string;
    shipment?: string;
    city?: string;
    paymentMethod?: string;
    province?: string;
    shippingCategory?: string;
    status?: string;
    sort?: string[];
    deliveryDateFrom?: string;
    deliveryDateTo?: string;
}

export function useShipments(
    query?: QueryProps,
    pagination?: PaginationProps,
): IServiceDataHookWithPagination<IShipment[]> {
    return useFetchingDataWithPagination<IShipment>({
        url: '/admin/order-shipments',
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[id]': query?.shipment ?? undefined,
            'filter[status]': query?.status ?? undefined,
            'filter[order.identifier]': query?.identifier ?? undefined,
            'filter[order.customer.nationalNumber]': query?.number ?? undefined,
            'filter[orderItems.inventory.variant.product.seller.id]':
                query?.seller ?? undefined,
            'filter[orderItems.inventory.variant.product.id]':
                query?.product ?? undefined,
            'filter[orderItems.inventory.id]': query?.inventory ?? undefined,
            'filter[orderItems.inventory.variant.product.shippingCategory.name]':
            query?.shippingCategory,
            'filter[order.paymentMethod]': query?.paymentMethod ?? undefined,
            'filter[order.orderAddress.city.id]': query?.city ?? undefined,
            'filter[order.orderAddress.city.province.id]':
                query?.province ?? undefined,
            'filter[deliveryDate][gte]': query?.deliveryDateFrom ?? undefined,
            'filter[deliveryDate][lte]': query?.deliveryDateTo ?? undefined,
            sort: query?.sort ? [...query.sort] : ['-id'],
        },
    });
}

export function useShipment(id: number): IServiceDataHook<IShipment> {
    function createRequest(): Promise<AxiosResponse> {
        return request.get('/admin/order-shipments/' + id);
    }

    return useFetchingDataByKey<IShipment>(createRequest);
}
