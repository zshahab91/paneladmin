import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import ShippingPeriod from "@entities/shippingPeriod";


export function useShippingPeriods(query?: {}, pagination?: PaginationProps): IServiceDataHookWithPagination<ShippingPeriod[]> {

    return useFetchingDataWithPagination<ShippingPeriod>({
        url: "/admin/shipping-periods",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage
        }
    });
}


export function useShippingPeriod(id: number): IServiceDataHook<ShippingPeriod> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/shipping-periods/" + id)
    }

    return useFetchingDataByKey<ShippingPeriod>(createRequest);
}
