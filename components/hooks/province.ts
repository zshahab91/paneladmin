import {
  IServiceDataHookWithPagination,
  PaginationProps,
  useFetchingDataWithPagination,
} from './utils';
import { Province } from '@entities/zone';

interface QueryProps {
  id?: string;
  name?: string;
}

export function useProvinces(
  query?: QueryProps,
  pagination?: PaginationProps,
): IServiceDataHookWithPagination<Province[]> {
  return useFetchingDataWithPagination<Province>({
    url: '/places/provinces',
    params: {
      page: pagination?.page,
      limit: pagination?.perPage,
      'filter[id]': query?.id ? `%${query.id}%` : undefined,
      'filter[name][like]': query?.name ?? undefined,
    },
  });
}
