import {
  IServiceDataHookWithPagination,
  PaginationProps,
  useFetchingDataWithPagination,
} from './utils';
import { City } from '@entities/zone';

interface QueryProps {
  id?: string;
  name?: string;
  customerAddressesId?: string;
}

export function useCities(
  query?: QueryProps,
  pagination?: PaginationProps,
): IServiceDataHookWithPagination<City[]> {
  return useFetchingDataWithPagination<City>({
    url: '/places/cities',
    params: {
      page: pagination?.page,
      limit: pagination?.perPage,
      'filter[city.id]': query?.id ? `%${query.id}%` : undefined,
      'filter[name]': query?.name ?? undefined,
      'filter[customerAddresses.id]': query?.customerAddressesId
        ? `%${query.customerAddressesId}%`
        : undefined,
    },
  });
}
