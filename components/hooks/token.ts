import {useEffect, useState} from "react";
import GlobalEvents from "@services/GlobalEvents";
import {TokenProps} from "@services/AuthService/models";
import AuthService from "@services/AuthService";


export function useToken() {
    const [token, setToken] = useState(AuthService.getTokenInfo());

    useEffect(() => {
        const handleTokenChange = (value: TokenProps) => setToken(value);
        GlobalEvents.addEventListener('token', handleTokenChange);
        return () => GlobalEvents.removeEventListener('token', handleTokenChange);
    }, []);

    return token;
}
