import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import CategoryCommission from "@entities/categoryCommission";


interface QueryProps {
    fee?: { gte: number, lte: number };
    categoryId?: number;
}

export function useCategoryCommissions(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<CategoryCommission[]> {

    return useFetchingDataWithPagination<CategoryCommission>({
        url: "/admin/category-commissions",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[fee][gte]': query?.fee?.gte === undefined ? undefined : query.fee.gte ,
            'filter[fee][lte]': query?.fee?.lte === undefined ? undefined : query.fee.lte ,
            "filter[category.id]": query?.categoryId
        }
    });
}


export function useCategoryCommission(id: number): IServiceDataHook<CategoryCommission> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/category-commissions/" + id)
    }

    return useFetchingDataByKey<CategoryCommission>(createRequest);
}
