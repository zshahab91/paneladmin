import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import Product from "@entities/product";


interface QueryProps {
    id?: string;
    title?: string;
    subtitle?: string;
    code?: string;
    brand?: string;
    category?: string;
    option?: string;
    status?: string;
    isActive?: boolean;
    seller?: number;
}

export function useProducts(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Product[]> {

    return useFetchingDataWithPagination<Product>({
        url: "/admin/products",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[id]': query?.id ? `${query.id}` : undefined,
            'filter[code][like]': query?.code ? `${query.code}` : undefined,
            'filter[status]': query?.status ? `${query.status}` : undefined,
            'filter[title][like]': query?.title ? `${query.title}` : undefined,
            'filter[subtitle][like]': query?.subtitle ? `${query.subtitle}` : undefined,
            'filter[brand.id]': query?.brand ?? undefined,
            'filter[options.id]': query?.option ?? undefined,
            'filter[category.id]': query?.category ?? undefined,
            'filter[isActive]': query?.isActive ?? undefined,
            'filter[productVariants.inventories.seller.id]': query?.seller ?? undefined,
        }
    });
}


export function useProduct(id: number): IServiceDataHook<Product> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/products/" + id)
    }

    return useFetchingDataByKey<Product>(createRequest);
}
