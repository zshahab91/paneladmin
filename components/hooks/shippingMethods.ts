import {request} from "@services/http/adapter";
import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import ShippingMethod from "@entities/shippingMethod";


export function useShippingMethods(query?: {}, pagination?: PaginationProps): IServiceDataHookWithPagination<ShippingMethod[]> {

    return useFetchingDataWithPagination<ShippingMethod>({
        url:"/admin/shipping-methods",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage
        }
    });
}


export function useShippingMethod(id: number): IServiceDataHook<ShippingMethod> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/shipping-methods/" + id)
    }

    return useFetchingDataByKey<ShippingMethod>(createRequest);
}
