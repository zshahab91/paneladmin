import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import Brand from "@entities/brand";

interface QueryProps {
    title?: string;
    subtitle?: string;
    code?: string;
    metaDescription?: string;
}

export function useBrands(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Brand[]> {

    return useFetchingDataWithPagination<Brand>({
        url: "/admin/brands",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[title][like]': query?.title ? `${query.title}` : undefined,
            'filter[code][like]': query?.code ? `${query.code}` : undefined,
            'filter[subtitle][like]': query?.subtitle ? `${query.subtitle}` : undefined,
            'filter[metaDescription][like]': query?.metaDescription ? `${query.metaDescription}` : undefined,
        }
    });
}


export function useBrand(id: number): IServiceDataHook<Brand> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/brands/" + id)
    }

    return useFetchingDataByKey<Brand>(createRequest);
}
