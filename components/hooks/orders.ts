import {request} from "@services/http/adapter";
import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import Order from "@entities/order";


interface QueryProps {
    status?: string;
    identifier?: string;
    subtotal?: string;
    grandTotal?: string;
    paymentMethod?: string;
    paymentStatus?: string;
    nationalNumber?: string;
    seller?: string;
    customer?: string;
    province?: string;
    city?: string;
    createdAtFrom?: string;
    createdAtTo?: string;
    balanceStatus?: string;
}

export function useOrders(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Order[]> {
    return useFetchingDataWithPagination<Order>({
        url:"/admin/orders",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[orderItems.inventory.seller.id]': query?.seller ? `${query.seller}` : undefined,
            'filter[status][like]': query?.status ? `${query.status}` : undefined,
            'filter[identifier][like]': query?.identifier ? `${query.identifier}` : undefined,
            'filter[paymentMethod][like]': query?.paymentMethod ? `${query.paymentMethod}` : undefined,
            'filter[paidAt]': query?.paymentStatus === 'PENDING' ? 'null' : undefined,
            'filter[paidAt][neq]': query?.paymentStatus === 'PAID' ? 'null' : undefined,
            'filter[customer.nationalNumber][like]': query?.nationalNumber ? `${query.nationalNumber}` : undefined,
            'filter[customer.id]': query?.customer ? `${query.customer}` : undefined,
            'filter[orderAddress.city.province.id]': query?.province ? `${query.province}` : undefined,
            'filter[orderAddress.city.id]': query?.city ? `${query.city}` : undefined,
            'filter[createdAt][gte]': query?.createdAtFrom ?? undefined,
            'filter[createdAt][lte]': query?.createdAtTo ?? undefined,
            'filter[balanceStatus]': query?.balanceStatus ?? undefined,
        }
    });
}


export function useOrder(id:number): IServiceDataHook<Order> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/orders/" + id)
    }

    return useFetchingDataByKey<Order>(createRequest);
}
