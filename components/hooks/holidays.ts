import {
    IServiceDataHook,
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataByKey,
    useFetchingDataWithPagination
} from "./utils";
import {AxiosResponse} from "axios";
import {request} from "@services/http/adapter";
import Holiday from "@entities/holiday";

interface QueryProps {
    title?: string;
    seller?: string;
    date?: string;
    supply?: true;
}
export function useHolidays(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<Holiday[]> {

    return useFetchingDataWithPagination<Holiday>({
        url:"/admin/holidays",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[title][like]': query?.title ? `${query.title}` : undefined,
            'filter[seller.id]': query?.seller ?? undefined,
            'filter[date][like]': query?.date ? `${query.date}` : undefined,
            'filter[supply]': query?.supply ? query.supply : undefined,
        }
    });
}



export function useHoliday(id:number): IServiceDataHook<Holiday> {

    function createRequest(): Promise<AxiosResponse> {
        return request.get("/admin/holidays/" + id)
    }

    return useFetchingDataByKey<Holiday>(createRequest);
}
