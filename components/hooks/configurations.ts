import {IServiceDataHookWithPagination, useFetchingDataWithPagination} from "./utils";
import Configuration from "@entities/configurations";


export function useConfigurations(): IServiceDataHookWithPagination<Configuration[]> {

    return useFetchingDataWithPagination<Configuration>({
        url: "/admin/configurations"
    });
}


export function usePaymentOnlineAdapters(): IServiceDataHookWithPagination<string[]> {

    return useFetchingDataWithPagination<string>({
        url: "/admin/payments/gateways/online"
    });
}
