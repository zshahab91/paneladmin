import {
    IServiceDataHookWithPagination,
    PaginationProps,
    useFetchingDataWithPagination
} from "./utils";
import {IAdmin} from "@entities/admin";

interface QueryProps {
    name?: string;
    family?: string;
    mobile?: string;
    email?:string;
}
export function useAdmins(query?: QueryProps, pagination?: PaginationProps): IServiceDataHookWithPagination<IAdmin[]> {
    return useFetchingDataWithPagination<IAdmin>({
        url:"/admin/admins",
        params: {
            page: pagination?.page,
            limit: pagination?.perPage,
            'filter[name][like]': query?.name ? `${query.name}` : undefined,
            'filter[family][like]': query?.family ? `${query.family}` : undefined,
            'filter[mobile][like]': query?.mobile ? `${query.mobile}` : undefined,
            'filter[email]': query?.email ? query.email : undefined,

        }
    });
}



