import {useEffect, useState} from "react";
import {AxiosRequestConfig, AxiosResponse} from "axios";
import {request} from "@services/http/adapter";


export interface IServiceDataHook<TData> {
    data?: TData;
    isLoading: boolean;
}

export interface IServiceDataHookWithPagination<TData> extends IServiceDataHook<TData> {
    totalItems?: number;
    perPage?: number
}

export interface PaginationProps {
    page?: number
    perPage?: number;
}


export function useFetchingDataWithPagination<TData, TQuery = {}>(requestInfo: AxiosRequestConfig): IServiceDataHookWithPagination<TData[]> {


    const [data, setData] = useState<TData[] | undefined>(undefined);
    const [isLoading, setIsLoading] = useState(false);
    const [totalItems, setTotalItems] = useState<number | undefined>();
    const [perPage, setPerPage] = useState<number | undefined>();


    const fetchData = (): void => {
        setIsLoading(true);

        request.request(requestInfo)
            .then(({data: {results, succeed, metas}}) => {
                if (succeed) {
                    setData(results);
                    setTotalItems(metas.totalItems)
                    setPerPage(metas.perPage)
                }

            }).catch(console.error)
            .finally(() => setIsLoading(false))
    }

    const keyToRefreshData = requestInfo.params && Object.entries(requestInfo.params)
        .map(([key, value]) => key + ':' + value)
        .join(',');
    useEffect(() => fetchData(), [keyToRefreshData]);

    return {data, isLoading, totalItems, perPage}
}


export function useFetchingDataByKey<TData>(createRequest: () => Promise<AxiosResponse>): IServiceDataHook<TData> {

    const [data, setData] = useState<TData | undefined>(undefined);
    const [isLoading, setIsLoading] = useState(false);

    const fetchData = (): void => {
        setIsLoading(true);

        createRequest()
            .then(({data: {results, succeed}}) => {
                if (succeed) {
                    setData(results);
                }

            }).catch(console.error)
            .finally(() => setIsLoading(false))
    }

    useEffect(() => fetchData(), []);

    return {data, isLoading}
}
