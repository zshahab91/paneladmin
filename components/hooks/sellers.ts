import {
  IServiceDataHook,
  IServiceDataHookWithPagination,
  PaginationProps,
  useFetchingDataByKey,
  useFetchingDataWithPagination,
} from './utils';
import { AxiosResponse } from 'axios';
import { request } from '@services/http/adapter';
import Seller from '@entities/seller';

interface QueryProps {
  identifier?: string;
  name?: string;
  username?: string;
  nationalIdentifier?: string;
  nationalNumber?: string;
}

export function useSellers(
  query?: QueryProps,
  pagination?: PaginationProps,
): IServiceDataHookWithPagination<Seller[]> {
  return useFetchingDataWithPagination<Seller>({
    url: '/admin/sellers',
    params: {
      page: pagination?.page,
      limit: pagination?.perPage,
      'filter[identifier]': query?.identifier
        ? `${query.identifier}`
        : undefined,
      'filter[name][like]': query?.name ? `${query.name}` : undefined,
      'filter[username][like]': query?.username
        ? `${query.username}`
        : undefined,
      'filter[nationalNumber]': query?.nationalNumber
        ? `${query.nationalNumber}`
        : undefined,
      'filter[nationalIdentifier]': query?.nationalIdentifier
        ? `${query.nationalIdentifier}`
        : undefined,
    },
  });
}

export function useSeller(id: number): IServiceDataHook<Seller> {
  function createRequest(): Promise<AxiosResponse> {
    return request.get('/admin/sellers/' + id);
  }

  return useFetchingDataByKey<Seller>(createRequest);
}
