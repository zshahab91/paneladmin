import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import CategoryDeliveryForm from '@components/CategoryDeliveriesPage/Form';
import CategoryDeliveryService from '@services/CategoryDeliveryService';
import { IValidationError } from '@entities/validation.error';
import { useRouter } from 'next/router';
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    setLoading(true);

    try {
      await CategoryDeliveryService.add({ ...values, category: values.category?.id });
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/category-deliveries`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <>
        <Card
          title="Add Category Submission Schedule"
          extra={
            <Link href="/category-deliveries">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
            <CategoryDeliveryForm
              loading={loading}
              form={form}
              onFinish={onFinish}
            />
          </FormAlignment>
        </Card>
    </>
  );
};

export default Add;
