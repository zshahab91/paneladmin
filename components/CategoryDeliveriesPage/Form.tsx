import React from 'react';
import { Button, Form, Space, Input } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import CategorySelector from '@components/CP/CategorySelector';



interface FormProps {
  onFinish: (values: Store) => Promise<void>;
  form: FormInstance;
  loading: boolean;
}

const CategoryDeliveryForm: React.FC<FormProps> = (props) => {
  const { onFinish, form, loading } = props;

  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item
        name="category"
        label="Category"
        rules={[{ required: true }]}
      >
      <CategorySelector isLeaf={true}/>
      </Form.Item>
      <Form.Item
        name="start"
        label="Sending intervals from"
        rules={[{ required: true }]}
      >
        <Input placeholder="1" type="number" />
      </Form.Item>
      <Form.Item
        name="end"
        label="Send up intervals"
        rules={[{ required: true }]}
      >
        <Input placeholder="10" type="number" />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={loading}>
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default CategoryDeliveryForm;
