import BreadcrumbPage from '@components/Breadcrumb';
import CategoryDeliveryForm from '@components/CategoryDeliveriesPage/Form';
import FormAlignment from '@components/CP/FormAlignment';
import { IValidationError } from '@entities/validation.error';
import CategoryDeliveryService from '@services/CategoryDeliveryService';
import { Card, Form, message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useEffect, useState } from 'react';

const Edit: React.FC = () => {
    const router = useRouter();
    const { id } = router.query;
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const [titlePage, setTitlePage] = useState('');

    useEffect(() => {
        (async () => {
            if (id) {
                try {
                    const categoryDelivery = await CategoryDeliveryService.getById(
                        id as string,
                    );

                    if (categoryDelivery?.data) {
                        const { category, start, end } = categoryDelivery.data.results;
                        //@ts-ignore
                        setTitlePage(category?.title);
                        form.setFieldsValue({ category, start, end });
                    }
                } catch {
                    message.error('Error receiving information');
                }
            }
        })();
    }, [id]);

    const onFinish = useCallback(
        async (values: Store): Promise<void> => {
            setLoading(true);

            try {
                await CategoryDeliveryService.edit(
                    { ...values, category: values.category?.id },
                    id as string,
                );
                message.success('Edited successfully.');
                setTimeout(() => {
                    router.push(`/category-deliveries`);
                }, 500);
            } catch (e) {
                const response: IValidationError = e.response?.data;
                if (response?.results) {
                    const formErrors = Object.keys(response.results).map(
                        (key: string) => ({
                            name: key,
                            errors: response.results[key],
                        }),
                    );
                    form.setFields(formErrors);
                }
                message.error(response?.message || 'Server error!');
            }

            setLoading(false);
        },
        [id],
    );

    return (
        <>
            <BreadcrumbPage
                listLinks={[
                    { name: 'Home', route: '/' },
                    { name: 'Category Deliveries', route: '/category-deliveries' },
                    { name: titlePage },
                ]}
            />
            <Card
                title="Edit Send Categories"
                extra={
                    <Link href="/category-deliveries">
                        <a>View All </a>
                    </Link>
                }
            >
                <FormAlignment>
                    <CategoryDeliveryForm
                        loading={loading}
                        form={form}
                        onFinish={onFinish}
                    />
                </FormAlignment>
            </Card>
        </>
    );
};

export default Edit;
