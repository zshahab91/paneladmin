import BreadcrumbPage from '@components/Breadcrumb';
import FormAlignment from "@components/CP/FormAlignment";
import GuaranteeForm from '@components/GuaranteesPage/Form';
import { Guarantee } from "@entities/productOption";
import { IValidationError } from '@entities/validation.error';
import GuaranteeService from "@services/Guarantees/guarantees.api.service";
import { Card, Form, message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useEffect, useState } from 'react';
import slugify from 'slugify';

const Edit: React.FC = () => {
    const router = useRouter();
    const { id: idString } = router.query;
    const id = Number(idString as string);
    const [form] = Form.useForm();
    const [title, setTitle] = useState<string>('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!router.query.id) {
            return;
        }

        (async () => {
            const { data: {results: guarantee} } = await GuaranteeService.show(router.query.id as unknown as number)
            setTitle(guarantee?.value as string);
            form.setFieldsValue(guarantee);
        })();
    }, [router.query.id])

    const onFinish = useCallback(
        async (values: Store): Promise<void> => {
            setLoading(true);
            const dto: Partial<Guarantee> = {
                ...values,
                code: slugify(values.code),
            };

            try {
                await GuaranteeService.edit(id as number, dto);
                message.success('Edited successfully.');
                setTimeout(() => {
                    router.push(`/guarantees`);
                }, 500);
            } catch (e) {
                const response: IValidationError = e.response?.data;
                message.error(response?.message || 'Server error!');
            }

            setLoading(false);
        },
        [id],
    );

    return (
        <>
            <BreadcrumbPage
                listLinks={[
                    { name: 'Home', route: '/' },
                    { name: 'Guarantees', route: '/guarantees' },
                    { name: title },
                ]}
            />
            <Card
                title="Update Guarantee"
                extra={
                    <Link href="/guarantees">
                        <a>View All</a>
                    </Link>
                }
            >
                <FormAlignment>
                    <GuaranteeForm loading={loading} form={form} onFinish={onFinish}/>
                </FormAlignment>
            </Card>
        </>
    );
};

export default Edit;
