import BabyDressSizesForm from "@components/BabyDressSizesPage/Form";
import BreadcrumbPage from '@components/Breadcrumb';
import FormAlignment from "@components/CP/FormAlignment";
import { DressSize } from "@entities/productOption";
import { IValidationError } from '@entities/validation.error';
import BabyDressSizesService from "@services/Sizes/baby.dress.sizes.api.service";
import { Card, Form, message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useEffect, useState } from 'react';
import slugify from 'slugify';

const Edit: React.FC = () => {
    const router = useRouter();
    const { id: idString } = router.query;
    const id = Number(idString as string);
    const [form] = Form.useForm();
    const [title, setTitle] = useState<string>('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!router.query.id) {
            return;
        }

        (async () => {
            const { data: { results: size } } = await BabyDressSizesService.show(router.query.id as unknown as number)
            setTitle(size?.value as string);
            form.setFieldsValue(size);
        })();
    }, [router.query.id])

    const onFinish = useCallback(
        async (values: Store): Promise<void> => {
            setLoading(true);
            const dto: Partial<DressSize> = {
                ...values,
                code: slugify(values.code),
            };

            try {
                await BabyDressSizesService.edit(id as number, dto);
                message.success('Edited successfully.');
                setTimeout(() => {
                    router.push(`/baby-dress-sizes`);
                }, 500);
            } catch (e) {
                const response: IValidationError = e.response?.data;
                message.error(response?.message || 'Server error!');
            }

            setLoading(false);
        },
        [id],
    );

    return (
        <>
            <BreadcrumbPage
                listLinks={[
                    { name: 'Home', route: '/' },
                    { name: 'Baby Dress sizes', route: '/baby-dress-sizes' },
                    { name: title },
                ]}
            />
            <Card
                title="Update Baby Dress size"
                extra={
                    <Link href="/baby-dress-sizes">
                        <a>View All</a>
                    </Link>
                }
            >
                <FormAlignment>
                    <BabyDressSizesForm loading={loading} form={form} onFinish={onFinish}/>
                </FormAlignment>
            </Card>
        </>
    );
};

export default Edit;
