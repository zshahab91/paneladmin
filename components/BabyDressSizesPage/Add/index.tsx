import BabyDressSizesForm from "@components/BabyDressSizesPage/Form";
import FormAlignment from "@components/CP/FormAlignment";
import DressSizesForm from '@components/DressSizesPage/Form';
import { Guarantee } from "@entities/productOption";
import { IValidationError } from "@entities/validation.error";
import BabyDressSizesService from "@services/Sizes/baby.dress.sizes.api.service";
import DressSizesService from "@services/Sizes/dress.sizes.api.service";
import { Card, Form, message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useState } from 'react';
import slugify from 'slugify';

const Add: React.FC = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const router = useRouter();
    const onFinish = useCallback(async (values: Store): Promise<void> => {
        setLoading(true);

        const dto: Partial<Guarantee> = {
            ...values,
            code: slugify(values.code),
        };

        try {
            await BabyDressSizesService.add(dto);
            message.success('Successfully added.');
            setTimeout(() => {
                router.push(`/baby-dress-sizes`);
            }, 500);
        } catch (e) {
            const response: IValidationError = e.response?.data;
            message.error(response?.message || 'Server error!');
        }

        setLoading(false);
    }, []);

    return (
        <Card
            title="Add Baby Dress size"
            extra={
                <Link href="/baby-dress-sizes">
                    <a>View All</a>
                </Link>
            }
        >
            <FormAlignment>
                <BabyDressSizesForm loading={loading} form={form} onFinish={onFinish}/>
            </FormAlignment>
        </Card>
    );
};

export default Add;
