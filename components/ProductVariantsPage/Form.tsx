import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Space, Select } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import { Nullable, IPaginatedResponse } from '@entities/paginated.response';
import Product from '@entities/product';
import ProductsService from '@services/ProductServices/Product.api.service';
import _ from 'lodash';
import ProductOption from '@entities/productOption';
import { EditProductVariant } from '@entities/productVariant';
import ProductOptionValue from '@entities/productOptionValue';

interface FormProps {
  onFinish: (values: Store) => Promise<void>;
  form: FormInstance;
  loading: boolean;
  editable?: boolean;
  productVariant?: EditProductVariant

}

const ProductVariantForm: React.FC<FormProps> = (props) => {

  const { onFinish, form, loading, editable, productVariant } = props;
  const [products, setProducts] = useState<IPaginatedResponse<Product[]>>();
  const [options, setOptions] = useState<ProductOption[]>();
  const [optionValues, setOptionValues] = useState<ProductOptionValue[]>();

  useEffect(() => {
    (async () => {
      const [
        products
      ] = await Promise.all([
        ProductsService.getList()
      ]);
      setProducts(products.data);
      if (editable && productVariant !== undefined) {
        setOptions(productVariant.product?.options);
        const values = productVariant.optionValues;
        _.forEach(values, function (item) {
          //@ts-ignore
          const idOption = item.option.id;
          const optionsCheck = productVariant.product?.options;
          const filterDataOptions = _.filter(optionsCheck, function (itm) { return itm.id === idOption });
          //@ts-ignore
          if (filterDataOptions.length !== 0) { setOptionValues(filterDataOptions[0].values) }

        });
      }

    })();
  }, []);
  //@ts-ignore
  const getProduct = async (id) => {
    const product = await ProductsService.getById(id);
    //@ts-ignore
    setOptions(product.data.results.options);
    form.setFieldsValue({ options: [] })

  }

  //@ts-ignore
  const getOption = (id) => {
    const option = _.filter(options, (option) => { return option.id === id });
    //@ts-ignore
    setOptionValues(option[0].values);
    form.setFieldsValue({ optionValues: [] })
  }
  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item name="code" label="Code" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="position" label="Position" rules={[{ required: true }]}>
        <Input type="number" step="0.1" min={0} placeholder="0.0" />
      </Form.Item>
      <Form.Item name="product" label="Products" rules={[{ required: true }]}>
        <Select
          placeholder="Select Product"
          allowClear
          onChange={getProduct}>
          {products?.results.map((item) => (
            //@ts-ignore
            <Select.Option key={item.id} value={item.id}>
              {item.title}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="options" label="Options" rules={[{ required: true }]}>
        <Select
          placeholder="Select Option"
          allowClear
          disabled={options?.length === 0 || options === undefined}
          onChange={getOption}>
          {options?.map((item) => (
            //@ts-ignore
            <Select.Option key={item.id} value={item.id}>
              {item.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="optionValues" label="Values" rules={[{ required: true }]}>
        <Select
          placeholder="Select Values"
          allowClear
          mode="multiple"
          disabled={optionValues === undefined || optionValues?.length === 0}>
          {optionValues?.map((item) => (
            //@ts-ignore
            <Select.Option key={item.id} value={item.id}>
              {item.value}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={loading}>
            Save
           </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default ProductVariantForm;
