import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import { Form, Card, message } from 'antd';
import ProductVariantForm from '@components/ProductVariantsPage/Form';
import { EditProductVariant } from '@entities/productVariant';
import ProductVariantService from '@services/ProductServices/ProductVariant.api.service';
import s from './styles.module.scss';
import Link from 'next/link';
import BreadcrumbPage from '@components/Breadcrumb';
import {IValidationError} from "@entities/validation.error";
import slugify from 'slugify';
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [loading, setLoading] = useState(false);
  const [productVariant, setProductVariant] = useState<EditProductVariant>();
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const productVariant = await ProductVariantService.getById(
            id as string,
          );

          if (productVariant?.data) {
            const {
              code,
              position,
              optionValues,
              product,
            } = productVariant.data.results;
            setTitlePage(code);

            setProductVariant(productVariant.data.results);
            form.setFieldsValue({
              code: code,
              position: position,
              optionValues:
                optionValues.length !== 0 &&
                optionValues.map((item) => item.id),
              //@ts-ignore
              options: optionValues.length !== 0 && optionValues[0]?.option.id,
              product: product.id,
            });
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = async (values: Store): Promise<void> => {
    const { position, product, optionValues, code } = values;
    setLoading(true);

    const dto: EditProductVariant = {
      position: parseInt(position),
      product: product,
      optionValues: optionValues,
      code: slugify(code)
    };

    try {
      await ProductVariantService.edit(dto, id as string);
      message.success('Edited successfully.');
      setTimeout(() => {
        router.push(`/product-variants`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  };
  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Product Variants', route: '/product-variants' },
          { name: titlePage },
        ]}
      />

      {productVariant && (
          <Card
            className={s.row}
            title="Details OF Product Variant"
            extra={
              <Link href="/product-variants">
                <a>View All </a>
              </Link>
            }
          >
            <FormAlignment>
              <ProductVariantForm
                loading={loading}
                form={form}
                editable={true}
                onFinish={onFinish}
                productVariant={productVariant}
              />
            </FormAlignment>
          </Card>
      )}
    </>
  );
};

export default Edit;
