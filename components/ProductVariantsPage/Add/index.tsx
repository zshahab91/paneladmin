import React, { useState } from 'react';
import Link from 'next/link';
import { Store } from 'rc-field-form/es/interface';
import { Form, Card, message } from 'antd';
import ProductVariantForm from '@components/ProductVariantsPage/Form';
import { AddProductVariant } from '@entities/productVariant';
import ProductVariantService from '@services/ProductServices/ProductVariant.api.service';
import s from './styles.module.scss';
import { useRouter } from 'next/router';
import { IValidationError } from '@entities/validation.error';
import slugify from 'slugify';
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = async (values: Store): Promise<void> => {
    const { position, product, optionValues, code } = values;
    setLoading(true);

    const dto: AddProductVariant = {
      position: parseInt(position),
      product: product,
      optionValues: optionValues,
      code: slugify(code)
    };

    try {
      await ProductVariantService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/product-variants`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  };
  return (
    <Card
      className={s.row}
      title="Add Product Variant"
      extra={
        <Link href="/product-variants">
          <a>View All </a>
        </Link>
      }
    >
      <FormAlignment>
        <ProductVariantForm
          loading={loading}
          form={form}
          onFinish={onFinish}
          editable={false}
        />
      </FormAlignment>
    </Card>
  );
};

export default Add;
