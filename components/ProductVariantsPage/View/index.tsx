import React, { useEffect, useState } from 'react';
import { Row, Col, Card, message, Descriptions } from 'antd';
import ProductVariantService from '@services/ProductServices/ProductVariant.api.service';
import { useRouter } from 'next/router';
import Link from 'next/link';
import s from './styles.module.scss';
import ProductVariant from '@entities/productVariant';
import BreadcrumbPage from '@components/Breadcrumb';

const View: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [productVariant, setProductVariant] = useState<ProductVariant>();
  const [titlePage, setTitlePage] = useState('');
  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const product = await ProductVariantService.getById(id as string);

          if (product?.data) {
            setProductVariant(product.data.results);
            setTitlePage(product?.data.results.code);
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  return (
    <Row>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Product Variants', route: '/product-variants' },
          { name: titlePage },
        ]}
      />

      <Col xs={24} md={12} lg={12} xl={12}>
        <Card
          title="General Information"
          extra={
            <Link href="/products">
              <a>View All </a>
            </Link>
          }
          className={s.row}
        >
          <Descriptions column={1}>
            <Descriptions.Item label="Code">
              {productVariant?.code}
            </Descriptions.Item>
            <Descriptions.Item label="Position">
              {productVariant?.position}
            </Descriptions.Item>
            <Descriptions.Item label="Options">
              {productVariant?.optionValues.length !== 0
                ? //@ts-ignore
                  productVariant?.optionValues[0].option.name
                : 'No Option!'}
            </Descriptions.Item>
            <Descriptions.Item label="Values">
              {productVariant?.optionValues.length !== 0
                ? productVariant?.optionValues.map((item) => {
                    return <span key={item.id}>{`- ${item.code}   `}</span>;
                  })
                : 'No Values!'}
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </Col>
    </Row>
  );
};

export default View;
