import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { useRouter } from 'next/router';
import CategoryLeadsService from '@services/categories/categoryDeliveries.api.service';
import CategoryLeadForm from '@components/CategoryLeadsPage/Form';
import BreadcrumbPage from '@components/Breadcrumb';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const categoryLead = await CategoryLeadsService.getById(id as string);

          if (categoryLead?.data) {
            const { category, value } = categoryLead.data.results;
            //@ts-ignore
            setTitlePage(category?.title);
            form.setFieldsValue({ category, value });
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      setLoading(true);

      try {
        await CategoryLeadsService.edit({ ...values, category: values.category?.id }, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/category-leads`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );
  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Category Leads', route: '/category-leads' },
          { name: titlePage },
        ]}
      />

        <Card
          title="Update Category Lead"
          extra={
            <Link href="/category-leads">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
            <CategoryLeadForm loading={loading} form={form} onFinish={onFinish} />
          </FormAlignment>
        </Card>
    </>
  );
};

export default Edit;
