import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import CategoryLeadForm from '@components/CategoryLeadsPage/Form';
import CategoryLeadsService from '@services/categories/categoryDeliveries.api.service';
import { useRouter } from 'next/router';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    setLoading(true);
    try {
      await CategoryLeadsService.add({ ...values, category: values.category?.id });
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/category-leads`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <>
        <Card
          title="Add Category Lead"
          extra={
            <Link href="/category-leads">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
            <CategoryLeadForm loading={loading} form={form} onFinish={onFinish} />
          </FormAlignment>
        </Card>
    </>
  );
};

export default Add;
