import React from 'react';
import { Layout, Menu } from 'antd';
import logo from 'public/static/images/LogoTypo.svg';
import { NextRouter, useRouter } from 'next/router';
import Link from 'next/link';
import {
  DashboardOutlined,
  DollarCircleOutlined,
  DropboxOutlined, FolderOpenOutlined,
  LogoutOutlined,
  SettingOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import s from './styles.module.scss';
import {
  FileDoneOutlined,
  ShoppingOutlined,
  SkinOutlined,
} from '@ant-design/icons/lib';
import AuthService from '@services/AuthService';
import Head from 'next/head';

const { Header, Footer, Sider, Content } = Layout;

interface AdminLayoutProps {
  hasFooter?: boolean;
  title: string;
}

const AdminLayout: React.FC<AdminLayoutProps> = ({
  children,
  hasFooter,
  title,
}) => {
  const router: NextRouter = useRouter();
  const { asPath } = router;

  return (
    <>
      <Head>
        <title>Timcheh | {title}</title>
      </Head>
      <Layout className={s.root}>
        <Sider
          defaultCollapsed={true}
          collapsedWidth={80}
          className={s.aside}
          collapsible
        >
          <img src={logo} alt="" className={s.logo} />
          <Menu
            theme="dark"
            defaultSelectedKeys={[`/${asPath.split('/')[1]}`]}
            mode="inline"
          >
            <Menu.Item key="/" icon={<DashboardOutlined />}>
              <Link href="/">
                <a>Dashboard</a>
              </Link>
            </Menu.Item>

            <Menu.Item key="/admins" icon={<UserOutlined />}>
              <Link href="/admins">
                <a>Admins</a>
              </Link>
            </Menu.Item>

            <Menu.Item key="/customers" icon={<TeamOutlined />}>
              <Link href="/customers">
                <a>Customers</a>
              </Link>
            </Menu.Item>

            <Menu.Item key="/sellers" icon={<DollarCircleOutlined />}>
              <Link href="/sellers">
                <a>Sellers</a>
              </Link>
            </Menu.Item>

            <Menu.SubMenu
              key="/products"
              icon={<SkinOutlined />}
              title="Product Catalogs"
            >
              <Menu.Item key="/products">
                <Link href="/products">
                  <a>Products</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/inventories">
                <Link href="/inventories">
                  <a>inventories</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/product-variants">
                <Link href="/product-variants">
                  <a>Product Variants</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/guarantees">
                <Link href="/guarantees">
                  <a>Guarantees</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/colors">
                <Link href="/colors">
                  <a>Colors</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/shoe-sizes">
                <Link href="/shoe-sizes">
                  <a>Shoe sizes</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/dress-sizes">
                <Link href="/dress-sizes">
                  <a>Dress sizes</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/baby-dress-sizes">
                <Link href="/baby-dress-sizes">
                  <a>Baby Dress sizes</a>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>

            <Menu.SubMenu
              key="/categories"
              icon={<FolderOpenOutlined />}
              title="Taxonomy"
            >
              <Menu.Item key="/brands">
                <Link href="/brands">
                  <a>Brands</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/categories">
                <Link href="/categories">
                  <a>Categories</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/category-commissions">
                <Link href="/category-commissions">
                  <a>Category Commissions</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/category-deliveries">
                <Link href="/category-deliveries">
                  <a>Category Deliveries</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/category-leads">
                <Link href="/category-leads">
                  <a>Category Leads</a>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>

            <Menu.Item key="/orders" icon={<FileDoneOutlined />}>
              <Link href="/orders">
                <a>Order Process</a>
              </Link>
            </Menu.Item>

            <Menu.Item key="/shipments" icon={<DropboxOutlined />}>
              <Link href="/shipments">
                <a>Shipment Process</a>
              </Link>
            </Menu.Item>

            <Menu.SubMenu
              key="/shipping"
              icon={<ShoppingOutlined />}
              title="Delivery"
            >
              <Menu.Item key="/shipping-categories">
                <Link href="/shipping-categories">
                  <a>Shipping Categories</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/shipping-methods">
                <Link href="/shipping-methods">
                  <a>Shipping Method</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/shipping-periods">
                <Link href="/shipping-periods">
                  <a>Shipping Periods</a>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>

            <Menu.SubMenu
              key="/setting"
              icon={<SettingOutlined />}
              title="Setting"
            >
              <Menu.Item key="/holidays">
                <Link href="/holidays">
                  <a>Holidays</a>
                </Link>
              </Menu.Item>
              <Menu.Item key="/payment-methods">
                <Link href="/payment-methods">
                  <a>Payment methods</a>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>
          </Menu>
        </Sider>
        <Layout className={s.content}>
          <Header className={s.header}>
            <span className={s.title}>Timcheh</span>
            <div>
              <LogoutOutlined
                className={s.icon}
                onClick={() => AuthService.signOut()}
              />
            </div>
          </Header>
          <Content>{children}</Content>
          {hasFooter && <Footer>Footer</Footer>}
        </Layout>
      </Layout>
    </>
  );
};

export default AdminLayout;
