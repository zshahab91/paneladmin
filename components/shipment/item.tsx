import {
  CheckOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
  MinusOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import OrderItem from '@entities/orderItems';
import { ImageProxy } from '@services/image';
import OrdersApiService from '@services/OrderService/Orders.api.service';
import {
  Button,
  Checkbox,
  Col,
  Divider,
  Modal,
  Row,
  Space,
  Typography,
} from 'antd';
import { useRouter } from 'next/router';
import React, { useState } from 'react';

const proxy = new ImageProxy();

interface IProps extends OrderItem {
  order?: string;
}

const Item: React.FC<IProps> = (props) => {
  const { id, subtotal, grandTotal, price, inventory } = props;
  const router = useRouter();
  const [quantity, setQuantity] = useState<number>(props.quantity);
  const [isModificationEnabled, setModificationStatus] = useState<boolean>(
    false,
  );
  const image = proxy.generate(
    inventory?.variant?.product?.featuredImage.url as string,
    250,
    250,
  );

  const onModificationToggle = () => setModificationStatus((s) => !s);
  const onQuantityDecreaseHandler = () => setQuantity((q) => q - 1);
  const onQuantityIncreaseHandler = () => setQuantity((q) => q + 1);
  const onQuantityChangeApplyHandler = async () => {
    try {
      await OrdersApiService.updateItems(props.order as string, [
        { id, quantity },
      ]);
      router.reload();
    } catch (e) {
      Modal.error({ content: 'This order item could not be updated!' });
    }
  };
  const onRemoveHandler = () => {
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined />,
      content: 'Are you sure you want to delete this item ?!',
      okText: 'Confirm',
      cancelText: 'Cancel',
      onOk: async () => {
        await OrdersApiService.deleteItem(id);
        router.reload();
      },
    });
  };

  return (
    <React.Fragment key={id}>
      <Row gutter={[15, 30]}>
        <Col xs={24} md={12} lg={2}>
          <a
            target="_blank"
            rel="noreferrer"
            href={`https://timcheh.com/product/${inventory?.variant?.product?.code}`}
          >
            <img src={image} alt="featured image" style={{ width: '100%' }} />
          </a>
        </Col>

        <Col
          xs={24}
          md={12}
          lg={22}
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'space-between',
          }}
        >
          <Row style={{ direction: 'rtl' }}>
            <Col xs={24} md={12} lg={24}>
              {inventory?.variant?.product?.title}
              {' | '}
              {inventory?.variant?.options?.['color']?.value}
              {' | '}
              {inventory?.variant?.options?.['guarantee']?.value}
            </Col>
          </Row>
          <Row style={{ width: '100%' }}>
            <Col xs={24} md={12} lg={8}>
              <b>TPI</b>: {inventory?.variant?.product?.id}
            </Col>
            <Col xs={24} md={12} lg={8}>
              <b>TVI</b>: {inventory?.id}
            </Col>
            <Col xs={24} md={12} lg={8}>
              <b>Seller</b>: {inventory?.seller?.name}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={[15, 15]}>
        <Col xs={24} md={12} lg={24}>
          <Row gutter={15} justify="space-between">
            <Col>
              <Checkbox
                disabled={props.quantity === 1}
                onChange={onModificationToggle}
              >
                <Typography.Text strong type="warning">
                  Enable quantity modification
                </Typography.Text>
              </Checkbox>
            </Col>

            <Col>
              <Space>
                <Button
                  size={'small'}
                  icon={<MinusOutlined />}
                  disabled={!(isModificationEnabled && quantity !== 1)}
                  onClick={onQuantityDecreaseHandler}
                />
                <Button disabled size="small">
                  {quantity}
                </Button>
                <Button
                  size={'small'}
                  icon={<PlusOutlined />}
                  disabled={
                    !(isModificationEnabled && quantity !== props.quantity)
                  }
                  onClick={onQuantityIncreaseHandler}
                />

                <Divider type={'vertical'} />

                <Button
                  size="small"
                  type="primary"
                  disabled={
                    !(isModificationEnabled && quantity !== props.quantity)
                  }
                  icon={<CheckOutlined />}
                  onClick={onQuantityChangeApplyHandler}
                />
                <Button
                  danger
                  size={'small'}
                  icon={<DeleteOutlined />}
                  onClick={onRemoveHandler}
                />
              </Space>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={[15, 15]}>
        <Col xs={24} md={12} lg={24}>
          <Row gutter={15} justify="space-between">
            <Col>
              <Typography.Text strong>Price: </Typography.Text>
              {price.toLocaleString()}
            </Col>
            <Col>
              <Typography.Text strong>Quantity: </Typography.Text>
              {props.quantity}
            </Col>
            <Col>
              <Typography.Text strong>Subtotal: </Typography.Text>
              {subtotal.toLocaleString()}
            </Col>
            <Col>
              <Typography.Text strong>Grand total: </Typography.Text>
              {grandTotal.toLocaleString()}
            </Col>
          </Row>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Item;
