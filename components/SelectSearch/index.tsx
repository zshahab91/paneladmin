import React, { useState, useEffect } from 'react';
import { Select } from 'antd';


interface typeDataList {
  id: string | number,
  value: string
}

interface SelectSearchProps {
  value?:number
  datalist: typeDataList[],
  placeholder: string,
  typeDataReturn?: string,
  keyShow: string,
  isMultiple?: boolean;
  getMoreData: (index: number) => void | Promise<void> | Promise<{}>;
  setSelectedOption: (selcted: number) => void | Promise<void> | Promise<{}>;
}

const SelectSearch: React.FC<SelectSearchProps> = (props) => {
  const [pageIndex, setPageIndex] = useState(1);
  const [dataList, setDataList] = useState<typeDataList[]>();
  const [loadingSelect, setLoadingSelect] = useState(false);
  useEffect(() => {
    setDataList(props.datalist)
  }, [props.datalist]);

  //@ts-ignore
  const onScroll = async (event) => {
    const target = event.target;
    if (!loadingSelect && target.scrollTop + target.offsetHeight === target.scrollHeight) {
      const oldData = dataList;
      setLoadingSelect(true)
      setPageIndex(pageIndex + 1)

      const dataOption = await props.getMoreData(pageIndex + 1 as number);
      //@ts-ignore
      if (dataOption?.data) {
        //@ts-ignore
        const newData = dataOption.data.results;
        if(newData.length !== 0 ){
          //@ts-ignore
          const totalDataList = oldData.concat(newData);
          //@ts-ignore
          setDataList(totalDataList)
        }
      }
      // target.scrollTo(0, target.scrollHeight)
      setLoadingSelect(false)
    }
  }

  return (
    <Select
      placeholder={props.placeholder}
      allowClear
      showSearch
      mode={props.isMultiple ? "multiple" : undefined}
      optionFilterProp="children"
      filterOption={(input, option) =>
        option?.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
      onPopupScroll={onScroll}
      onChange={(val)=> {
        //@ts-ignore
        props.setSelectedOption(val)
      } }
      value={props.value}
    >
      {dataList && dataList.map((item) => (
        <Select.Option key={item.id} 
        //@ts-ignore
        value={props.typeDataReturn? item[props.typeDataReturn] : item.id}>
          {
            //@ts-ignore
            
            item[props.keyShow]
          }
        </Select.Option>
      ))}
    </Select>
  )
};

export default SelectSearch;
