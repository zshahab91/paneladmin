import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import ShippingPeriodForm from '@components/ShippingPeriodsPage/Form';
import ShippingPeriod from '@entities/shippingPeriod';
import ShippingPeriodsService from '@services/ShippingService/ShippingPeriods.api.service';
import { useRouter } from 'next/router';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    setLoading(true);

    const dto: Partial<ShippingPeriod> = values;

    try {
      await ShippingPeriodsService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/shipping-periods`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
        <Card
          title="Add Shipping Priods"
          extra={
            <Link href="/shipping-periods">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
          <ShippingPeriodForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
          </FormAlignment>
        </Card>
  );
};

export default Add;
