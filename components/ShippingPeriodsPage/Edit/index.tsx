import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { useRouter } from 'next/router';
import ShippingPeriodForm from '@components/ShippingPeriodsPage/Form';
import ShippingPeriod from '@entities/shippingPeriod';
import ShippingPeriodsService from '@services/ShippingService/ShippingPeriods.api.service';
import BreadcrumbPage from '@components/Breadcrumb';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const shippingPeriod = await ShippingPeriodsService.getById(
            id as string,
          );

          if (shippingPeriod?.data) {
            form.setFieldsValue(shippingPeriod.data.results);
            //@ts-ignore
            setTitlePage(shippingPeriod.data.results.id);
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      setLoading(true);

      const dto: Partial<ShippingPeriod> = values;

      try {
        await ShippingPeriodsService.edit(dto, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/shipping-periods`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Periods', route: 'shipping-periods' },
          { name: `Edit ${titlePage} Priod` },
        ]}
      />

        <Card
          title="Edit Shipping Period"
          extra={
            <Link href="/shipping-periods">
              <a>View All </a>
            </Link>
          }
        >
        <FormAlignment>
          <ShippingPeriodForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
        </FormAlignment>
        </Card>
    </>
  );
};

export default Edit;
