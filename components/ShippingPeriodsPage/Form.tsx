import React from 'react';
import { Button, Checkbox, Form, Input, Space } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';

interface FormProps {
  onFinish: (values: Store) => Promise<void>;
  form: FormInstance;
  loading: boolean;
}

const ShippingPeriodForm: React.FC<FormProps> = (props) => {
  const { onFinish, form, loading } = props;
  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item name="start" label="Starting interval" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="end" label="End of interval" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item
        name="active"
        valuePropName="checked"
        rules={[{ required: true }]}
      >
        <Checkbox>Active</Checkbox>
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={loading}>
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default ShippingPeriodForm;
