import React, {useCallback, useState} from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import {Card, Form, message} from 'antd';
import {Store} from 'rc-field-form/es/interface';
import ColorsForm from '@components/ColorsPage/Form';
import {IValidationError} from "@entities/validation.error";
import slugify from 'slugify';
import FormAlignment from "@components/CP/FormAlignment";
import ColorService from "@services/Color/color.api.service";
import {Color} from "@entities/productOption";

const Add: React.FC = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const router = useRouter();
    const onFinish = useCallback(async (values: Store): Promise<void> => {
        setLoading(true);

        try {
            await ColorService.add(values);
            message.success('Successfully added.');
            setTimeout(() => {
                router.push(`/colors`);
            }, 500);
        } catch (e) {
            const response: IValidationError = e.response?.data;
            message.error(response?.message || 'Server error!');
        }

        setLoading(false);
    }, []);

    return (
        <Card
            title="Add Color"
            extra={
                <Link href="/colors">
                    <a>View All</a>
                </Link>
            }
        >
            <FormAlignment>
                <ColorsForm loading={loading} form={form} onFinish={onFinish}/>
            </FormAlignment>
        </Card>
    );
};

export default Add;
