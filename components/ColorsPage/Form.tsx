import React, { useState, useEffect } from 'react';
import { Button, Form, Input, Space } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import { slugify } from '@services/common/slugify';

interface FormProps {
  onFinish: (values: Store) => Promise<void>;
  form: FormInstance;
  loading: boolean;
  valueHex?: string;
}

const ColorsForm: React.FC<FormProps> = (props) => {
  const { onFinish, form, loading } = props;

  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item name="code" label="Name (En)" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="value" label="Name (Fa)" rules={[{ required: true }]}>
        <Input />
      </Form.Item>

      <Form.Item name={['attributes', 'hex']} label="Hex" rules={[{ required: true }]}>
        <Input type="color" style={{ width: '80px' }} defaultValue={"#FFFFFF"} />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={loading}>
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default ColorsForm;
