// import React, {useCallback, useEffect, useState} from 'react';
// import Link from 'next/link';
// import {Card, Form, message} from 'antd';
// import {Store} from 'rc-field-form/es/interface';
// import {useRouter} from 'next/router';
// import GuaranteeForm from '@components/GuaranteesPage/Form';
// import BreadcrumbPage from '@components/Breadcrumb';
// import {IValidationError} from '@entities/validation.error';
// import slugify from 'slugify';
// import FormAlignment from "@components/CP/FormAlignment";
// import GuaranteeService from "@services/Color/color.api.service";
// import {Guarantee} from "@entities/productOption";
// import {useColors} from "@components/hooks/productOptions";

// const Edit: React.FC = () => {
//     const router = useRouter();
//     const {id: idString} = router.query;
//     const id = Number(idString as string);
//     const [form] = Form.useForm();
//     const [loading, setLoading] = useState(false);

//     const {data: guarantees} = useGuarantees();
//     const guarantee = guarantees?.find(g => g.id === id);

//     useEffect(() => {
//         if (guarantee) {
//             // eslint-disable-next-line @typescript-eslint/no-unused-vars
//             const {id, ...restExceptId} = guarantee;
//             form.setFieldsValue(restExceptId);
//         }
//     }, [guarantee?.id])

//     const onFinish = useCallback(
//         async (values: Store): Promise<void> => {
//             setLoading(true);
//             const dto: Partial<Guarantee> = {
//                 ...values,
//                 code: slugify(values.code),
//             };

//             try {
//                 await GuaranteeService.edit(id as number, dto);
//                 message.success('Edited successfully.');
//                 setTimeout(() => {
//                     router.push(`/guarantees`);
//                 }, 500);
//             } catch (e) {
//                 const response: IValidationError = e.response?.data;
//                 message.error(response?.message || 'Server error!');
//             }

//             setLoading(false);
//         },
//         [id],
//     );

//     return (
//         <>
//             <BreadcrumbPage
//                 listLinks={[
//                     {name: 'Home', route: '/'},
//                     {name: 'Guarantees', route: '/guarantees'},
//                     {name: guarantee?.value || ''},
//                 ]}
//             />
//             <Card
//                 title="Update Guarantee"
//                 extra={
//                     <Link href="/guarantees">
//                         <a>View All</a>
//                     </Link>
//                 }
//             >
//                 <FormAlignment>
//                     <GuaranteeForm loading={loading} form={form} onFinish={onFinish}/>
//                 </FormAlignment>
//             </Card>
//         </>
//     );
// };

// export default Edit;

import GuaranteeService from "@services/Guarantees/guarantees.api.service";
import React, { useCallback, useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import ColorsForm from '@components/ColorsPage/Form';
import { IValidationError } from '@entities/validation.error';
import slugify from 'slugify';
import FormAlignment from '@components/CP/FormAlignment';
import ColorService from '@services/Color/color.api.service';
import { Color } from '@entities/productOption';
import { useColors } from '@components/hooks/productOptions';

const Edit: React.FC = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const router = useRouter();
    const { id: idString } = router.query;
    const id = Number(idString as string);

    useEffect(() => {
        if (!router.query.id) {
            return;
        }

        (async () => {
            const { data: { results: color } } = await ColorService.show(router.query.id as unknown as number)
            form.setFieldsValue(color);
        })();
    }, [router.query.id])
    const onFinish = useCallback(
        async (values: Store): Promise<void> => {
            setLoading(true);

            try {
                await ColorService.edit(id as number, values);
                message.success('Edited successfully.');
                setTimeout(() => {
                    router.push(`/colors`);
                }, 500);
            } catch (e) {
                const response: IValidationError = e.response?.data;
                message.error(response?.message || 'Server error!');
            }

            setLoading(false);
        },
        [id],
    );

    return (
        <Card
            title="Edit Color"
            extra={
                <Link href="/colors">
                    <a>View All</a>
                </Link>
            }
        >
            <FormAlignment>
                <ColorsForm loading={loading} form={form} onFinish={onFinish}/>
            </FormAlignment>
        </Card>
    );
};

export default Edit;
