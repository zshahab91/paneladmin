import BreadcrumbPage from '@components/Breadcrumb';
import FormAlignment from "@components/CP/FormAlignment";
import DressSizesForm from "@components/DressSizesPage/Form";
import { DressSize } from "@entities/productOption";
import { IValidationError } from '@entities/validation.error';
import DressSizesService from "@services/Sizes/dress.sizes.api.service";
import { Card, Form, message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useEffect, useState } from 'react';
import slugify from 'slugify';

const Edit: React.FC = () => {
    const router = useRouter();
    const { id: idString } = router.query;
    const id = Number(idString as string);
    const [form] = Form.useForm();
    const [title, setTitle] = useState<string>('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!router.query.id) {
            return;
        }

        (async () => {
            const { data: { results: size } } = await DressSizesService.show(router.query.id as unknown as number)
            setTitle(size?.value as string);
            form.setFieldsValue(size);
        })();
    }, [router.query.id])

    const onFinish = useCallback(
        async (values: Store): Promise<void> => {
            setLoading(true);
            const dto: Partial<DressSize> = {
                ...values,
                code: slugify(values.code),
            };

            try {
                await DressSizesService.edit(id as number, dto);
                message.success('Edited successfully.');
                setTimeout(() => {
                    router.push(`/dress-sizes`);
                }, 500);
            } catch (e) {
                const response: IValidationError = e.response?.data;
                message.error(response?.message || 'Server error!');
            }

            setLoading(false);
        },
        [id],
    );

    return (
        <>
            <BreadcrumbPage
                listLinks={[
                    { name: 'Home', route: '/' },
                    { name: 'Dress sizes', route: '/dress-sizes' },
                    { name: title },
                ]}
            />
            <Card
                title="Update Dress size"
                extra={
                    <Link href="/dress-sizes">
                        <a>View All</a>
                    </Link>
                }
            >
                <FormAlignment>
                    <DressSizesForm loading={loading} form={form} onFinish={onFinish}/>
                </FormAlignment>
            </Card>
        </>
    );
};

export default Edit;
