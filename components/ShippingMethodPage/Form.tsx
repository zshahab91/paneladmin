import React, { useEffect, useState } from 'react';
import { Button, Divider, Form, Input, Select, Skeleton, Space } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import { IPaginatedResponse, Nullable } from '@entities/paginated.response';
import ShippingPeriod from '@entities/shippingPeriod';
import ShippingPeriodsService from '@services/ShippingService/ShippingPeriods.api.service';
import ZonesApiService from '@services/ZoneService/Zones.api.service';
import Zone from '@entities/zone';
import ShippingCategoriesApiService from '@services/ShippingService/ShippingCategories.api.service';
import ShippingCategory from '@entities/shippingCategory';

const { Option } = Select;

interface FormProps {
  onFinish: (values: Store) => Promise<void>;
  form: FormInstance;
  loading: boolean;
}

const ShippingMethodForm: React.FC<FormProps> = (props) => {
  const { onFinish, form, loading } = props;
  const [shippingCategories, setShippingCategories] = useState<
    Nullable<IPaginatedResponse<ShippingCategory[]>>
  >();
  const [periods, setPeriods] = useState<
    Nullable<IPaginatedResponse<ShippingPeriod[]>>
  >();
  const [zones, setZones] = useState<Nullable<IPaginatedResponse<Zone[]>>>();

  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  useEffect(() => {
    (async () => {
      const [categories, periods, zones] = await Promise.all([
        ShippingCategoriesApiService.grid(),
        ShippingPeriodsService.getList(),
        ZonesApiService.getList(),
      ]);
      setShippingCategories(categories.data);
      setPeriods(periods.data);
      setZones(zones.data);
    })();
  }, []);

  if (!shippingCategories || !periods || !zones) {
    return <Skeleton active />;
  }

  if (!zones?.results.length) {
    return <h4>Error receiving zones...!Areas should not be empty.</h4>;
  }

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item
        name="name"
        label="Submission method name"
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="categories"
        label="Categories"
        rules={[{ required: true }]}
      >
        <Select mode="multiple" placeholder="Select Category" allowClear>
          {shippingCategories?.results.map((item) => (
            <Option key={item.id} value={item.id}>
              {item.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="periods" label="Periods" rules={[{ required: true }]}>
        <Select
          mode="multiple"
          placeholder="Select Sending Interval"
          allowClear
        >
          {periods?.results.map((item) => (
            <Option key={item.id} value={item.id}>
              {item.start}-{item.end}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Divider orientation="left">Shipping Costs To Different Areas</Divider>
      {zones?.results?.map((item, index) => {
        return (
          <Form.Item
            key={item.code}
            label={item.name}
            rules={[{ required: true }]}
            name="shippingMethodPrices"
          >
            <Form.Item
              name={['shippingMethodPrices', index, 'price']}
              rules={[{ required: true, message: 'Price is required!' }]}
              style={{ margin: 0 }}
            >
              <Input style={{ direction: 'ltr' }} />
            </Form.Item>
            <Form.Item
              name={['shippingMethodPrices', index, 'zone']}
              initialValue={item.id}
              style={{ height: 0, margin: 0 }}
              rules={[{ required: true }]}
            >
              <Input type="hidden" value={item.id} />
            </Form.Item>
          </Form.Item>
        );
      })}
      <Form.Item {...tailLayout}>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={loading}>
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default ShippingMethodForm;
