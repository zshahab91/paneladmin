import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import ShippingMethodForm from '@components/ShippingMethodPage/Form';
import ShippingMethodsService from '@services/ShippingService/ShippingMethods.api.service';
import { AddShippingMethod } from '@entities/shippingMethod';
import { useRouter } from 'next/router';
import {IValidationError} from "@entities/validation.error";
import {FormErrorProps} from "@entities/form";
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    const { name, categories, periods, shippingMethodPrices } = values;
    setLoading(true);

    const dto: AddShippingMethod = {
      name,
      categories,
      periods,
      configuration: [],
      shippingMethodPrices,
    };

    try {
      await ShippingMethodsService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/shipping-methods`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response) {
        const errorsDto: FormErrorProps = {
          price: ['shippingMethodPrices'],
        };
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: errorsDto[key] || key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
        <Card
          title="Add Shipping Method"
          extra={
            <Link href="/shipping-methods">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
          <ShippingMethodForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
          </FormAlignment>
        </Card>
  );
};

export default Add;
