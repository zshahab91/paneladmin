import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { EditShippingMethod } from '@entities/shippingMethod';
import { useRouter } from 'next/router';
import ShippingMethodsService from '@services/ShippingService/ShippingMethods.api.service';
import ShippingMethodForm from '@components/ShippingMethodPage/Form';
import BreadcrumbPage from '@components/Breadcrumb';
import {IValidationError} from "@entities/validation.error";
import {FormErrorProps} from "@entities/form";
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const response = await ShippingMethodsService.getById(id as string);

          if (response?.data) {
            const {
              name,
              categories,
              periods,
              shippingMethodPrices,
            } = response.data.results;
            setTitlePage(name);
            form.setFieldsValue({
              name,
              categories: categories.map((item) => item.id),
              periods: periods.map((item) => item.id),
              shippingMethodPrices: shippingMethodPrices.map((item) => ({
                price: item.price,
                zone: item.zone.id,
              })),
            });
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      const { name, categories, periods, shippingMethodPrices } = values;
      setLoading(true);

      const dto: EditShippingMethod = {
        configuration: [],
        name,
        categories,
        periods,
        shippingMethodPrices,
      };

      try {
        await ShippingMethodsService.edit(dto, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/shipping-methods`);
        }, 500);
      } catch (e) {
          const response: IValidationError = e.response?.data;
          if (response) {
              const errorsDto: FormErrorProps = {
                  price: ['shippingMethodPrices'],
              };
              const formErrors = Object.keys(response.results).map((key: string) => ({
                  name: errorsDto[key] || key,
                  errors: response.results[key],
              }));
              form.setFields(formErrors);
          }
          message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Methods', route: '/shipping-methods' },
          { name: titlePage },
        ]}
      />

        <Card
          title="Edit Shipping Method"
          extra={
            <Link href="/shipping-methods">
              <a>View All </a>
            </Link>
          }
        >
        <FormAlignment>
          <ShippingMethodForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
        </FormAlignment>
        </Card>
    </>
  );
};

export default Edit;
