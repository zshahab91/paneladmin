import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Form, Card, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { EditBrandProps } from '@entities/brand';
import { useRouter } from 'next/router';
import BrandsService from '@services/BrandsService';
import BrandForm from '@components/BrandsPage/Form';
import BreadcrumbPage from '@components/Breadcrumb';
import { FormErrorProps } from '@entities/form';
import { IValidationError } from '@entities/validation.error';
import slugify from 'slugify';
import FormAlignment from '@components/CP/FormAlignment';

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const brand = await BrandsService.getById(id as string);

          if (brand?.data) {
            const {
              title,
              subtitle,
              metaDescription,
              image,
              code,
            } = brand.data.results;
            setTitlePage(title);
            form.setFieldsValue({
              title,
              subtitle,
              metaDescription,
              image,
              code,
            });
          }
        } catch (e) {
          message.error(
            `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
          );
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      setLoading(true);

      const dto: EditBrandProps = {
        metaDescription: values.metaDescription,
        title: values.title,
        subtitle: values.subtitle,
        image: values.image || null,
        code: slugify(values.code),
      };

      try {
        await BrandsService.edit(dto, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/brands`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        const errorsDto: FormErrorProps = {
          path: ['image', 'path'],
          alt: ['image', 'alt'],
        };
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: errorsDto[key] || key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Brands', route: '/brands' },
          { name: titlePage },
        ]}
      />
      <Card
        title="Update Brand"
        extra={
          <Link href="/brands">
            <a>View All</a>
          </Link>
        }
      >
        <FormAlignment>
          <BrandForm loading={loading} form={form} onFinish={onFinish} />
        </FormAlignment>
      </Card>
    </>
  );
};

export default Edit;
