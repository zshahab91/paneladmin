import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Form, Card, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import BrandForm from '@components/BrandsPage/Form';
import BrandsService from '@services/BrandsService';
import { AddBrandProps } from '@entities/brand';
import BreadcrumbPage from '@components/Breadcrumb';
import { FormErrorProps } from '@entities/form';
import {IValidationError} from "@entities/validation.error";
import slugify from 'slugify';
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    const { title, subtitle, metaDescription, image, code } = values;
    setLoading(true);

    const dto: AddBrandProps = {
      title,
      subtitle,
      metaDescription,
      image,
      code: slugify(code)
    };

    try {
      await BrandsService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/brands`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const errorsDto: FormErrorProps = {
          path: ['image', 'path'],
          alt: ['image', 'alt'],
        };
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: errorsDto[key] || key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Brands', route: '/brands' },
          { name: 'Add New Brand' },
        ]}
      />
        <Card
          title="Add Brand"
          extra={
            <Link href="/brands">
              <a>View All</a>
            </Link>
          }
        >
          <FormAlignment>
            <BrandForm loading={loading} form={form} onFinish={onFinish} />
          </FormAlignment>
        </Card>
    </>
  );
};

export default Add;
