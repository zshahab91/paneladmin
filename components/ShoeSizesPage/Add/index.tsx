import React, {useCallback, useState} from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import {Card, Form, message} from 'antd';
import {Store} from 'rc-field-form/es/interface';
import ShoeSizesForm from '@components/ShoeSizesPage/Form';
import {IValidationError} from "@entities/validation.error";
import slugify from 'slugify';
import FormAlignment from "@components/CP/FormAlignment";
import ShoeSizesService from "@services/Sizes/shoe.sizes.api.service";
import {Guarantee} from "@entities/productOption";

const Add: React.FC = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(false);
    const router = useRouter();
    const onFinish = useCallback(async (values: Store): Promise<void> => {
        setLoading(true);

        const dto: Partial<Guarantee> = {
            ...values,
            code: slugify(values.code),
        };

        try {
            await ShoeSizesService.add(dto);
            message.success('Successfully added.');
            setTimeout(() => {
                router.push(`/shoe-sizes`);
            }, 500);
        } catch (e) {
            const response: IValidationError = e.response?.data;
            message.error(response?.message || 'Server error!');
        }

        setLoading(false);
    }, []);

    return (
        <Card
            title="Add Shoe size"
            extra={
                <Link href="/shoe-sizes">
                    <a>View All</a>
                </Link>
            }
        >
            <FormAlignment>
                <ShoeSizesForm loading={loading} form={form} onFinish={onFinish}/>
            </FormAlignment>
        </Card>
    );
};

export default Add;
