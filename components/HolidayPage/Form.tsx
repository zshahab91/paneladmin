import SellerSelector from "@components/CP/SellerSelector";
import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Space, message, Select } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import Seller from '@entities/seller';
import SellerApiService from '@services/SellerService/Seller.api.service';
import CPDatePicker from '@components/CP/CPDatePicker/CPDatePicker';
import SelectSearch from '@components/SelectSearch';

interface FormProps {
    onFinish: (values: Store) => Promise<void>;
    form: FormInstance;
    loading: boolean;
    isSupply?: boolean;
    nameSeller?: string;
}

const HolidayForm: React.FC<FormProps> = (props) => {
    const { onFinish, form, loading } = props;
    const [, setSupply] = useState(false);

    // form layout
    const layout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    };

    // form layout
    const tailLayout = {
        wrapperCol: { offset: 6, span: 16 },
    };

    useEffect(() => {
        if (props.isSupply) {
            setSupply(props.isSupply);
        }
    }, []);

    const onFinishHandler = (values: Store) => {
        onFinish({ ...values, seller: values.seller?.id })
    }

    return (
        <Form {...layout} form={form} name="control-hooks" onFinish={onFinishHandler}>
            <Form.Item name="title" label="Name Fa" rules={[{ required: true }]}>
                <Input style={{ direction: "rtl" }}/>
            </Form.Item>
            <Form.Item name="date" label="Date" rules={[{ required: true }]}>
                {props.isSupply ? (
                    form.getFieldValue('date') && (
                        <CPDatePicker
                            initialValue={form.getFieldValue('date')}
                            onChange={(value) => {
                                form.setFieldsValue({
                                    date: value,
                                });
                            }}
                        />
                    )
                ) : (
                    <CPDatePicker
                        initialValue={form.getFieldValue('date')}
                        onChange={(value) => {
                            form.setFieldsValue({
                                date: value,
                            });
                        }}
                    />
                )}
            </Form.Item>
            <Form.Item name="seller" label="Seller" rules={[{ required: true }]}>
                <SellerSelector/>
            </Form.Item>
            <Form.Item name="supply" label="Supply" rules={[{ required: true }]}>
                <Select>
                    <Select.Option value={0}>We do not send</Select.Option>
                    <Select.Option value={1}>We have a send</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item {...tailLayout}>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={loading}>
                        Save
                    </Button>
                </Space>
            </Form.Item>
        </Form>
    );
};

export default HolidayForm;
