import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { useRouter } from 'next/router';
import HolidayApiService from '@services/HolidayService/Holiday.api.service';
import BreadcrumbPage from '@components/Breadcrumb';
import Holiday, { EditHoliday } from '@entities/holiday';
import HolidayForm from '../Form';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from '@components/CP/FormAlignment';

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');
  const [holiday, setHoliday] = useState<Holiday>();
  const [nameSeller, setNameSeller] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const holiday = await HolidayApiService.getById(id as string);

          if (holiday?.data) {
            const { title, seller, date, supply } = holiday.data.results;
            setTitlePage(title);
            setHoliday(holiday?.data.results);
            setNameSeller(seller?.name);
            form.setFieldsValue({
              title: title,
              date: date,
              seller: seller.id,
              supply: supply,
            });
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      const { title, date, seller, supply } = values;
      setLoading(true);

      const dto: EditHoliday = {
        title: title,
        date: date,
        seller: seller,
        supply: supply,
      };

      try {
        await HolidayApiService.edit(dto, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/holidays`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Holidays', route: '/holidays' },
          { name: titlePage },
        ]}
      />

      <Card
        title="Edit Holiday"
        extra={
          <Link href="/holidays">
            <a>View All </a>
          </Link>
        }
      >
        <FormAlignment>
          {holiday && (
            <HolidayForm
              loading={loading}
              form={form}
              onFinish={onFinish}
              isSupply={holiday?.supply}
              nameSeller={nameSeller}
            />
          )}
        </FormAlignment>
      </Card>
    </>
  );
};

export default Edit;
