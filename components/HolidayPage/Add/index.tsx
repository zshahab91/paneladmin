import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Form, Card, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import HolidayForm from '@components/HolidayPage/Form';
import HolidayService from '@services/HolidayService/Holiday.api.service';
import { AddHoliday } from '@entities/holiday';
import { useRouter } from 'next/router';
import {IValidationError} from "@entities/validation.error";
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    const { seller, title, date, supply } = values;
    setLoading(true);

    const dto: AddHoliday = {
      seller,
      title,
      date,
      supply,
    };

    try {
      await HolidayService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/holidays`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <Card
      title="Add Holiday"
      extra={
        <Link href="/holidays">
          <a>View All </a>
        </Link>
      }
    >
      <FormAlignment>
        <HolidayForm loading={loading} form={form} onFinish={onFinish} />
      </FormAlignment>
    </Card>
  );
};

export default Add;
