import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { useRouter } from 'next/router';
import CategoryCommissionsService from '@services/categories/categoryCommissions.api.service';
import CategoryCommissionForm from '@components/CategoryCommissionsPage/Form';
import BreadcrumbPage from '@components/Breadcrumb';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const categoryCommission = await CategoryCommissionsService.getById(
            id as string,
          );

          if (categoryCommission?.data) {
            const { category, fee } = categoryCommission.data.results;
            //@ts-ignore
            setTitlePage(category?.title);
            form.setFieldsValue({ category, fee });
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      setLoading(true);
      try {
        await CategoryCommissionsService.edit({ ...values, category: values.category?.id }, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/category-commissions`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Category Commissions', route: '/category-commissions' },
          { name: titlePage },
        ]}
      />

        <Card
          title="Update Category Commissions"
          extra={
            <Link href="/category-leads">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
            <CategoryCommissionForm
              loading={loading}
              form={form}
              onFinish={onFinish}
            />
          </FormAlignment>
        </Card>
    </>
  );
};

export default Edit;
