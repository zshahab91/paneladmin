import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import CategoryCommissionForm from '@components/CategoryCommissionsPage/Form';
import CategoryCommissionsService from '@services/categories/categoryCommissions.api.service';
import { useRouter } from 'next/router';
import {IValidationError} from "@entities/validation.error";
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    try {
      setLoading(true);
      await CategoryCommissionsService.add({ ...values, category: values.category?.id });
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/category-commissions`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <>
        <Card
          title=" Add Category Commissions"
          extra={
            <Link href="/category-commissions">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
            <CategoryCommissionForm
              loading={loading}
              form={form}
              onFinish={onFinish}
            />
          </FormAlignment>
        </Card>
    </>
  );
};

export default Add;
