import React from 'react';
import Link from 'next/link';
import { Card, Tree } from 'antd';
// import { AntTreeNodeMouseEvent } from 'antd/es/tree';
// import {AntTreeNode, AntTreeNodeDropEvent} from 'antd/es/tree/Tree';

const AddCategory: React.FC = () => {
  const treeData = [
    {
      title: 'parent 1',
      key: '0-0',
      children: [
        {
          title: 'parent 1-0',
          key: '0-0-0',
          disabled: true,
          children: [
            {
              title: 'leaf',
              key: '0-0-0-0',
              disableCheckbox: true,
            },
            {
              title: 'leaf',
              key: '0-0-0-1',
            },
          ],
        },
        {
          title: 'parent 1-1',
          key: '0-0-1',
          children: [
            {
              title: <span style={{ color: '#1890ff' }}>sss</span>,
              key: '0-0-1-0',
            },
          ],
        },
      ],
    },
  ];

  return (
    <Card
      title="Group Chart"
      extra={
        <Link href="/categories">
          <a>View All </a>
        </Link>
      }
    >
      <Tree
        draggable
        defaultExpandedKeys={['0-0-0', '0-0-1']}
        defaultSelectedKeys={['0-0-0', '0-0-1']}
        defaultCheckedKeys={['0-0-0', '0-0-1']}
        onDragEnd={({ event, node }) => {}}
        treeData={treeData}
      />
    </Card>
  );
};

export default AddCategory;
