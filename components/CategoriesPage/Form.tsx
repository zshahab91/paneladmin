import CategorySelector from '@components/CP/CategorySelector';
import React, { useState } from 'react';
import { Button, Form, Input, Space, Upload } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import UploadService from '@services/UploadService';
import { PlusOutlined } from '@ant-design/icons/lib';
import { ratioCheck } from '@utils/components.util';

interface FormProps {
  onFinish: (values: Store) => Promise<void>;
  form: FormInstance;
  loading: boolean;
}

const CategoryForm: React.FC<FormProps> = (props) => {
  const [image, setImage] = useState<string>();
  const { onFinish, form, loading } = props;
  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  const onFinishHandler = (values: Store) => {
    let data = values;
    if (!image) {
      data = { ...data, image: undefined };
    }
    onFinish({ ...data, parent: data.parent?.id });
  };

  return (
    <Form
      {...layout}
      form={form}
      name="control-hooks"
      onFinish={onFinishHandler}
    >
      <Form.Item name="code" label="Slug" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="title" label="Name Fa" rules={[{ required: true }]}>
        <Input style={{ direction: 'rtl' }} />
      </Form.Item>
      <Form.Item name="subtitle" label="Name En" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name="pageTitle" label="Page Title">
        <Input />
      </Form.Item>
      <Form.Item name="parent" label="Title Of Parent ">
        <CategorySelector />
      </Form.Item>
      <Form.Item name="description" label="Description">
        <Input.TextArea rows={8} maxLength={2000} />
      </Form.Item>
      <Form.Item name="metaDescription" label="Meta Description">
        <Input.TextArea rows={4} maxLength={300} />
      </Form.Item>
      <Form.Item name={['image', 'alt']} label="Name Of Image">
        <Input />
      </Form.Item>
      <Form.Item label="Image" shouldUpdate>
        {() => {
          const url = form.getFieldValue(['image', 'url']);
          // const errorText = form.getFieldError(['image', 'path']);

          if (url) {
            return (
              <Upload
                listType="picture-card"
                showUploadList={{
                  showDownloadIcon: false,
                  showPreviewIcon: false,
                  showRemoveIcon: true,
                }}
                onRemove={(e) => {
                  form.setFields([
                    {
                      name: ['image', 'path'],
                      value: null, // eslint-disable-line @rushstack/no-null
                    },
                    {
                      name: ['image', 'url'],
                      value: null, // eslint-disable-line @rushstack/no-null
                    },
                  ]);
                }}
                fileList={[
                  {
                    size: 100,
                    type: '',
                    uid: 'preview',
                    name: 'preview.png',
                    status: 'done',
                    url: image ?? url,
                  },
                ]}
              >
                {undefined}
              </Upload>
            );
          }

          return (
            <>
              <Upload
                customRequest={async (e) => {
                  try {
                    const response = await UploadService.add(
                      'category',
                      e.file,
                    );
                    setImage(response.data.results.media.url);
                    form.setFields([
                      {
                        name: ['image', 'path'],
                        value: response?.data.results.imageFileName,
                      },
                      {
                        name: ['image', 'url'],
                        value: response.data.results.media.url,
                      },
                    ]);
                    e.onSuccess(response?.data.results, e.file);
                  } catch (error) {
                    e.onError(error);
                  }
                }}
                beforeUpload={ratioCheck}
                listType="picture-card"
                showUploadList={false}
              >
                <PlusOutlined />
              </Upload>
              {/* <span style={{ color: '#ff4d4f' }}>{errorText}</span> */}
            </>
          );
        }}
      </Form.Item>
      <Form.Item
        name={['image', 'path']}
        style={{ height: 0, margin: 0, overflow: 'hidden' }}
      >
        <Input type="hidden" />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Space size="small">
          <Button type="primary" htmlType="submit" loading={loading}>
            Save
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default CategoryForm;
