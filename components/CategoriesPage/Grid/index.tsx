import React, {useEffect, useState} from 'react';
import {Space, Table} from 'antd';
import categoriesService from '@services/categories/categories.api.service';
import {IPaginatedResponse, Nullable,} from '@entities/paginated.response';
import Category from "@entities/category";
// import s from './styles.module.scss';

const Grid: React.FC = () => {
  const [loading, setLoading] = useState(true);
  const [,setData] = useState<Nullable<IPaginatedResponse<Category[]>>>(
    undefined,
  );

  useEffect(() => {
    (async () => {
      const { data: res } = await categoriesService.grid();
      setData(res);
      setLoading(false);
    })();
  }, []);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text: string) => <a>{text}</a>,
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: string) => (
        <Space size="middle">
          <a>Invite {123}</a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

  const dataSource = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
      tags: ['loser'],
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
      tags: ['cool', 'teacher'],
    },
  ];

  return <Table columns={columns} dataSource={dataSource} loading={loading} />;
};

export default Grid;
