import BreadcrumbPage from '@components/Breadcrumb';
import CategoryForm from '@components/CategoriesPage/Form';
import FormAlignment from '@components/CP/FormAlignment';
import { EditCategoryProps } from '@entities/category';
import { FormErrorProps } from '@entities/form';
import { IValidationError } from '@entities/validation.error';
import categoriesService from '@services/categories/categories.api.service';
import { Card, Form, message } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import React, { useCallback, useEffect, useState } from 'react';
import slugify from 'slugify';

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        const {data: {results: response}} = await categoriesService.categoryById(id as string);

          setTitlePage(response.title);
          form.setFieldsValue({ ...response });
        }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      setLoading(true);
      const dto: EditCategoryProps = {
        parent: values.parent,
        title: values.title,
        subtitle: values.subtitle,   
        code: slugify(values.code),
        level: 0,
        pageTitle: values.pageTitle,
        description : values.description,
        metaDescription: values.metaDescription
      };
      if(values.image !== undefined) {
        dto.image = values.image;
      }


      try {
        await categoriesService.edit(dto, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/categories`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        const errorsDto: FormErrorProps = {
          path: ['image', 'path'],
          alt: ['image', 'alt'],
        };
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: errorsDto[key] || key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Categories', route: '/categories' },
          { name: titlePage },
        ]}
      />

      <Card
        title="Update Category"
        extra={
          <Link href="/categories">
            <a>View All </a>
          </Link>
        }
      >
        <FormAlignment>
          <CategoryForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
        </FormAlignment>
      </Card>
    </>
  );
};

export default Edit;
