import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import categoriesService from '@services/categories/categories.api.service';
import { Store } from 'rc-field-form/es/interface';
import CategoryForm from '@components/CategoriesPage/Form';
import { IPaginatedResponse, Nullable } from '@entities/paginated.response';
import Category, { AddCategoryProps } from '@entities/category';
import { useRouter } from 'next/router';
import { FormErrorProps } from '@entities/form';
import { IValidationError } from '@entities/validation.error';
import slugify from 'slugify';
import FormAlignment from '@components/CP/FormAlignment';

const AddCategory: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [, setData] = useState<Nullable<IPaginatedResponse<Category[]>>>(
    undefined,
  );
  const router = useRouter();

  useEffect(() => {
    (async () => {
      const { data: res } = await categoriesService.grid();
      setData(res);
    })();
  }, []);

  const onFinish = useCallback(async (values: Store): Promise<void> => {
    setLoading(true);

    const dto: AddCategoryProps = {
      parent: values.parent,
      title: values.title,
      subtitle: values.subtitle,
      level: 0,
      configurations: [],
      image: values.image,
      code: slugify(values.code),
      pageTitle: values.pageTitle,
      description : values.description,
      metaDescription: values.metaDescription
    };

    try {
      await categoriesService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/categories`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      const errorsDto: FormErrorProps = {
        path: ['image', 'path'],
        alt: ['image', 'alt'],
      };
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: errorsDto[key] || key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <>
      <Card
        title="Add Category"
        extra={
          <Link href="/categories">
            <a>View All </a>
          </Link>
        }
      >
        <FormAlignment>
          <CategoryForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
        </FormAlignment>
      </Card>
    </>
  );
};

export default AddCategory;
