import React, { useEffect, useState } from 'react';
import { Store } from 'rc-field-form/es/interface';
import { Row, Col, Form, Card, message } from 'antd';
import ProductsService from '@services/ProductServices/Product.api.service';
import { useRouter } from 'next/router';
import ProductForm from '@components/ProductsPage/Form';
import BreadcrumbPage from '@components/Breadcrumb';
import Product, { EditProductProps, ImageResponse } from '@entities/product';
import UplaodMedia from '../UploadMedia';
import s from './styles.module.scss';
import { IValidationError } from '@entities/validation.error';
import slugify from 'slugify';

const Edit: React.FC = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [loading, setLoading] = useState(false);
  const [product, setProduct] = useState<Product>();
  const [images, setImages] = useState<ImageResponse[]>();
  const [featuredImage, setFeaturedImage] = useState<ImageResponse>();
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
        (async () => {
            if (id) {
                try {
                    const product = await ProductsService.getById(id as string);
                    if (product?.data) {
                        const {
                            title,
                            subtitle,
                            description,
                            metaDescription,
                            weight,
                            width,
                            length,
                            category,
                            brand,
                            seller,
                            shippingCategory,
                            status,
                            isActive,
                            height,
                            options,
                            isOriginal,
                            code
                        } = product.data.results;
                        setImages(product?.data.results.images);
                        setFeaturedImage(
                            product?.data.results.featuredImage
                        );
                        //@ts-ignore
                        setProduct(product?.data.results);
                        setTitlePage(title);
                        form.setFieldsValue({
                            title,
                            subtitle,
                            description,
                            metaDescription,
                            weight,
                            width,
                            length,
                            category,
                            brand,
                            seller,
                            shippingCategory: shippingCategory.id,
                            status,
                            isOriginal: isOriginal ? "1" : "0",
                            isActive,
                            height,
                            code,
                            options: options.map((item) => item.id),
                        });
                    }
                } catch {
                    message.error('Error receiving information');
                }
            }
        })();
    }, [id]);

    const onFinish = async (values: Store): Promise<void> => {
        const {
            title,
            subtitle,
            description,
            metaDescription,
            weight,
            width,
            length,
            category,
            brand,
            seller,
            shippingCategory,
            status,
            isOriginal,
            isActive,
            options,
            height,
            code
        } = values;
        setLoading(true);

        const finalImages = images?.map(i => ({path: i.path, alt: i.alt})); // Remove `url` from objects
        const finalFeaturedImages:ImageResponse  = {...featuredImage} as ImageResponse;
        delete finalFeaturedImages.url; // Remove `url` from objects

        //@ts-ignore
        const dto: EditProductProps = {
            title: title,
            subtitle: subtitle,
            description: description,
            metaDescription: metaDescription,
            weight: weight,
            width: width,
            length: length,
            category: category?.id,
            brand: brand?.id,
            seller: seller?.id,
            shippingCategory: shippingCategory,
            status: status,
            isOriginal: isOriginal === "1" ? true : false,
            isActive: isActive,
            options: options,
            height: height,
            code: slugify(code),
            featuredImage: finalFeaturedImages,
            images: finalImages,
        };

        try {
            await ProductsService.edit(dto, id as string);
            message.success('Edited successfully.');
            setTimeout(() => {
                router.push(`/products`);
            }, 500);
        } catch (e) {
            const response: IValidationError = e.response?.data;
            if (response?.results) {
                const formErrors = Object.keys(response.results).map((key: string) => ({
                    name: key,
                    errors: response.results[key],
                }));
                form.setFields(formErrors);
            }
            message.error(response?.message || 'Server error!');
        }

        setLoading(false);
    };

    return (
        <Row>
            <BreadcrumbPage
                listLinks={[
                    { name: 'Home', route: '/' },
                    { name: 'Products', route: '/products' },
                    { name: titlePage },
                ]}
            />

            <Col xs={24} md={12} lg={12} xl={12}>
                {product && (
                    <ProductForm
                        loading={loading}
                        form={form}
                        isActive={product?.isActive}
                        onFinish={onFinish}

                    />
                )}
            </Col>
            <Col xs={24} md={12} lg={12} xl={12}>
                {product && (
                    <Card className={s.row} title="Edit Feature Photo">
                        <p className={s.red}> Feature photo must have 1:1 ratio. (e.g. 1200x1200)</p>
                        {!featuredImage?.path && <p className={s.red}> Add Alt is mandatory for each photo!</p>}
                        <UplaodMedia
                            //@ts-ignore
                            onSetImages={(data, isDelete) => {
                                if (isDelete) {
                                    //@ts-ignore
                                    setFeaturedImage({});
                                } else {
                                    //@ts-ignore
                                    setFeaturedImage(data)
                                }
                            }
                            }
                            isFeatureImage={true}
                            fileList={
                                product?.featuredImage !== null ? [product?.featuredImage] : []
                            }
                        />
                    </Card>
                )}
                {product && (
                    <Card className={s.row} title="Edit Gallery">
                        <p className={s.red}> Gallery photos must have 1:1 ratio. (e.g. 600x600)</p>
                        {!images?.length && <p className={s.red}> Add Alt is mandatory for each photo!</p>}
                        <UplaodMedia
                            //@ts-ignore
                            onSetImages={(data, isDelete) => {
                                if (isDelete) {
                                    const newImages = images?.filter(i => i.path !== data.path);
                                    setImages(newImages);
                                } else {
                                    //@ts-ignore
                                    setImages((oldImages) => [...oldImages, data])
                                }
                            }
                            }
                            isFeatureImage={false}
                            fileList={images}
                        />
                    </Card>
                )}
            </Col>
        </Row>
    );
};

export default Edit;
