import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Store } from 'rc-field-form/es/interface';
import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  InputNumber,
  message,
  Row,
  Select,
} from 'antd';
import s from './styles.module.scss';
import BreadcrumbPage from '@components/Breadcrumb';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from '@components/CP/FormAlignment';
import SellerSelector from '@components/CP/SellerSelector';
import ProductsService from '@services/ProductServices/Product.api.service';
import Product from '@entities/product';
import { InventoryDTO } from '@entities/inventories';

const Inventory: React.FC = () => {
  const [form] = Form.useForm();
  const [form2] = Form.useForm();
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [product, setProduct] = useState<Product>();
  const [optionValues, setOptionValues] = useState<{ [id: number]: number }>(
    {},
  );
  const [maxLead, setMaxLead] = useState(0);
  useEffect(() => {
    if (!router.query.id) {
      return;
    }

    (async () => {
      const {
        data: { results },
      } = await ProductsService.getById(router.query.id as string);
      setProduct(results);
      //@ts-ignore
      setMaxLead(results.maxLead);
      if(results.maxLead === 0){
        form.setFieldsValue({ suppliesIn: 0 });
      }
    })();
  }, [router.query.id]);

  const onFinish = async (values: Store): Promise<void> => {
    setLoading(true);
    const dto = {
      seller : values.seller.id,
      stock: values.stock,
      price: values.price,
      finalPrice: values.finalPrice,
      isTrackable: values.isTrackable,
      isActive: values.isActive,
      maxPurchasePerOrder: values.maxPurchasePerOrder,
      suppliesIn: maxLead !== 0 ? values.suppliesIn : 0
    }
    try {
      //@ts-ignore
      await ProductsService.createInventory({
        ...dto,
        product: (router.query.id as unknown) as number,
        optionValues: Object.values(optionValues),
      } as InventoryDTO);
      message.success('Created successfully.');
      setTimeout(() => {
        router.push(`/products`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  };
  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Products', route: '/products' },
          { name: String(router.query.id) },
          { name: 'Create inventory' },
        ]}
      />

      <Form.Provider>
        <Row gutter={24}>
          <Col span={12}>
            <Card className={s.row} title="Inventory detail">
              <FormAlignment>
                <Form
                  form={form}
                  onFinish={onFinish}
                  initialValues={{ isTrackable: true, isActive: true }}
                >
                  <Form.Item
                    name="seller"
                    label="Seller"
                    rules={[{ required: true }]}
                  >
                    <SellerSelector/>
                  </Form.Item>

                  <Form.Item
                    name="stock"
                    label="Stock"
                    rules={[{ required: true }]}
                  >
                    <InputNumber className={s.input} />
                  </Form.Item>

                  <Form.Item
                    name="price"
                    label="Price"
                    rules={[{ required: true }]}
                  >
                    <InputNumber
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => (value || '').replace(/(,*)/g, '')}
                        onChange={price => form.setFieldsValue({price})}
                    />
                  </Form.Item>

                  <Form.Item
                    name="finalPrice"
                    label="Final Price"
                    rules={[{ required: true }]}
                  >
                    <InputNumber
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => (value || '').replace(/(,*)/g, '')}
                        onChange={finalPrice => form.setFieldsValue({finalPrice})}
                    />
                  </Form.Item>

                  <Form.Item
                    name="isTrackable"
                    label="Is Trackable"
                    valuePropName="checked"
                  >
                    <Checkbox />
                  </Form.Item>

                  <Form.Item
                    name="isActive"
                    label="Is active"
                    valuePropName="checked"
                  >
                    <Checkbox />
                  </Form.Item>

                  <Form.Item
                    name="maxPurchasePerOrder"
                    label="Maximum purchase per order"
                    rules={[{ required: true }]}
                  >
                    <InputNumber className={s.input} />
                  </Form.Item>

                  <Form.Item
                    name="suppliesIn"
                    label="Lead time"
                    rules={[{ required: true }]}
                  >
                    <InputNumber max={maxLead} min={0} className={s.input} defaultValue={maxLead} disabled={maxLead === 0}  />
                  </Form.Item>
                </Form>
                {/*<ProductVariantForm*/}
                {/*    loading={loading}*/}
                {/*    form={form}*/}
                {/*    editable={true}*/}
                {/*    onFinish={onFinish}*/}
                {/*    productVariant={productVariant}*/}
                {/*/>*/}
              </FormAlignment>
            </Card>
          </Col>
          <Col span={12}>
            <Card title={'Options'}>
              <Form form={form2}>
                {product?.options.map((option) => {
                  return (
                    <Form.Item
                      key={option.id}
                      name={option.id}
                      label={option.name}
                      rules={[{ required: true }]}
                    >
                      <Select
                        onChange={(value) =>
                          setOptionValues((state) => ({
                            ...state,
                            [option.id]: value as number,
                          }))
                        }
                      >
                        {option.values.map((value) => (
                          <Select.Option key={value.id} value={value.id ?? ''}>
                            {value.value}
                          </Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                  );
                })}
              </Form>
            </Card>
          </Col>
        </Row>

        <Button
          type="primary"
          htmlType="submit"
          onClick={() => {
            form.submit();
            form2.submit();
          }}
          className={s.row}
          loading={loading}
        >
          Save
        </Button>
      </Form.Provider>
    </>
  );
};

export default Inventory;
