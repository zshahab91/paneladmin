import { ISeller } from '@entities/seller';
import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  Card,
  message,
  Switch,
  Descriptions,
  Input,
  Button,
} from 'antd';
import ProductsService from '@services/ProductServices/Product.api.service';
import { useRouter } from 'next/router';
import Product from '@entities/product';
import Link from 'next/link';
import s from './styles.module.scss';
import BreadcrumbPage from '@components/Breadcrumb';

const View: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [product, setProduct] = useState<Product>();
  const [titlePage, setTitlePage] = useState('');
  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const product = await ProductsService.getById(id as string);

          if (product?.data) {
            setProduct(product.data.results);
            setTitlePage(product?.data.results.title);
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  return (
    <Row>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Products', route: '/products' },
          { name: titlePage },
        ]}
      />

      <Col xs={24} md={12} lg={12} xl={12}>
        <Card
          title="General Information"
          extra={
            <>
              <Link href={`/inventories?product=${router.query.id}`}>
                <Button
                  style={{ marginRight: 3, marginLeft: 3 }}
                  size={'small'}
                >
                  Manage Inventory
                </Button>
              </Link>
              <Link href={`/products/${router.query.id}/inventory`}>
                <Button
                  style={{ marginRight: 3, marginLeft: 3 }}
                  size={'small'}
                >
                  Create Inventory
                </Button>
              </Link>
              <Link href={`/product-variants/?product=${router.query.id}`}>
                <Button
                  style={{ marginRight: 3, marginLeft: 3 }}
                  size={'small'}
                >
                  Variants
                </Button>
              </Link>
            </>
          }
          className={s.row}
        >
          {product?.featuredImage !== null && (
            <div className={s.imgMainBox}>
              <img
                src={product?.featuredImage.url}
                alt={product?.featuredImage.alt}
                width="100%"
              />
            </div>
          )}
          <Descriptions column={2}>
            <Descriptions.Item label="Name Fa">
              {product?.title}
            </Descriptions.Item>
            <Descriptions.Item label="Name En">
              {product?.subtitle}
            </Descriptions.Item>

            <Descriptions.Item label="Category">
              {product?.category.title}
            </Descriptions.Item>
            <Descriptions.Item label="Brand">
              {product?.brand.title}
            </Descriptions.Item>
            <Descriptions.Item label="Status">
              {product?.status}
            </Descriptions.Item>
            <Descriptions.Item label="Is active">
              <Switch checked={product?.isActive} disabled={true} />
            </Descriptions.Item>
            <Descriptions.Item label="seller">
              {(product?.seller as ISeller)?.name || 'Timcheh'}
            </Descriptions.Item>
          </Descriptions>
        </Card>
        <Card className={s.row} title="SEO">
          <h4>Descrition:</h4>
          <Input.TextArea
            rows={4}
            style={{ width: '100%' }}
            maxLength={300}
            disabled
            value={product?.description}
          />
          <br />
          <br />
          <h4>Meta Description:</h4>
          <Input.TextArea
            rows={4}
            style={{ width: '100%', direction:"rtl" }}
            maxLength={300}
            disabled
            value={product?.metaDescription}
          />
        </Card>
        <Card className={s.row} title="Product Size">
          <Descriptions column={2}>
            <Descriptions.Item label="Weight (gr) ">
              {product?.weight}
            </Descriptions.Item>
            <Descriptions.Item label="Width (mm) ">
              {product?.width}
            </Descriptions.Item>
            <Descriptions.Item label="Length (mm)">
              {product?.length}
            </Descriptions.Item>
            <Descriptions.Item label="Height (mm)">
              {product?.height}
            </Descriptions.Item>
          </Descriptions>
        </Card>
        <Card className={s.row} title="Send Product">
          <Descriptions column={2}>
            <Descriptions.Item label="How To Send">
              {product?.shippingCategory.name}
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </Col>
      <Col xs={24} md={12} lg={12} xl={12}>
        <Card title="Gallery" className={s.row}>
          <div className={s.flex}>
            {
              //@ts-ignore
              product?.images.length !== 0 ? (
                product?.images?.map((item, inx) => {
                  return (
                    <a key={inx} className={s.imgBox}>
                      <img
                        src={item.url}
                        alt={item?.alt}
                        width="100%"
                      />
                      <p>{item.alt}</p>
                    </a>
                  );
                })
              ) : (
                <>
                  <p>No Photos!</p>
                  <a
                    href={`/products/${product.id}/edit`}
                    style={{ margin: '0 10px' }}
                  >
                    Add Photo
                  </a>
                </>
              )
            }
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default View;
