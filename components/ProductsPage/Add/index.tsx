import React, { useState } from 'react';
import { Store } from 'rc-field-form/es/interface';
import { Row, Col, Form, Card, message } from 'antd';
import ProductsService from '@services/ProductServices/Product.api.service';
import { AddProductProps } from '@entities/product';
import ProductForm from '@components/ProductsPage/Form';
import UplaodMedia from '@components/ProductsPage/UploadMedia';
import { useRouter } from 'next/router';
import s from './styles.module.scss';
import { IValidationError } from '@entities/validation.error';
import slugify from 'slugify';
import _ from 'lodash';

const Add: React.FC = () => {
    const [form] = Form.useForm();
    const [title, setTitle] = useState('');
    const [loading, setLoading] = useState(false);
    const [images, setImages] = useState([]);
    const [featuredImage, setFeaturedImage] = useState(undefined);
    const [isFeature, setIsFeature] = useState(false);
    const [isImages, setIsImages] = useState(false);
    const router = useRouter();

    const onChange = () => {
        setTitle(form.getFieldValue('title'))
    }
    const onFinish = async (values: Store): Promise<void> => {
        setLoading(true);
        try {
            const data = await ProductsService.add({
                ...values,
                brand: values.brand.id,
                category: values.category.id,
                seller: values?.seller?.id,
                featuredImage,
                images
            } as unknown as AddProductProps);
            message.success('Successfully added.');
            setTimeout(() => {
                router.push(`/products/[id]/edit`, `/products/${data.data.results.id}/edit`);
            }, 500);
        } catch (e) {
            const response: IValidationError = e.response?.data;
            if (response?.results) {
                const formErrors = Object.keys(response.results).map((key: string) => ({
                    name: key,
                    errors: response.results[key],
                }));
                form.setFields(formErrors);
            }
            message.error(response?.message || 'Server error!');
        }

        setLoading(false);
    };

    return (
        <Row>
            <Col xs={24} md={12} lg={12} xl={12}>
                <ProductForm
                    loading={loading}
                    form={form}
                    onFinish={onFinish}
                    onChange={onChange}
                    isRequire={true}
                />
            </Col>
            <Col xs={24} md={12} lg={12} xl={12}>
                <Card className={s.row} title="Featured Photo">
                    <p className={s.red}> Featured photo must have 1:1 ratio. (e.g. 1200x1200)</p>
                    {!isFeature ? <p className={s.red}> Add Alt is mandatory for each photo!</p> : ''}
                    <UplaodMedia
                        alt={title}
                        //@ts-ignore
                        onSetImages={(data, isDelete) => {
                            if (isDelete) {
                                //@ts-ignore
                                setFeaturedImage({});
                            }
                            if (!data.path.includes('http')) {
                                //@ts-ignore
                                setFeaturedImage(data)
                                setIsFeature(true)
                            }
                        }
                        }
                        isFeatureImage={true}
                        fileList={[]}
                    />
                </Card>
                <Card className={s.row} title="Gallery">
                    <p className={s.red}> Gallery photos must have 1:1 ratio. (e.g. 600x600)</p>
                    {!isImages ? <p className={s.red}> Add Alt is mandatory for each photo!</p> : ''}
                    <UplaodMedia
                        alt={title}
                        //@ts-ignore
                        onSetImages={(data, isDelete) => {
                            if (isDelete) {
                                _.remove(images!, {
                                    //@ts-ignore
                                    alt: data.alt
                                });
                            }
                            if (!data.path.includes('http')) {
                                //@ts-ignore
                                setImages((oldImages) => [...oldImages, data])
                                setIsImages(true)
                            }
                        }
                        }
                        isFeatureImage={false}
                        fileList={[]}
                    />
                </Card>
            </Col>
        </Row>
    );
};

export default Add;
