import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Button, Form } from 'antd';
import { Upload, Modal } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { Store } from 'rc-field-form/es/interface';
import _ from 'lodash';
import UploadService from '@services/UploadService';
import { ImageResponse } from '@entities/product';
import {ratioCheck, resizeDataURL} from '@utils/components.util';

interface UploadProps {
  alt?: string;
  fileList: ImageResponse[] | undefined;
  onSetImages: (values: Store, isDelete?: boolean) => Promise<ImageResponse>;
  isFeatureImage: boolean;
}

const UplaodMedia: React.FC<UploadProps> = (props) => {
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState<ImageResponse>();
  const [previewTitle, setPreviewTitle] = useState('');
  const [fileList, setFileList] = useState<ImageResponse[]>();
  const [alt, setAlt] = useState(props.alt);
  const [editAlt, setEditAlt] = useState(false);
  useEffect(() => {
    setFileList(props.fileList);
  }, []);
  useEffect(() => {
    setAlt(props.alt);
  }, [props.alt]);

  const handleCancel = () => {
    //@ts-ignore
    return setPreviewVisible(false);
  };
  //@ts-ignore
  const getBase64 = (file): Promise<string> => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result as string);
      reader.onerror = (error) => reject(error);
    });
  };
  //@ts-ignore
  const handlePreview = async (file) => {
    setPreviewImage(file);
    setPreviewVisible(true);
    setPreviewTitle(file.alt || file.name);
    return;
  };
  //@ts-ignore
  const saveImage = (obj) => {
    const res = obj.path.split("/");
    const inx = _.findIndex(fileList, function(o) { 
      return o.path === res[res.length -1]
    });

  //@ts-ignore
  if(inx !== -1){
    //@ts-ignore
    fileList[inx].alt = obj.alt;
    props.onSetImages(obj);
  }

  setAlt('');
  setEditAlt(false)
  handleCancel();

  };
  //@ts-ignore
  const deleteImage = (title, previewFile) => {
    handleCancel();
    const obj = {
      alt: title,
      path: previewFile.path,
      url: previewFile.url,
    }
    _.remove(fileList!, {
      //@ts-ignore
      path: previewFile.path,
    });
    props.onSetImages(obj, true);
  };

  const uploadButton = (
    <Button>
      <UploadOutlined /> Upload Image
    </Button>
  );

  return (
    <Row>
      <Col xs={24} md={24} lg={24} xl={24}>
        {!props.isFeatureImage ? (
          <Form name="control-hooks">
            <Form.Item
              extra={
                <Upload
                  name="imageFile"
                  customRequest={async (e) => {
                    const preview = await getBase64(e.file);
                    const resized = await resizeDataURL(preview, 500); // local adjustment for preview, NOT for uploaded file
                    try {
                      const response = await UploadService.add('product-gallery', e.file);
                      const obj = {
                        path: response?.data.results.imageFileName,
                        alt: alt,
                        url: resized
                      };
                      if (obj.path) {
                        props.onSetImages(obj);
                        //@ts-ignore
                        setFileList((oldImages) => [...oldImages, obj]);
                      }
                      setAlt('');
                      e.onSuccess(response?.data.results, e.file);
                    } catch (error) {
                      e.onError(error);
                    }
                  }}
                  beforeUpload={ratioCheck}
                  onPreview={handlePreview}
                  disabled={alt === ''}
                >
                  {uploadButton}
                </Upload>
              }
            >
              <Input
                value={alt}
                onChange={(e) => setAlt(e.target.value)}
                onPressEnter={() => setAlt('')}
                placeholder="Alt"
                style={{ width: '50%', margin: '10px auto' }}
              />
            </Form.Item>
          </Form>
        ) : fileList?.length === 0 ? (
          <Form name="control-hooks">
            <Form.Item
              extra={
                <Upload
                  name="imageFile"
                  customRequest={async (e) => {
                    try {
                      const response = await UploadService.add('product-image', e.file);
                      const preview = await getBase64(e.file);
                      const resized = await resizeDataURL(preview, 500); // local adjustment for preview, NOT for uploaded file
                      const obj = {
                        path: response?.data.results.imageFileName,
                        alt: alt,
                        url: resized,
                      };
                      if (obj.path) {
                        props.onSetImages(obj);
                        //@ts-ignore
                        setFileList((oldImages) => [...oldImages, obj]);
                      }
                      setAlt('');
                      e.onSuccess(response?.data.results, e.file);
                    } catch (error) {
                      e.onError(error);
                    }
                  }}
                  beforeUpload={ratioCheck}
                  onPreview={handlePreview}
                  disabled={alt === ''}
                >
                  {uploadButton}
                </Upload>
              }
            >
              <Input
                value={alt}
                onChange={(e) => setAlt(e.target.value)}
                onPressEnter={() => setAlt('')}
                placeholder="Alt"
                style={{ width: '50%', margin: '10px auto' }}
              />
            </Form.Item>
          </Form>
        ) : undefined}
        {fileList &&
          fileList.length !== 0 &&
          fileList?.map((item, inx) => {
            return (
              item && (
                <a
                  key={inx}
                  style={{ margin: '5px', display: 'inline-block', textAlign: 'center' }}
                  onClick={() => handlePreview(item)}
                >
                  <img
                    src={item.url}
                    alt={item?.alt}
                    width="100"
                    height="100"
                  />
                  <h4>{item.alt}</h4>
                </a>
              )
            );
          })}
        <Modal
          visible={previewVisible}
          title={previewTitle}
          footer={[
            <Button key="back" onClick={handleCancel}>
              Close
            </Button>,
            <Button
              type="primary"
              key="test"
              onClick={() => setEditAlt(true)}
            >
              Edit
            </Button>,
            <Button
            key="submit"
            danger
            onClick={() => deleteImage(previewTitle, previewImage)}
          >
            Delete
          </Button>,
          ]}
          onCancel={handleCancel}
        >
          {editAlt ? 
          <>
          <Input defaultValue={previewTitle} onChange={(e)=> setAlt(e.target.value)}/>
          <br/>
          <br/>
          <Button
            type="primary"
            onClick={() => saveImage({alt: alt, path:previewImage?.path, url: previewImage?.url })}
          >
            Save
          </Button>
          </> : ''}
          <img alt={previewTitle} style={{ width: '100%' }} src={previewImage?.url} />
        </Modal>
      </Col>
    </Row>
  );
};

export default UplaodMedia;
