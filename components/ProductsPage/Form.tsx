import BrandSelector from "@components/CP/BrandSelector";
import CategorySelector from "@components/CP/CategorySelector";
import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Space, Switch, Select, Card, InputNumber } from 'antd';
import { FormInstance } from 'antd/es/form';
import { Store } from 'rc-field-form/es/interface';
import categoriesService from '@services/categories/categories.api.service';
import ShippingCategoriesApiService from '@services/ShippingService/ShippingCategories.api.service';
import BrandsService from '@services/BrandsService';
import ProductOptionApiService from '@services/ProductServices/ProductOption.api.service';
import { Nullable, IPaginatedResponse } from '@entities/paginated.response';
import Category from '@entities/category';
import ShippingCategory from '@entities/shippingCategory';
import Brand from '@entities/brand';
import ProductOption from '@entities/productOption';
import SellerSelector from "@components/CP/SellerSelector";
import SelectSearch from '@components/SelectSearch';
import s from './styles.module.scss';
import Link from 'next/link';

interface FormProps {
    onFinish: (values: Store) => Promise<void>;
    onChange?: (event: React.FormEvent<HTMLFormElement>) => void;
    form: FormInstance;
    loading: boolean;
    isActive?: boolean;
    isRequire?: boolean;
}

const ProductForm: React.FC<FormProps> = (props) => {
    const { onFinish, form, loading } = props;
    const [categories, setCategories] = useState<Category[]>();
    const [shippingCategories, setShippingCategories] = useState<Nullable<IPaginatedResponse<ShippingCategory[]>>>();
    const [productOptions, setProductOptions] = useState<Nullable<IPaginatedResponse<ProductOption[]>>>();
    const [isActiveProduct, setIsActiveProduct] = useState(true);
    const status = ['DRAFT', 'EDITING', 'WAITING_FOR_ACCEPT', 'CONFIRMED', 'REJECTED', 'SOON', 'UNAVAILABLE', 'SHUTDOWN'];

    useEffect(() => {
        (async () => {
            const [
                categories,
                shippingCategories,
                productOptions,
            ] = await Promise.all([
                categoriesService.grid(),
                ShippingCategoriesApiService.grid(),
                ProductOptionApiService.getList(1, "&limit=9999"),
            ]);
            const data = categoriesService.searchParentCategory(categories.data);
            //@ts-ignore
            setCategories(data);
            //@ts-ignore
            setShippingCategories(shippingCategories.data.results);
            //@ts-ignore
            setProductOptions(productOptions.data.results);
            if (props.isActive) {
                setIsActiveProduct(props?.isActive);
            }

        })();
    }, []);
    // form layout
    const layout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    };


    const getMoreDataCategories = async (pageIndex: number) => {
        const newData = await categoriesService.grid(pageIndex as number);
        //@ts-ignore
        return CategoriesApiService.searchParentCategory(newData.data);
    };
    const getMoreDataBrands = async (pageIndex: number) => {
        const newData = await BrandsService.getList(pageIndex as number);
        //@ts-ignore
        return newData;
    };
    const getMoreDataProductOptions = async (pageIndex: number) => {
        const newData = await ProductOptionApiService.getList(pageIndex as number);
        //@ts-ignore
        return newData;
    }
    const getMoreDataShippingCategory = async (pageIndex: number) => {
        const newData = await ShippingCategoriesApiService.grid(pageIndex as number);
        //@ts-ignore
        return newData;
    }

    return (
        <Form
            {...layout}
            form={form}
            name="control-hooks"
            //@ts-ignore
            onFinish={onFinish}
            onChange={props.onChange}
            className={s.flex}
            initialValues={{
                isOriginal: "1"
              }}
        >
            <Card
                title="General Information"
                className={s.row}
                extra={
                    <Link href="/products">
                        <a>View All </a>
                    </Link>
                }
            >
                <Form.Item name="code" label="Slug"  rules={[{ required: true }]}>
                    <Input/>
                </Form.Item>
                <Form.Item name="title" label="Name Fa"  rules={[{ required: true }]} >
                <Input  style={{direction: "rtl"}} />
                </Form.Item>
                <Form.Item
                    name="subtitle"
                    label="Name En"
                >
                    <Input/>
                </Form.Item>

                <Form.Item name="category" label="Category"
                  rules={[{ required: props.isRequire }]}
                >
                    <CategorySelector isLeaf/>
                </Form.Item>
                <Form.Item name="brand" label="Brand"
                   rules={[{ required: props.isRequire }]}
                >
                    <BrandSelector />
                </Form.Item>
                <Form.Item name="seller" label="Seller">
                    <SellerSelector/>
                </Form.Item>

                <Form.Item
                    name="status"
                    label="Status"
                     rules={[{ required: true }]}
                >
                    <Select placeholder="Select Status" allowClear>
                        {status.map((item, inx) => (
                            //@ts-ignore
                            <Select.Option key={inx} value={item}>
                                {item}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>

                <Form.Item
                    name="isOriginal"
                    label="Is original"
                    rules={[{ required: true }]}
                >
                    <Select placeholder="Select originality" allowClear >
                        <Select.Option value={"1"}>Original</Select.Option>
                        <Select.Option value={"0"}>Not original</Select.Option>
                    </Select>
                </Form.Item>

                <Form.Item name="isActive" label="IsActive" valuePropName="checked">
                    <Switch />
                </Form.Item>
            </Card>
            <Card className={s.row} title="SEO">
                <Form.Item
                    name="description"
                    label="Description"
                >
                    <Input.TextArea rows={8} maxLength={2000}/>
                </Form.Item>
                <Form.Item
                    name="metaDescription"
                    label="Meta Description"
                >
                    <Input.TextArea rows={4} maxLength={300}/>
                </Form.Item>
            </Card>
            <Card title="Product Size" className={s.row}>
                <Form.Item name="weight" label="Weight (gr)" >
                    <InputNumber className={s.numberInput}/>
                </Form.Item>
                <Form.Item name="width" label="Width (mm)">
                    <InputNumber className={s.numberInput}/>
                </Form.Item>
                <Form.Item name="length" label="Length (mm)" >
                    <InputNumber className={s.numberInput}/>
                </Form.Item>
                <Form.Item name="height" label="Height (mm)" >
                    <InputNumber className={s.numberInput}/>
                </Form.Item>
            </Card>
            <Card title="Shipping & Variants" className={s.row}>
                <Form.Item name="shippingCategory" label="Shipping Category" 
                 rules={[{ required: props.isRequire }]}
                >
                    {shippingCategories && (
                        <SelectSearch
                            placeholder={"Select shipping category"}
                            keyShow={'name'}
                            //@ts-ignore
                            datalist={shippingCategories}
                            getMoreData={(pageInxSelect) => getMoreDataShippingCategory(pageInxSelect)}
                            setSelectedOption={(val: number) =>

                                form.setFieldsValue({ shippingCategory: val })
                            }
                        />
                    )}
                </Form.Item>

                <Form.Item name="options" label="Product Options">
                    {productOptions && (
                        <SelectSearch
                            placeholder={'Select Options'}
                            keyShow={'name'}
                            isMultiple={true}
                            //@ts-ignore
                            datalist={productOptions}
                            getMoreData={(pageInxSelect) => getMoreDataProductOptions(pageInxSelect)}
                            setSelectedOption={(val: number) => form.setFieldsValue({ options: val })}

                        />
                    )}
                </Form.Item>

            </Card>

            <Form.Item>
                <Space size="small">
                    <Button type="primary" htmlType="submit" loading={loading}>
                        Save
                    </Button>
                </Space>
            </Form.Item>
        </Form>
    );
};

export default ProductForm;
