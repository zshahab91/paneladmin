import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Form, Card, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import ShippingCategoriesForm from '@components/ShippingCategoryPage/Form';
import { AddShippingCategory } from '@entities/shippingCategory';
import ShippingCategoriesApiService from '@services/ShippingService/ShippingCategories.api.service';
import { useRouter } from 'next/router';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const onFinish = useCallback(async (values: Store): Promise<void> => {
    const { name } = values;
    setLoading(true);

    const dto: AddShippingCategory = {
      name,
    };

    try {
      await ShippingCategoriesApiService.add(dto);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/shipping-categories`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
        <Card
          title="Add Shipping Category"
          extra={
            <Link href="/shipping-categories">
              <a>View All </a>
            </Link>
          }
        >
          <FormAlignment>
          <ShippingCategoriesForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
          </FormAlignment>
        </Card>
  );
};

export default Add;
