import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Form, Card, message } from 'antd';
import { Store } from 'rc-field-form/es/interface';
import { useRouter } from 'next/router';
import ShippingCategoriesForm from '@components/ShippingCategoryPage/Form';
import { EditShippingCategory } from '@entities/shippingCategory';
import ShippingCategoriesApiService from '@services/ShippingService/ShippingCategories.api.service';
import BreadcrumbPage from '@components/Breadcrumb';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [titlePage, setTitlePage] = useState('');

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const shippingCategory = await ShippingCategoriesApiService.getById(
            id as string,
          );

          if (shippingCategory?.data) {
            const { name } = shippingCategory.data.results;
            setTitlePage(name);
            form.setFieldsValue({ name });
          }
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  const onFinish = useCallback(
    async (values: Store): Promise<void> => {
      setLoading(true);

      const dto: EditShippingCategory = {
        name: values.name,
      };

      try {
        await ShippingCategoriesApiService.edit(dto, id as string);
        message.success('Edited successfully.');
        setTimeout(() => {
          router.push(`/shipping-categories`);
        }, 500);
      } catch (e) {
        const response: IValidationError = e.response?.data;
        if (response?.results) {
          const formErrors = Object.keys(response.results).map(
            (key: string) => ({
              name: key,
              errors: response.results[key],
            }),
          );
          form.setFields(formErrors);
        }
        message.error(response?.message || 'Server error!');
      }

      setLoading(false);
    },
    [id],
  );

  return (
    <>
      <BreadcrumbPage
        listLinks={[
          { name: 'Home', route: '/' },
          { name: 'Shipping Categories', route: '/shipping-categories' },
          { name: titlePage },
        ]}
      />

        <Card
          title="Edit Shipping Category"
          extra={
            <Link href="/shipping-categories">
              <a>View All </a>
            </Link>
          }
        >
        <FormAlignment>
          <ShippingCategoriesForm
            loading={loading}
            form={form}
            onFinish={onFinish}
          />
        </FormAlignment>
        </Card>
    </>
  );
};

export default Edit;
