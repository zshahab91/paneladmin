import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Row, Col, Card, message, Form } from 'antd';
import ProductOptionsForm from '@components/ProductOptionsPage/Form';
import { AddProductOption } from '@entities/productOption';
import ProductOptionApiService from '@services/ProductServices/ProductOption.api.service';
import { useRouter } from 'next/router';

const Edit: React.FC = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [loading, setLoading] = useState(false);
  const initialState = {
    name: '',
    code: '',
    values: [{ value: '', attributes: {} }],
  };
  const [fields, setFields] = useState<AddProductOption>(initialState);

  const onFinish = useCallback(async (formValues: AddProductOption): Promise<
    void
  > => {
    setLoading(true);

    try {
      await ProductOptionApiService.edit(formValues, id as string);
      message.success('Edited successfully.');
      setTimeout(() => {
        router.push(`/product-options`);
      }, 500);
    } catch (e) {
      message.error(
        `Error in operation is: ${JSON.parse(JSON.stringify(e)).message} `,
      );
    }

    setLoading(false);
  }, []);

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const response = await ProductOptionApiService.getById(id as string);
          setFields(response?.data?.results);
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  return (
    <Row>
      <Col xs={24} md={12} lg={12} xl={10}>
        <Card
          title="Update Product Options"
          extra={
            <Link href="/product-options">
              <a>View All </a>
            </Link>
          }
        >
          <ProductOptionsForm
            form={form}
            read
            loading={loading}
            onFinish={onFinish}
            fields={fields}
            setFields={setFields}
          />
        </Card>
      </Col>
    </Row>
  );
};

export default Edit;
