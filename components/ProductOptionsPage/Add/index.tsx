import React, { useCallback, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import ProductOptionsForm from '@components/ProductOptionsPage/Form';
import { AddProductOption } from '@entities/productOption';
import ProductOptionApiService from '@services/ProductServices/ProductOption.api.service';
import { useRouter } from 'next/router';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";


const Add: React.FC = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const initialState = {
    name: '',
    values: [{ value: '', attributes: {} }],
    code: ''
  };
  const [fields, setFields] = useState<AddProductOption>(initialState);
  const router = useRouter();


  
  const onFinish = useCallback(async (formValues: AddProductOption): Promise<
    void
  > => {
    setLoading(true);

    try {
      await ProductOptionApiService.add(formValues);
      setFields(initialState);
      message.success('Successfully added.');
      setTimeout(() => {
        router.push(`/product-options`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, []);

  return (
    <Card
      title="Add Product Option"
      extra={
        <Link href="/product-options">
          <a>View All </a>
        </Link>
      }
    >
      <FormAlignment>
        <ProductOptionsForm
          form={form}
          loading={loading}
          onFinish={onFinish}
          fields={fields}
          setFields={setFields}
        />
      </FormAlignment>
    </Card>
  );
};

export default Add;
