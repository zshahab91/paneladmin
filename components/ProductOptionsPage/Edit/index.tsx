import React, { useCallback, useEffect, useState } from 'react';
import Link from 'next/link';
import { Card, Form, message } from 'antd';
import ProductOptionsForm from '@components/ProductOptionsPage/Form';
import { EditProductOption } from '@entities/productOption';
import ProductOptionApiService from '@services/ProductServices/ProductOption.api.service';
import { useRouter } from 'next/router';
import { IValidationError } from '@entities/validation.error';
import FormAlignment from "@components/CP/FormAlignment";

const Edit: React.FC = () => {
  const [form] = Form.useForm();
  const router = useRouter();
  const { id } = router.query;
  const [loading, setLoading] = useState(false);
  const initialState = {
    name: '',
    values: [{ value: '', attributes: {} }],
    code: ''
  };
  const [fields, setFields] = useState<EditProductOption>(initialState);

  const onFinish = useCallback(async (formValues: EditProductOption): Promise<
    void
  > => {
    setLoading(true);

    try {
      await ProductOptionApiService.edit(formValues, id as string);
      message.success('Edited successfully.');
      setTimeout(() => {
        router.push(`/product-options`);
      }, 500);
    } catch (e) {
      const response: IValidationError = e.response?.data;
      if (response?.results) {
        const formErrors = Object.keys(response.results).map((key: string) => ({
          name: key,
          errors: response.results[key],
        }));
        form.setFields(formErrors);
      }
      message.error(response?.message || 'Server error!');
    }

    setLoading(false);
  }, [id]);

  useEffect(() => {
    (async () => {
      if (id) {
        try {
          const response = await ProductOptionApiService.getById(id as string);
          setFields(response?.data?.results || initialState);
          form.setFieldsValue(response?.data?.results)
        } catch {
          message.error('Error receiving information');
        }
      }
    })();
  }, [id]);

  return (
    <>
      <Card
        title="Update Product Options"
        extra={
          <Link href="/product-options">
            <a>View All </a>
          </Link>
        }
      >
        <FormAlignment>
          <ProductOptionsForm
            form={form}
            loading={loading}
            onFinish={onFinish}
            fields={fields}
            setFields={setFields}
          />
        </FormAlignment>
      </Card>
    </>
  );
};

export default Edit;
