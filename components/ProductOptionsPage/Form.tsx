import React from 'react';
import { Form, Input, Button, Space, Divider } from 'antd';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons/lib';
import s from './styles.module.scss';
import { AddProductOption } from '@entities/productOption';
import slugify from 'slugify';
import { FormInstance } from 'antd/es/form';

interface FormProps {
  form: FormInstance;
  onFinish: (values: AddProductOption) => Promise<void>;
  loading?: boolean;
  fields: AddProductOption;
  setFields: (values: AddProductOption) => void;
  read?: boolean;
}

const ProductOptionsForm: React.FC<FormProps> = (props) => {
  const { onFinish, loading, setFields, fields, read, form } = props;

  // form layout
  const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
  };

  // form layout
  const tailLayout = {
    wrapperCol: { offset: 6, span: 16 },
  };

  const handleAddAttributes = (index: number, value: string): void => {
    if (!value) return;
    const itemClone = { ...fields.values[index] };
    itemClone.attributes![value] = itemClone.attributes![value] || '';
    const arrayClone = [...fields.values];
    arrayClone.splice(index, 1, itemClone);
    setFields({ ...fields, values: arrayClone });
  };

  const handleAddNewValue = (): void => {
    const hasEmptyValue = fields.values.some((item) => item.value === '');
    if (!hasEmptyValue) {
      const newData = {
        ...fields,
        values: [...fields.values, { value: '', attributes: {} }],
      };

      setFields(newData);
    }
  };

  const handleDeleteValue = (index: number): void => {
    if (fields.values.length > 1) {
      const arrayClone = [...fields.values];
      arrayClone.splice(index, 1);
      setFields({ ...fields, values: arrayClone });
    }
  };

  const handleValueChange = (value: string, index: number): void => {
    const itemClone = { ...fields.values[index], value };
    const arrayClone = [...fields.values];
    arrayClone.splice(index, 1, itemClone);
    setFields({ ...fields, values: arrayClone });
  };

  const handleAttributeChange = (
    value: string,
    i: number,
    index: number,
  ): void => {
    const itemClone = { ...fields.values[index] };
    const prevKey = Object.entries(itemClone!.attributes as object)[i][0];
    itemClone!.attributes![prevKey] = value;
    const arrayClone = [...fields.values];
    arrayClone.splice(index, 1, itemClone);
    setFields({ ...fields, values: arrayClone });
  };

  const handleDeleteAttribute = (i: number, index: number): void => {
    const itemClone = { ...fields.values[index] };
    const prevKey = Object.entries(itemClone!.attributes as object)[i][0];
    delete itemClone!.attributes![prevKey];
    const arrayClone = [...fields.values];
    arrayClone.splice(index, 1, itemClone);
    setFields({ ...fields, values: arrayClone });
  };

  return (
    <Form {...layout} form={form} name="control-hooks">
      <Form.Item name="code" label="Code" rules={[{ required: true }]}>
      <Input
          value={fields?.code}
          onChange={({ target: { value } }) => {
            setFields({ ...fields, code: slugify(value) });
          }}
          placeholder=""
        />
      </Form.Item>
      <Form.Item label="Name" name="name" rules={[{ required: true }]}>
        <Input
          placeholder="Color"
          onChange={({ target: { value } }) => {
            setFields({ ...fields, name: value });
          }}
        />
      </Form.Item>
      <Divider orientation="left">Values</Divider>
      {fields?.values?.map((item, index) => (
        <>
          <Form.Item label="Value" name="value" rules={[{ required: true }]}>
            <Space className={s.inputRow}>
              <Input
                value={item.value}
                placeholder="Red"
                onChange={({ target: { value } }) =>
                  handleValueChange(value, index)
                }
              />
              <Button
                type="text"
                danger
                icon={<MinusOutlined />}
                onClick={() => {
                  handleDeleteValue(index);
                }}
              />
            </Space>
          </Form.Item>
          {Object.entries(item.attributes as object).map((arr, i) => {
            return (
              <Form.Item label="Property" key={arr[0]} name={arr[0]}>
                <Input.Group>
                  <Space>
                    <Input placeholder="hex" value={arr[0]} disabled />
                    <Input
                      placeholder="#f5f5f5"
                      value={arr[1] as string}
                      onChange={({ target: { value } }) =>
                        handleAttributeChange(value, i, index)
                      }
                    />
                    <Button
                      type="text"
                      danger
                      icon={<MinusOutlined />}
                      onClick={() => {
                        handleDeleteAttribute(i, index);
                      }}
                    />
                  </Space>
                </Input.Group>
              </Form.Item>
            );
          })}
          <Form.Item label="&nbsp;" colon={false}>
            <div style={{ width: '200px' }}>
              <Input
                size="small"
                onPressEnter={({ currentTarget }) => {
                  handleAddAttributes(index, currentTarget.value);
                }}
                placeholder="New Feature Title + Enter"
              />
            </div>
          </Form.Item>
        </>
      ))}

      {!read && (
        <Form.Item {...tailLayout}>
          <Space size="small">
            <Button
              type="primary"
              htmlType="button"
              onClick={() => {
                onFinish(fields);
              }}
              loading={loading}
            >
              Save
            </Button>
            <Button
              htmlType="button"
              icon={<PlusOutlined />}
              onClick={handleAddNewValue}
            >
              Add value
            </Button>
          </Space>
        </Form.Item>
      )}
    </Form>
  );
};

export default ProductOptionsForm;
