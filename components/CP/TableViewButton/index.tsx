import React from "react";
import { FolderViewOutlined } from "@ant-design/icons/lib";
import Link from "next/link";
import s from "./styles.module.scss";


interface Props {
    href: string;
}

const TableViewButton: React.FC<Props> = ({ href }) => {
    return (
        <Link href={href}>
            <a target="_blank">
                <FolderViewOutlined className={s.icon}/>
            </a>
        </Link>
    );
};

export default TableViewButton;
