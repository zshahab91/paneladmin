import React, { useState, useEffect } from 'react';
import moment, { Moment } from 'moment-jalaali';
import { Input } from 'antd';
import DatePicker, { DayValue } from 'react-modern-calendar-datepicker';

interface DatePickerProps {
  onChange?: (value: Moment) => void;
  initialValue?: Date;
}

const CPDatePicker: React.FC<DatePickerProps> = ({
  onChange,
  initialValue,
}) => {
  const month = moment(initialValue).format('jM');
  const day = moment(initialValue).format('jD');
  const year = moment(initialValue).format('jYYYY');
  const defaultValue = {
    year: Number(year),
    month: Number(month),
    day: Number(day),
  };



  const [selectedDay, setSelectedDay] = useState<DayValue>();
  useEffect(()=>{
    if(initialValue){
      setSelectedDay(defaultValue)
    }
  },[])

  return (
    <DatePicker
      renderInput={({ ref }) => {
        return (
          <Input
            // @ts-ignore
            ref={ref}
            value={
              selectedDay
                ? `${selectedDay?.year}-${selectedDay?.month}-${selectedDay?.day}`
                : ''
            }
          />
        );
      }}
      locale="fa"
      value={selectedDay ? selectedDay : defaultValue}
      onChange={(value) => {
        if (value) {
          onChange?.(
            //@ts-ignore
            moment(`${value?.year}-${value?.month}-${value?.day}`, 'jYYYY-jM-jD').format('YYYY-MM-DD')
          );
          setSelectedDay(value);
        }
      }}
      //@ts-ignore
      inputPlaceholder={defaultValue? defaultValue : 'Select Date'}
      shouldHighlightWeekends
    />
  );
};

export default CPDatePicker;
