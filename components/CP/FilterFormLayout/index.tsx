import {FormInstance} from "antd/es/form";
import {Form} from "antd";
import React from "react";


interface Props {
    onFinish: (values: { [key: string]: unknown }) => void;
    form: FormInstance;
    children: React.ReactNode;
}


const FilterFormLayout: React.FC<Props> = ({form, onFinish, children}) => {
    return <Form
        form={form}
        name="control-hooks"
        onFinish={(e) => onFinish(e)}
        labelCol={{
            xs: {span: 24},
            sm: {span: 8},
        }}
        wrapperCol={{
            xs: {span: 24},
            sm: {span: 16},
        }}>
        {children}
    </Form>
}

export default FilterFormLayout
