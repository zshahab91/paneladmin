import React from "react";
import Link from "next/link";
import s from "./styles.module.scss";
import { LockOutlined } from "@ant-design/icons";


interface Props {
    href: string;
}

const TableChangePasswordButton: React.FC<Props> = ({href}) => {
    return (
        <Link href={href}>
            <LockOutlined className={s.icon}/>
        </Link>
    );
};

export default TableChangePasswordButton;
