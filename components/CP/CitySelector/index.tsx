import { useProvinces } from "@components/hooks/province";
import { City, Province } from "@entities/zone";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from 'react';
import SelectSearch from '@components/SelectSearch';
import { useCities } from '@components/hooks/city';

interface Props {
  value?: City;
  onChange?: (category: City) => void;
}

const CitySelector: React.FC<Props> = ({ onChange, value }) => {
  const [search, setSearch] = useState<string>();
  const { data: cities, isLoading } = useCities({ name: search });
  const onSearchHandler = (value: string) => setSearch(value);
  const onChangeHandler = ({ value: id }: { value: number }) => {
    const city: City = find<City>(cities, { id }) as City;
    onChange?.(city);
    setSearch(undefined);
  }
  const onSearch = debounce(onSearchHandler, 500, { leading: false })

  return (
      <Select
          showSearch
          allowClear
          labelInValue
          loading={isLoading}
          onSearch={onSearch}
          onChange={onChangeHandler}
          filterOption={false}
          placeholder="Choose a city"
          value={value ? { label: value?.name, value: value?.id } : undefined}
      >
        {cities?.map(p => (<Select.Option key={p.id} value={p.id}>{p.name}</Select.Option>))}
      </Select>
  );
};

export default CitySelector;
