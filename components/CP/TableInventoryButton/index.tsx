import React from 'react';
import { LinkedinOutlined } from '@ant-design/icons/lib';
import Link from "next/link";
import s from "./styles.module.scss";

interface Props {
  href: string;
}

const TableInventoryButton: React.FC<Props> = ({ href }) => {
  return (
      <Link href={href}>
          <LinkedinOutlined className={s.icon}/>
      </Link>
  );
};

export default TableInventoryButton;
