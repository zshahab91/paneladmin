import { useBrands } from "@components/hooks/brands";
import Brand from "@entities/brand";
import { Select } from "antd";
import React, { useState } from "react";
import { debounce, find } from 'lodash'

interface Props {
    value?: Brand;
    onChange?: (brand: Brand) => void
}

const BrandSelector: React.FC<Props> = ({ onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: brands, isLoading } = useBrands({ title: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const brand: Brand = find<Brand>(brands, { id }) as Brand;
        onChange?.(brand);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Choose a brand"
            value={value ? { label: value?.title, value: value?.id } : undefined}
        >11
            {brands?.map(b => (<Select.Option key={b.id} value={b.id}>{b.title}</Select.Option>))}
        </Select>
    );
}

export default BrandSelector
