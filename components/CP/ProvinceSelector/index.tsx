import { useProvinces } from '@components/hooks/province';
import { Province } from "@entities/zone";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from 'react';

interface Props {
    value?: Province;
    onChange?: (category: Province) => void;
}

const ProvinceSelector: React.FC<Props> = ({ onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: provinces, isLoading } = useProvinces({ name: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const province: Province = find<Province>(provinces, { id }) as Province;
        onChange?.(province);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Choose a province"
            value={value ? { label: value?.name, value: value?.id } : undefined}
        >
            {provinces?.map(p => (<Select.Option key={p.id} value={p.id}>{p.name}</Select.Option>))}
        </Select>
    );
};

export default ProvinceSelector;
