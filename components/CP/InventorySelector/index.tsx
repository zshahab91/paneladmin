import { useInventories } from "@components/hooks/inventories";
import { useProducts } from "@components/hooks/products";
import Inventory from "@entities/inventories";
import Product from "@entities/product";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from 'react';
import SelectSearch from '@components/SelectSearch';
import { useSellers } from '@components/hooks/sellers';

interface Props {
    value?: Inventory;
    onChange?: (inventory: Inventory) => void;
}

const InventorySelector: React.FC<Props> = ({ onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: inventories, isLoading } = useInventories({ id: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const inventory: Inventory = find<Inventory>(inventories, { id }) as Inventory;
        onChange?.(inventory);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Choose a TVI"
            value={value ? { label: value?.title, value: value?.id } : undefined}
        >
            {inventories?.map(p => (<Select.Option key={p.id} value={p.id}>{p.title}</Select.Option>))}
        </Select>
    );
};

export default InventorySelector;
