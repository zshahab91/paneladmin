import React from "react";
import {Input} from "antd";

interface Props {
    value?: { gte?: number, lte?: number };
    onChange?: (newValue: Props["value"]) => void
}

const RangeInput: React.FC<Props> = ({onChange, value}) => {

    function handleValueChange(fieldName: 'gte' | 'lte', newFieldValue: string): void {
        if (!onChange)
            return;

        const newValue = {
            ...(value || {}),
            [fieldName]: Number(newFieldValue)
        };
        onChange(newValue)
    }

    return <div style={{display: "flex"}}>
        <Input value={value?.gte} type="number"
               style={{flex: 1}}
               onChange={e => handleValueChange('gte', e.target.value)}/>

        <span style={{margin: 5}}>to</span>

        <Input value={value?.lte} type="number"
               style={{flex: 1}}
               onChange={e => handleValueChange('lte', e.target.value)}/>
    </div>
}

export default RangeInput
