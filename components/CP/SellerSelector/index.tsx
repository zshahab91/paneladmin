import { useSellers } from "@components/hooks/sellers";
import Seller from "@entities/seller";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from 'react';

interface Props {
    value?: Seller;
    onChange?: (seller: Seller) => void
}

const SellerSelector: React.FC<Props> = ({ onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: sellers, isLoading } = useSellers({ name: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const seller: Seller = find<Seller>(sellers, { id }) as Seller;
        onChange?.(seller);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Choose a seller"
            value={value ? { label: value?.name, value: value?.id } : undefined}
        >
            {sellers?.map(b => (<Select.Option key={b.id} value={b.id}>{b.name}</Select.Option>))}
        </Select>
    );
};

export default SellerSelector;
