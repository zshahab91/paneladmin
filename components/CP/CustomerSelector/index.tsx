import { useCustomers } from '@components/hooks/customers';
import Customer from "@entities/customer";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from 'react';

interface Props {
    value?: Customer;
    onChange?: (category: Customer) => void;
}

const CustomerSelector: React.FC<Props> = ({ onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: customers, isLoading } = useCustomers({ mobile: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const customer: Customer = find<Customer>(customers, { id }) as Customer;
        onChange?.(customer);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Phone number"
            value={value ? { label: value?.mobile, value: value?.id } : undefined}
        >
            {customers?.map(p => (
                <Select.Option key={p.id} value={p.id}>{p.name} {p.family} - {p.mobile}</Select.Option>))}
        </Select>
    );
};

export default CustomerSelector;
