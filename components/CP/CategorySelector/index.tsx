import Category from "@entities/category";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from "react";
import { useCategories } from "@components/hooks/categories";

interface Props {
    value?: Category;
    onChange?: (category: Category) => void
    isLeaf?: boolean;
}

const CategorySelector: React.FC<Props> = ({ isLeaf, onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: categories, isLoading } = useCategories({ isLeaf, title: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const category: Category = find<Category>(categories, { id }) as Category;
        onChange?.(category);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Choose a category"
            value={value ? { label: value?.title, value: value?.id } : undefined}
        >
            {categories?.map(c => (<Select.Option key={c.id} value={c.id}> {c.title} | {c.subtitle}</Select.Option>))}
        </Select>
    )
}

export default CategorySelector
