import { useProducts } from "@components/hooks/products";
import Product from "@entities/product";
import { Select } from "antd";
import { debounce, find } from "lodash";
import React, { useState } from 'react';

interface Props {
    value?: Product;
    onChange?: (category: Product) => void;
}

const ProductSelector: React.FC<Props> = ({ onChange, value }) => {
    const [search, setSearch] = useState<string>();
    const { data: products, isLoading } = useProducts({ title: search });
    const onSearchHandler = (value: string) => setSearch(value);
    const onChangeHandler = ({ value: id }: { value: number }) => {
        const category: Product = find<Product>(products, { id }) as Product;
        onChange?.(category);
        setSearch(undefined);
    }
    const onSearch = debounce(onSearchHandler, 500, { leading: false })

    return (
        <Select
            showSearch
            allowClear
            labelInValue
            loading={isLoading}
            onSearch={onSearch}
            onChange={onChangeHandler}
            filterOption={false}
            placeholder="Choose a product"
            value={value ? { label: value?.title, value: value?.id } : undefined}
        >
            {products?.map(p => (<Select.Option key={p.id} value={p.id}>{p.title}</Select.Option>))}
        </Select>
    );
};

export default ProductSelector;
