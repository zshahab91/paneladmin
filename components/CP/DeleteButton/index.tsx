import React from 'react';
import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons/lib';

const { confirm } = Modal;

interface DeleteModalProps {
  onDelete: () => void | Promise<void> | Promise<{}>;
}

const DeleteButton: React.FC<DeleteModalProps> = ({ onDelete }) => {

  function showDeleteConfirm(): void {
    confirm({
      title: 'Are you sure you want to delete this item?',
      icon: <ExclamationCircleOutlined />,
      content: 'Delete operation is irrevocable.',
      okText: 'Delete',
      okType: 'danger',
      cancelText: 'Cancle',
      async onOk() {
        await onDelete();
        location.reload();
        // router.reload(); todo
      },
      onCancel() {},
    });
  }

  return (
    <DeleteOutlined
      style={{ color: 'red', fontSize: '2rem' }}
      onClick={showDeleteConfirm}
    />
  );
};

export default DeleteButton;
